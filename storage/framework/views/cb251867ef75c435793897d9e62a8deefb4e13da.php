                <div class="box-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Name</th>
						<th>Photo</th>
                        <th>Email</th>
                       
                        <th>Status</th>
						<th>Verified</th>
						<th>Created</th>
                        <th id="action" style="display: ">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['first_name']." ".$value['last_name']; ?></td>
<td class="list-img"><?php if($value['profile_image'] !='') { ?> <img src="<?php if($value['profile_image'] !='') { echo url('/').'/users/'.$value['profile_image']; }?>" style="max-height:140px"   class="img-thumbnail"> <?php } else { echo '<span>No Image</span>';}?></td>         
                    <td><?php echo $value['email']; ?></td>
                    
                   	<td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Inactive";
                   	   }
                   	   else
                   	   {
                   	    echo "Active";
                   		} ?>
                   	</td>
			 
			 
			 <!-----verify --->
			 <td>
			 		  <?php if($value['verified'] == '1') { ?>
				   <span style="color:#008A11"><b>VERIFIED</b></span>
                   <a class="btn btn-small btn-danger" title="Verify" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/unverify')); ?>" onclick="return confirm('Are you sure to Verify this User?')">UnVerify</a>  
				  <?php } else { ?>  <span style="color:#8A1000"><b>UNVERIFIED</b></span>
                   <a class="btn btn-small btn-danger" title="UnVerify" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/verify')); ?>" onclick="return confirm('Are you sure to UnVerfiy this User?')">Verify</a>  
				  <?php } ?>
			</td>	  
				  <!------------->
				   
                    <td><?php echo $value['created_at']; ?></td>
					
					
					
					
					
				
                    <td class="action-btn">
                     <a class="btn btn-small  " title="View User" href="<?php echo e(URL::to('admin/user/show/'.$value['id'])); ?>"  target="_blank" ><i class="fa fa-eye"></i></a>           
                   <a class="btn btn-small  " title="Edit User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/edit')); ?>" target="_blank"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small  " title="Block User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/block')); ?>" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small  " title="Unblock User" href="<?php echo e(URL::to('admin/user/'.$value['id'].'/unblock')); ?>" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
				  
				  
				 
 
            	   </td>
                   	 </tr>
					 
					 
					 
					 
 



                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                