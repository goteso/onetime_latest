<?php $__env->startSection('content'); ?>

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Users</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo e(URL::to('admin/user')); ?>">Users</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <!-- Main content -->
        <section class="content">
            <div class="box ">
                
				 <nav class="navbar filters-nav">
 
    <div class="navbar-header">
      <div class="navbar-brand" >Filter By Status :</div>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="<?php echo e(URL::to('admin/user/status/1')); ?>">Active(<?php echo e(\App\User::where(['status' => 1])->get()->count()); ?>)</a></li>
      <li><a href="<?php echo e(URL::to('admin/user/status/0')); ?>">Blocked(<?php echo e(\App\User::where(['status' => 0])->get()->count()); ?>)</a></li>
	  <li><a href="<?php echo e(URL::to('admin/user/verified/1')); ?>">Verified(<?php echo e(\App\User::where(['verified' => 1])->get()->count()); ?>)</a></li>
	  <li><a href="<?php echo e(URL::to('admin/user/verified/0')); ?>">UnVerified(<?php echo e(\App\User::where(['verified' => 0])->get()->count()); ?>)</a></li>
	  
	  <li><a href="<?php echo e(URL::to('admin/reported_users')); ?>">Reported(<?php echo e(\App\ReportedUsers::get()->count()); ?>)</a></li>
 
    </ul>
   
  
</nav>


<input type="hidden"   value="<?php echo e(Session::token()); ?>" name="_token" id="token" >


                  <a href="<?php echo e(URL::to('admin/user/create')); ?>" class="pull-right btn btn-info btn-sm" style="display:none" ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
             
			
			
			
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 
	   
			  
               <!-- <div class="box-header">
                  <h3 class="box-title">User Listing</h3>
                </div><!-- /.box-header -->
                <?php if(Session::has('message')): ?>
                <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
		<?php echo $__env->make('admin.users.data-ajax', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
			   <?php echo $users->render(); ?>

			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
	  
 
		<?php echo e(csrf_field()); ?>

 
	


	  
 
 
 

     
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>