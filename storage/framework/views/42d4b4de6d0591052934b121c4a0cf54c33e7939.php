<?php $__env->startSection('content'); ?>
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-app="myApp">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <strong>Bitcoin Trades Posts</strong>
	 
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
         <li><a href="<?php echo e(URL::to('admins/admin')); ?>">Admins</a></li>
         <li class="active">Orders</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info" style="display:none"> 
            <nav class="navbar filters-nav">
               <!---  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="<?php echo e(URL::to('admins/admin/status/1')); ?>">Active(<?php echo e(\App\Admin::where(['status' => 1])->get()->count()); ?>)</a></li>
                    <li><a href="<?php echo e(URL::to('admins/admin/status/0')); ?>">Blocked(<?php echo e(\App\Admin::where(['status' => 0])->get()->count()); ?>)</a></li>
                  </ul> -->
            </nav> 
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <!--<div class="box-header">
                  <h3 class="box-title">Orders Listing</h3>
               </div>
               <!-- /.box-header -->
               <?php if(Session::has('message')): ?>
               <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>
               <div id="loading"></div>
               <style>
                  #loading {
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  -webkit-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
                  }
               </style>
               <div id="item-lists">
                  <?php echo $__env->make('admin.trades.data-ajax', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
               </div>
               <?php echo $result->render(); ?>

            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>