<?php $__env->startSection('content'); ?>

</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" ng-app="myApp">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Reported Posts</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo e(URL::to('admins/admin')); ?>">Admins</a></li>
            <li class="active">Posts</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info">
                <div class="box-header" style="display:none">
				                  		<nav class="navbar navbar-default">
  <!---<div class="container-fluid">
    <div class="navbar-header">
      <b class="navbar-brand" >Filter By Status :</b>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="<?php echo e(URL::to('admins/admin/status/1')); ?>">Active(<?php echo e(\App\Admin::where(['status' => 1])->get()->count()); ?>)</a></li>
      <li><a href="<?php echo e(URL::to('admins/admin/status/0')); ?>">Blocked(<?php echo e(\App\Admin::where(['status' => 0])->get()->count()); ?>)</a></li>
    </ul>
   
  </div>-->
</nav>
 
                </div><!-- /.box-header -->
            </div>
			
			
			
	
	
 
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 


 
			  
               <!-- <div class="box-header">
                  <h3 class="box-title">Offers Listing</h3>
                </div><!-- /.box-header -->
                <?php if(Session::has('message')): ?>
                <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
						
					
		                <div class="box-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					    <th>Post Details</th>
                        <th>Reported By</th>
                        <th>Type</th>
						<th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($posts as $key => $value) { 
					 
					 $post_id = $value['post_details']['id'];
					 ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td>

					 <a href="<?php echo e(URL::to('posts_list/'.$post_id.'/edit')); ?>">Post Details </a>
					 
					 <p>
<?php if($value['post_image']['image'] !='') { ?> <img src="<?php if($value['post_image']['image'] != '') 
{ echo url('/').'/users/'.$value['post_image']['image']; }?>" style="max-height:140px"   class="img-thumbnail"> <?php } 
else { echo '<span>No Image</span>';}?>

<?php echo $value['post_details']['title'];?>
</p>

</td> 
					<td style="max-width: 100px;overflow: hidden; text-overflow: ellipsis;   white-space: nowrap;"><?php echo $value['reported_by_user_details']['first_name']." ".$value['reported_by_user_details']['last_name']; ?>
					</br><?php echo $value['reported_by_user_details']['email']; ?>
					</td>
                    <td><?php
                   	   if($value->type == 0)
                   	 	{
                   	 	  echo "Normal";
                   	    }
                   	    else
                   	    {
                   	      echo "Multiple Free";
                   		} 
						?>
                   	</td>
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td class="action-btn">
                   <a class="btn btn-small " title="Show Post" href="<?php echo e(URL::to('/posts_list/'.$post_id.'/edit')); ?>"><i class="fa fa-eye"></i></a>               
                   <a class="btn btn-small " title="Edit User" href="<?php echo e(URL::to('/posts_list/'.$post_id.'/edit')); ?>"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small " title="Delete User" href="<?php echo e(URL::to('/posts_list/'.$post_id.'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

			 
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                
	
			   <?php echo $posts->render(); ?>

			  </div> 
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
 
     
	 
	 
	 
 
	 
	 
	 
	 
	 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body" ng-controller="mainCtrl">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>