<?php $__env->startSection('content'); ?>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
 
<script>tinymce.init({
  selector: "textarea",  // change this value according to your HTML
  
 
});</script>

 


</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		
          <h1>
            <strong>Settings</strong>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
            
 <div class="row">
            <div class="col-xs-12">
              <div class="box">
             
                <?php if(Session::has('message')): ?>
                <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>

            
			
			<ul class="nav nav-tabs nav-tabs1">
  <?php if(auth()->user()->user_type == 'admin')
			{?><li class="active"><a data-toggle="tab" href="#terms">Terms & Conditions</a></li>	<?php } ?>
  <?php if(auth()->user()->user_type == 'admin')
			{?><li><a  href="<?php echo e(URL::asset('admin/app_data')); ?>">Application Data</a></li>	<?php } ?>
  <?php if(auth()->user()->user_type == 'admin')
			{?><li><a  href="<?php echo e(URL::asset('admin/pop_up_content')); ?>">App HomePage Notice</a></li>	<?php } ?>
</ul>

<div class="tab-content">
  <div id="terms" class="tab-pane fade in active">
      <div class="panel ">
<br>
                 <!--- <div class="panel-heading">Terms and Conditions</div>-->
                  
                  <div class="panel-body">
                  <?php echo e(Form::open(array('url' => 'admin/terms_conditions_update','class'=>'form-horizontal'))); ?>

                  
				  <textarea class="description" rows="10" name="terms_conditions"><?php echo e($content); ?></textarea>
                  
		          <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
				  
                  <?php echo e(Form::close()); ?>

                  </div>

            </div>
  </div> 
</div>




          

       
 

			   
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>