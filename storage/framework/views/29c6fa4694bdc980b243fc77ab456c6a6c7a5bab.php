                <div class="box-body">
                  <table id="example1" class="table  table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					    <th>Id</th>
                        <th>User</th>
                        <th>Seller</th>
                        <th>Price</th>
						<th>Product</th>
						<th>Status</th>
						<th id="action">Created</th> 
                        <th id="action" style="display:none">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($result as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
				 
					<td><?php echo '<a href="order_details/'.$value['id'].'">#'.$value['id']."</a>"; ?></td>
					<td><?php echo $value['user_name']; ?></td>
                    <td style=""><?php echo $value['seller_name']; ?></td>
					<td style=" "><?php echo $value['order_currency']."".$value['price']; ?></td>
					<td style=" "><?php echo $value['post_title'] ; ?></td>
					<td style=" "><?php echo $value['status'] ; ?></td>
                 
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td class="action-btn" style="display:none">
                               
                   <a class="btn btn-small " title="Edit User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/edit')); ?>"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                