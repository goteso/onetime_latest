 <div class="wrapper">
<header class="main-header">
        <!-- Logo -->
        <a href="<?php echo e(URL::to('admin/dashboard')); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?php echo e(env('APP_NAME')); ?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo e(env('APP_NAME')); ?> 
		  <?php
		  if(Auth::user()->user_type=='brand') {echo 'BRAND';}
		  if(Auth::user()->user_type=='store') {echo 'STORE';}
		  if(Auth::user()->user_type=='admin') {echo 'ADMIN';}
		  
		  ?>
		 
		  
		  </b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="<?php echo e(URL::to('admin/dashboard')); ?>" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo e(URL::asset('backend/dist/img/76-x-76.png')); ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs">		  <?php
		  if(Auth::user()->user_type=='brand') {echo 'BRAND';}
		  if(Auth::user()->user_type=='store') {echo 'STORE';}
		  if(Auth::user()->user_type=='admin') {echo 'ADMIN';}
		  
		  ?></span>
                </a>
				
				
				<ul class="dropdown-menu header-dropdown">
                   <li class=" ">
                    <a href="<?php echo e(URL::to('admin/change-password')); ?>" class="btn btn-default btn-flat"><i class="fa fa-key"></i>Change Password</a>
                  </li>
                  <li class=" "> 
                      <a href="<?php echo e(URL::to('admin/logout')); ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>Sign out</a> 
                  </li>
                </ul>
				
				
               <!-- <ul class="dropdown-menu">
                                    <li class="user-header">
                    <img src="<?php echo e(URL::asset('backend/dist/img/76-x-76.png')); ?>" class="img-circle" alt="User Image">
                    <p>
                      Admin
                     
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo e(URL::to('admin/change-password')); ?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo e(URL::to('admin/logout')); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul> -->
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>