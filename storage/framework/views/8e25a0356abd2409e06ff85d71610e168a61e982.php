<!-- Stored in resources/views/layouts/admin.blade.php -->
<!DOCTYPE html>
<html>
        <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="hold-transition skin-black-light sidebar-mini" >
        <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('includes.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>          
        <?php echo $__env->yieldContent('content'); ?>        
       
    </body>
	
	<style>.breadcrumb{ display:none;}</style>
	<script type="text/javascript">

	
 <?php $img = URL::asset('backend/dist/img/ajax-loader.gif');  ?> 

 
$(window).on('hashchange', function() {
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        }else{
           // getData(page);
        }
    }
});


$(document).ready(function()
{
    $(document).on('click', '.pagination a',function(event)
    {
		//alert("helo1");
		     if (page != 1) { //Fix for page 1 because it doesn't get a link with ajax
           $('.pagination li:nth-child(2) > span').replaceWith('<a href="?page=1">1</a>');
  $('.pagination li').removeClass("active");
            $('.pagination li').removeClass("disabled");
     
			event.preventDefault();  
            }
		 
		$('#loading').html('<img src="<?php echo $img;?>">');
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        


        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
 var r = getData(page);
event.preventDefault();
 
    });
});

 


function getData(page){
	  
        $.ajax(
        {
            url: '?page='+page,
            type: "get",
            datatype: "html",
        })
        .done(function(data)
        {
            $("#item-lists").empty().html(data);
            location.hash = page;
			//alert("done");
			$('#loading').html("");
			return 'done';
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
			  $('#loading').html("");
        });
	 
}


</script>
</html>