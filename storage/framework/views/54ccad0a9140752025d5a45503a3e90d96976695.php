<?php $__env->startSection('content'); ?>

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Reported Users</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo e(URL::to('admin/user')); ?>">Users</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <!-- Main content -->
        <section class="content">
            <div class="box ">
                
				 <nav class="navbar filters-nav">
 
    <div class="navbar-header">
      <div class="navbar-brand" >Filter By Status :</div>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="<?php echo e(URL::to('admin/user/status/1')); ?>">Active(<?php echo e(\App\User::where(['status' => 1])->get()->count()); ?>)</a></li>
      <li><a href="<?php echo e(URL::to('admin/user/status/0')); ?>">Blocked(<?php echo e(\App\User::where(['status' => 0])->get()->count()); ?>)</a></li>
	  <li><a href="<?php echo e(URL::to('admin/user/verified/1')); ?>">Verified(<?php echo e(\App\User::where(['verified' => 1])->get()->count()); ?>)</a></li>
	  <li><a href="<?php echo e(URL::to('admin/user/verified/0')); ?>">UnVerified(<?php echo e(\App\User::where(['verified' => 0])->get()->count()); ?>)</a></li>
	  
	  <li><a href="<?php echo e(URL::to('admin/reported_users')); ?>">Reported(<?php echo e(\App\ReportedUsers::get()->count()); ?>)</a></li>
 
    </ul>
   
  
</nav>


<input type="hidden"   value="<?php echo e(Session::token()); ?>" name="_token" id="token" >


                  <a href="<?php echo e(URL::to('admin/user/create')); ?>" class="pull-right btn btn-info btn-sm" style="display:none" ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
             
			
			
			
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 
	   
			  
               <!-- <div class="box-header">
                  <h3 class="box-title">User Listing</h3>
                </div><!-- /.box-header -->
                <?php if(Session::has('message')): ?>
                <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
		                <div class="box-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Reported User</th>
					   
						<th>Status</th>
						<th>Reported By</th>
					    <th>Created</th>
                        <th id="action" style="display: ">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php if($value['reported_user_details']['profile_image'] !='') { ?> <img src="<?php if($value['reported_user_details']['profile_image'] != '') { echo url('/').'/users/'.$value['reported_user_details']['profile_image']; }?>" style="max-height:140px"   class="img-thumbnail"> <?php } else { echo '<span>No Image</span>';}?></br>
					<a href="<?php echo e(URL::to('admin/user/show/'.$value['reported_user_id'])); ?>" ><?php echo $value['reported_user_details']['first_name']." ".$value['reported_user_details']['last_name']; ?></a></br>
					<?php echo $value['reported_user_details']['email']; ?></td>
					
					          <td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Inactive";
                   	   }
                   	   else
                   	   {
                   	    echo "Active";
                   		} ?>
                   	</td>
					
					
					
					<td><?php if($value['reported_by_user_details']['profile_image'] !='') { ?> <img src="<?php if($value['reported_by_user_details']['profile_image'] != '') { echo url('/').'/users/'.$value['reported_by_user_details']['profile_image']; }?>" style="max-height:140px"   class="img-thumbnail"> <?php } else { echo '<span>No Image</span>';}?></br>
					<a href="<?php echo e(URL::to('admin/user/show/'.$value['user_id'])); ?>" ><?php echo $value['reported_by_user_details']['first_name']." ".$value['reported_by_user_details']['last_name']; ?></a></br>
					<?php echo $value['reported_by_user_details']['email']; ?></td>
					
					
                     
               
          
			 
 
				   
                    <td><?php echo $value['created_at']; ?></td>
					
					
					
					
					
				
                    <td class="action-btn">
                     <a class="btn btn-small  " title="View User" href="<?php echo e(URL::to('admin/user/show/'.$value['reported_user_id'])); ?>"  target="_blank" ><i class="fa fa-eye"></i></a>           
                   <a class="btn btn-small  " title="Edit User" href="<?php echo e(URL::to('admin/user/'.$value['reported_user_id'].'/edit')); ?>" target="_blank"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="<?php echo e(URL::to('admin/user/'.$value['reported_user_id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small  " title="Block User" href="<?php echo e(URL::to('admin/user/'.$value['reported_user_id'].'/block')); ?>" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small  " title="Unblock User" href="<?php echo e(URL::to('admin/user/'.$value['reported_user_id'].'/unblock')); ?>" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
				  
				  
				 
 
            	   </td>
                   	 </tr>
					 
					 

                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                
	</div>
			   
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
	  
 
		<?php echo e(csrf_field()); ?>

 
	


	  
 
 
 

     
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>