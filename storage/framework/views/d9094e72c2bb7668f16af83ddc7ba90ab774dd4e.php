                <div class="box-body">
   
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					   
                        <th>User</th>
                        <th>Range</th>
						<th>Country</th>
                        <th>Created</th>
                        <th id="action" style="display:none">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>
				   
				

                   	 <?php $sr = 0; foreach($result as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					 <td ><?php echo $value['user_name']; ?></td>
                    <td ><?php echo $value['range_min']." - ".$value['range_max']; ?></td>
                   
                   	<td ><?php echo  $value['country']; ?></td> 
                  
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td style="display:none">
                               
                   <a class="btn btn-small btn-info" title="Edit User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/edit')); ?>"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                
				
 