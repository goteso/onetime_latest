<?php $__env->startSection('content'); ?>

 
 

 


</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Settings</strong>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
           <div class="container-fluid">
 <div class="row">
            <div class="col-xs-12">
              <div class="box">
             
                <?php if(Session::has('message')): ?>
                <p class="alert alert-success"><?php echo e(Session::get('message')); ?></p>
               <?php endif; ?>

	 

    <div class="row"  >

     
	<ul class="nav nav-tabs nav-tabs1">
  <?php if(auth()->user()->user_type == 'admin')
			{?><li><a  href="<?php echo e(URL::asset('admin/terms_conditions')); ?>">Terms & Conditions</a></li>	<?php } ?>
 <?php if(auth()->user()->user_type == 'admin')
			{?> <li class="active"><a data-toggle="tab" href="#app-data">Application Data</a></li>	<?php } ?>
 <?php if(auth()->user()->user_type == 'admin')
			{?> <li><a  href="<?php echo e(URL::asset('admin/pop_up_content')); ?>">App HomePage Notice</a></li>	<?php } ?>
</ul>

<div class="tab-content">
  <div id="app-data" class="tab-pane fade in active">
            <div class="panel  ">
<br>
            <!--    <div class="panel-heading">Update Application Data</div>-->
				 <div class="row  panel-body">
                   <div class="col-sm-7  ">
				    <div class="row  ">
		  <?php echo e(Form::open(array('url' => 'admin/app_data_update','class'=>'form-horizontal'))); ?>

				<?php
				
				foreach($content as $key)
				{
					?>
					
					 
					
					    <div class="col-md-12" style=" margin-bottom:2%" >
						<label><?php  echo   str_replace("_"," ",title_case($key["key_name"])) ?></label>
						  <input class="form-control" name="<?php echo $key["key_name"]?>" value="<?php echo $key["value"]?>">
                       </div> 
                   				<?php
				}
				
				
				
				?>    
				</div>
				</div>
					    <div class="col-sm-5 text-right">
                            <button type="submit" class="btn btn-primary"  style="margin-top:2%;margin-bottom:2%">Submit</button>
						</div>
					</div>
                 
                
            
					
					
					
	
				    <?php echo e(Form::close()); ?>

                <div class="panel-body">
    
                </div>

            </div>

     </div>

            </div>

     

</div>

			   </div>
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>