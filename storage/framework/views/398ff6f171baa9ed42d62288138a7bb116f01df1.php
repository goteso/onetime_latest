                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
						<th>Trade details</th>
					    <th>Trade Post details</th>
                        <th>Seller</th>
                        <th>Buyer</th>
						<th>Currency Amount</th>
                        <th>BTC Amount</th>
						<th>Created</th>
                        <th id="action" style="display:none">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($result as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td><?php echo '<a href="trades_details/'.$value['id'].'">#'.$value['id']."</a>"; ?></td>
					<td><?php echo '<a href="trades_posts_details/'.$value['trades_posts_id'].'">Trades Posts</a>'; ?></td>
			 
                    <td style=""><?php echo $value['seller_name']; ?></td>
					 <td style=""><?php echo $value['buyer_name']; ?></td>
					 <td style=""><?php echo $value['trades_posts_currency'].$value['other_amount']; ?></td>
					 <td style=""><?php echo $value['btc_amount']; ?></td>
                     <td ><?php echo $value['created_at']; ?></td>
                    <td style="display:none">
                               
                   <a class="btn btn-small btn-info" title="Edit User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/edit')); ?>"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete User" href="<?php echo e(URL::to('/posts_list/'.$value['id'].'/delete')); ?>" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                