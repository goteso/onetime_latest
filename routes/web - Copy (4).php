<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/cache', function () {
    return Cache::get('key');
});


Route::get('/admin_login', function () {
    return view('admin.login.index');
});

// route to show the login form for admin section
Route::group(array('namespace'=>'Admin'), function()
{
    Route::get('/admin_login', array('as' => 'admin', 'uses' => 'LoginController@index'));
    Route::post('/login_post',  array('as' => 'admin', 'uses' => 'LoginController@login_post'));
    Route::get('/admin/logout', array( 'middleware' => 'auth', 'uses' => 'LoginController@logout'));
    Route::get('/admin/dashboard', array( 'middleware' => 'auth', 'uses' => 'DashController@index'));
    Route::get('/admin/change-password', array('middleware' => 'App\Http\Middleware\Role', 'uses' => 'DashController@change_password'));
    Route::put('/admin/change-password', array('middleware' => 'App\Http\Middleware\Role','uses' => 'DashController@update_password'));

	
	 
	Route::post('/admin/ajax_users_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_users_data'));
	Route::post('/admin/ajax_likes_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_likes_data'));
	Route::post('/admin/get_countries_data', array( 'middleware' => 'auth', 'uses' => 'DashController@get_countries_data'));
	Route::post('/admin/get_genders_data', array( 'middleware' => 'auth', 'uses' => 'DashController@get_genders_data'));
	
	
	Route::post('/admin/ajax_transaction_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_transaction_data'));
 
	//Users
   Route::resource('user', 'UsersController');
   Route::get('/admin/user', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@index'));
   Route::get('/admin/user/status/{status}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_status'));
   Route::get('/admin/user/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_verified'));
   Route::get('/admin/user/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_verified'));
   
   Route::get('/admin/user/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@create'));
   Route::post('/admin/user', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@store'));
   Route::get('/admin/user/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@show'));
   Route::get('/admin/user/verify/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@verify'));
   Route::get('/admin/user/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@edit'));
   Route::put('/admin/user/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@update'));
   Route::get('/admin/user/{id}/delete', array('uses' => 'UsersController@destroy'));
   Route::get('/admin/user/{id}/block/', array('uses' => 'UsersController@block'));
   Route::get('/admin/user/{id}/unblock/', array('uses' => 'UsersController@unblock'));
   Route::get('/admin/user/{id}/verify/', array('uses' => 'UsersController@verify'));
   Route::get('/admin/user/{id}/unverify/', array('uses' => 'UsersController@unverify'));
   
   
   Route::get('/admin/reported_users/', array('uses' => 'UsersController@reported_users'));
   
 
   
   Route::get('/admin/user/show/{id}', array('uses' => 'UsersController@show'));
   
   
   
   
   
   
   
   
   
   	//Admin Users
   Route::resource('admins', 'AdminController');
   Route::get('/admins/admin/status/{status}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@user_by_status'));
   Route::get('/admins/admin/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@user_by_verified'));
   Route::get('/admins/admin/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@create'));
   Route::post('/admins/admin', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@store'));
   Route::get('/admins/admin/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@show'));
   Route::get('/admins/admin/verify/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@verify'));
   Route::get('/admins/admin/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@edit'));
   Route::put('/admins/admin/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@update'));
   Route::get('/admins/admin/{id}/delete', array('uses' => 'AdminController@destroy'));
   Route::get('/admins/admin/{id}/block/', array('uses' => 'AdminController@block'));
   Route::get('/admins/admin/{id}/unblock/', array('uses' => 'AdminController@unblock'));
   
   

   
   
   
   
  //posts 
   Route::get('posts_list', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@index'))->name('posts_list');
   Route::get('/posts_list/create/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@create'));
   Route::get('/posts_list/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@edit_post'))->name('/posts_list/edit');
   Route::post('/posts_list/update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@update_post'))->name('/posts_list/update');
   Route::get('/posts_list/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy'));
   
   Route::get('/posts_list/images/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy_post_images'));
   Route::get('/posts_list/comments/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy_post_comments'));
   
   
   
   
    Route::resource('pop_up_content', 'TermsConditionsController');
    Route::get('/admin/pop_up_content', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppPopUpController@index'));
	Route::post('/admin/app_pop_up_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppPopUpController@pop_up_content_update'));

    //purchase points
    Route::resource('terms_conditions', 'TermsConditionsController');
    Route::get('/admin/terms_conditions', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@index'));
	Route::post('/admin/terms_conditions_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@terms_conditions_update'));
	
	
	
	
    //Membership Card Types
    Route::resource('membership_card_types', 'MemberShipCardTypesController');
    Route::get('/admin/membership_card_types', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@index'));//show the list of records
	
	Route::get('/admin/membership_card_types/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@create')); //show add form
    Route::post('/admin/membership_card_types/store', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@store'))->name("membership_card_types_store"); // add data to database
    
	Route::get('/admin/membership_card_types/{id}/delete', array('uses' => 'MemberShipCardTypesController@destroy'));//delete
	
	Route::get('/admin/membership_card_types/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@edit'));//show edit form
    Route::post('/admin/membership_card_types/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@update'))->name("membership_card_types_update");;//update to database
	
	
	
	
	Route::post('/admin/terms_conditions_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@terms_conditions_update'));
	
	//App Data
	    Route::resource('app_data', 'AppDataController');
    Route::get('/admin/app_data', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppDataController@index'));
	Route::post('/admin/app_data_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppDataController@app_data_update'));
 
	//Auctions
    Route::resource('country', 'CountryController');
    Route::get('/admin/country', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@index'));
    Route::get('/admin/country/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@create'));
    Route::post('/admin/country', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@store'));
    Route::get('/admin/country/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@show'));
    Route::get('/admin/country/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@edit'));
    Route::put('/admin/country/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@update'));
    Route::get('/admin/country/{id}/delete', array('uses' => 'CountryController@destroy'));
	
	
	
	
	
	//this will return page where all the commissions transactions will show
	Route::get('/admin/earned_commisions', array('uses' => 'TransactionsController@earned_commisions'));
	
	
	//this will return page where all the commissions transactions will show
	Route::get('/admin/reported_posts', array('uses' => 'PostsController@reported_posts'));
	
	
	//mail templates
Route::get('/email_otp', function () {
    return view('emails.otp');
});
	
	
	
	// New admin Services 
	
	   Route::get('/admin/post_orders/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@post_orders'));
	   
	   Route::get('/admin/trades/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@trades'));
	   
	   Route::get('/admin/trades_orders/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@trades_orders'));
	   
	   Route::get('/admin/disputed_orders/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@disputed_orders'));
	   
	   
	   //users orders
        Route::get('/admin/users_orders/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_orders'));
		Route::get('/admin/users_trades/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_trades'));
		Route::get('/admin/users_transactions/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_transactions'));
		Route::get('/admin/users_posts/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_posts'));
		Route::get('/admin/users_trades_posts/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_trades_posts'));
		Route::get('/admin/trades_posts_details/{trades_posts_id}/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@trades_posts_details'));
		
		
		
		
		
		
		//trades details from page link from pages in sidebar
			Route::get('/admin/trades_posts_details/{trades_posts_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@trades_posts_details'));
			Route::get('/admin/trades_details/{trades_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@trades_details'));
			Route::get('/admin/order_details/{order_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@order_details'));
	 
 
 
   
	
	
});