<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/coinbase', array('as' => 'coinbase', 'uses' => 'Api\CountryController@coinbase'));

Route::post('register', 'API\RegisterController@register');


	Route::middleware('auth:api')->group(  function () {
 
	 
	Route::post('/user_login_details', array('as' => 'user_login_details', 'uses' => 'Api\UsersController@user_login_details'));
	  
		Route::post('details', 'Api\UsersController@details');
		
	
});


Route::post('/get_tokens', array('as' => 'get_tokens', 'uses' => 'Api\UsersController@get_tokens'));



Route::post('/authWithFacebook', array('as' => 'authWithFacebook', 'uses' => 'SocialUserResolver@authWithFacebook'));
Route::group(array('namespace'=>'Api'    ), function()
{
	Route::post('/signup', array('as' => 'signup', 'uses' => 'UsersController@register'));
    Route::post('/login', array('as' => 'login', 'uses' => 'UsersController@login'));
	Route::post('/facebook_signup_login', array('as' => 'facebook_signup_login', 'uses' => 'UsersController@register'));
	Route::post('/login_fb', array('as' => 'login_fb', 'uses' => 'UsersController@login_fb'));
	Route::post('/login_google', array('as' => 'login_google', 'uses' => 'UsersController@login_google'));
	Route::post('/forgot_password', array('as' => 'forgot_password', 'uses' => 'UsersController@forgotPasswordEmail'));
	Route::post('/verify_otp', array('as' => 'verify_otp', 'uses' => 'UsersController@verify_otp'));
	Route::post('/forgot_password_change', array('as' => 'forgot_password_change', 'uses' => 'UsersController@forgotPasswordChange'));
	
    Route::post('/upload_verify_images', array('as' => 'upload_verify_images', 'uses' => 'UsersController@upload_verify_images'));
    Route::post('/upload_profile_image', array('as' => 'upload_profile_image', 'uses' => 'UsersController@upload_profile_image'));
	Route::post('/upload_user_card', array('as' => 'upload_user_card', 'uses' => 'AppDataController@upload_user_card'));
	 
	Route::post('/add_post', array('as' => 'add_post', 'uses' => 'PostsController@add_post'));
	Route::post('/check_facebook_id_existence', array('as' => 'check_facebook_id_existence', 'uses' => 'UsersController@check_facebook_id_existence'));

Route::post('/coinbase_notification', array('as' => 'coinbase_notification', 'uses' => 'CountryController@coinbase_notification'));
	
 
    //terms_conditions
    Route::post('/get_terms_conditions', array('as' => 'get_terms_conditions', 'uses' => 'TermsConditionsController@get_terms_conditions'));
	
});





// route to show the login form for api section
Route::group(array('namespace'=>'Api' , 'middleware'=>'auth:api'), function()
//Route::group(array('namespace'=>'Api' ), function()
{
    Route::post('/traits', array('as' => 'traits', 'uses' => 'UsersController@traits'));
    Route::get('/get_countries', array('as' => 'get_country', 'uses' => 'CountryController@get_countries'));
	Route::get('/get_currency_codes', array('as' => 'get_currency_codes', 'uses' => 'CountryController@get_currency_codes')); // NEW API
	Route::post('/currency_bitcoin_price', array('as' => 'currency_bitcoin_price', 'uses' => 'BitcoinController@currency_bitcoin_price')); // NEW API
	Route::post('/currency_amount_bitcoin_price', array('as' => 'currency_amount_bitcoin_price', 'uses' => 'BitcoinController@currency_amount_bitcoin_price')); // NEW API
	Route::post('/update_order_status', array('as' => 'update_order_status', 'uses' => 'PostsController@update_order_status')); // NEW API
	
	Route::post('/update_trade_order_status', array('as' => 'update_trade_order_status', 'uses' => 'WalletController@update_trade_order_status')); // NEW API
	
	Route::post('get_coinbase_pending_transaction', array('as' => 'get_coinbase_pending_transaction', 'uses' => 'UsersController@get_coinbase_pending_transaction')); // NEW API
	
	
	Route::post('/search_posts', array('as' => 'search_posts', 'uses' => 'PostsController@search_posts')); // NEW API
	Route::post('/add_orders_feedback', array('as' => 'add_orders_feedback', 'uses' => 'PostsController@add_orders_feedback')); // NEW API
	Route::post('/add_trades_feedback', array('as' => 'add_trades_feedback', 'uses' => 'WalletController@add_trades_feedback')); // NEW API
	
	
	
    Route::post('/get_init_data', array('as' => 'get_init_data', 'uses' => 'AppDataController@get_init_data'));
    Route::get('/notification', array('as' => 'notification', 'uses' => 'CustomController@sendNotificationToDevice'));
 
 
 
	//services on 21 april
    Route::post('/check_pgp_key_exist', array('as' => 'check_pgp_key_exist', 'uses' => 'UsersController@check_pgp_key_exist'));	
	Route::post('/get_2fa_otp_message', array('as' => 'get_2fa_otp_message', 'uses' => 'UsersController@get_2fa_otp_message'));
	Route::post('/update_two_fa_status', array('as' => 'update_two_fa_status', 'uses' => 'UsersController@update_two_fa_status'));
	Route::post('/gpg_encryption_decryption', array('as' => 'gpg_encryption_decryption', 'uses' => 'BitcoinController@gpg_encryption_decryption'));
	Route::post('/two_fa_login', array('as' => 'two_fa_login', 'uses' => 'UsersController@two_fa_login'));
	
	
   
    //Users  basic api's
 
    Route::post('/update_pgp_key', array('as' => 'update_pgp_key', 'uses' => 'UsersController@update_pgp_key'));
 
	Route::post('/update_wallet_address', array('as' => 'update_wallet_address', 'uses' => 'UsersController@update_wallet_address'));
	Route::post('/update_wallet_pin', array('as' => 'update_wallet_pin', 'uses' => 'UsersController@update_wallet_pin'));
	
	
	//Posts  basic api's

	Route::post('/edit_post', array('as' => 'edit_post', 'uses' => 'PostsController@edit_post'));
	Route::post('/delete_post', array('as' => 'add_post', 'uses' => 'PostsController@delete_post'));
	Route::post('/get_posts', array('as' => 'get_posts', 'uses' => 'PostsController@get_posts'));
	
	
	Route::post('/write_post_comment', array('as' => 'write_post_comment', 'uses' => 'PostsController@write_post_comment'));
	Route::post('/get_post_comment', array('as' => 'get_post_comment', 'uses' => 'PostsController@get_post_comment'));
	
	Route::post('/write_users_reviews', array('as' => 'write_users_reviews', 'uses' => 'PostsController@write_users_reviews'));
	Route::post('/get_users_reviews', array('as' => 'get_users_reviews', 'uses' => 'PostsController@get_users_reviews'));
	
	//manage listings Api's starts
	
	Route::post('/my_listings', array('as' => 'my_listings', 'uses' => 'PostsController@my_listings'));
Route::post('/delete_posts', array('as' => 'delete_posts', 'uses' => 'PostsController@delete_posts'));




	Route::post('/my_buyings', array('as' => 'my_buyings', 'uses' => 'PostsController@my_buyings'));
	Route::post('/my_sellings', array('as' => 'my_sellings', 'uses' => 'PostsController@my_sellings'));
	
	Route::post('/order_details', array('as' => 'order_details', 'uses' => 'PostsController@order_details'));
		
	Route::post('/like_post', array('as' => 'like_post', 'uses' => 'PostsController@like_post'));
	Route::post('/unlike_post', array('as' => 'unlike_post', 'uses' => 'PostsController@unlike_post'));
	Route::post('/get_post_detail', array('as' => 'get_post_detail', 'uses' => 'PostsController@get_post_detail'));
	Route::post('/buy_post', array('as' => 'buy_post', 'uses' => 'PostsController@buy_post'));
	 
	
	
	
	//trades & bitcoins apis
	Route::post('/withdraw_bitcoins', array('as' => 'withdraw_bitcoins', 'uses' => 'WalletController@withdraw_bitcoins'));
	Route::post('/onetime_payment', array('as' => 'onetime_payment', 'uses' => 'WalletController@onetime_payment'));
	Route::post('/add_trade', array('as' => 'add_trade', 'uses' => 'WalletController@add_trade'));
	Route::post('/edit_trade', array('as' => 'edit_trade', 'uses' => 'WalletController@edit_trade'));
	Route::post('/delete_trade', array('as' => 'delete_trade', 'uses' => 'WalletController@delete_trade'));
	Route::post('/trade_details', array('as' => 'trade_details', 'uses' => 'WalletController@trade_details'));
	Route::post('/trade_post_details', array('as' => 'trade_post_details', 'uses' => 'WalletController@trade_post_details'));
		
		
	Route::post('/buy_bitcoins', array('as' => 'buy_bitcoins', 'uses' => 'WalletController@buy_bitcoins'));
	Route::post('/sell_bitcoins', array('as' => 'sell_bitcoins', 'uses' => 'WalletController@sell_bitcoins'));
	
	
	
	Route::post('/get_sell_trades_posts', array('as' => 'get_sell_trades_posts', 'uses' => 'WalletController@get_sell_trades_posts'));
	Route::post('/get_buy_trades_posts', array('as' => 'get_buy_trades_posts', 'uses' => 'WalletController@get_buy_trades_posts'));
	
	Route::post('/get_sell_my_trades_posts', array('as' => 'get_sell_my_trades_posts', 'uses' => 'WalletController@get_sell_my_trades_posts'));
	Route::post('/get_buy_my_trades_posts', array('as' => 'get_buy_my_trades_posts', 'uses' => 'WalletController@get_buy_my_trades_posts'));
	Route::post('/get_user_bitcoin_balance', array('as' => 'get_user_bitcoin_balance', 'uses' => 'WalletController@get_user_bitcoin_balance'));
	Route::post('/my_transaction_history', array('as' => 'my_transaction_history', 'uses' => 'WalletController@my_transaction_history'));
	Route::post('/delete_transaction_history', array('as' => 'delete_transaction_history', 'uses' => 'WalletController@delete_transaction_history'));
	
	Route::post('/send_messages', array('as' => 'send_messages', 'uses' => 'WalletController@send_messages'));
	Route::post('/get_messages', array('as' => 'get_messages', 'uses' => 'WalletController@get_messages'));
	Route::post('/get_chats_lists', array('as' => 'get_chats_lists', 'uses' => 'WalletController@get_chats_lists'));
	
	
	
	Route::post('/trade_list', array('as' => 'trade_list', 'uses' => 'WalletController@trade_list'));
	
 
 
	 
	
 

	
	
	
		Route::post('/get_offers', array('as' => 'get_offers', 'uses' => 'PostsController@get_offers'));
		Route::post('/get_offers_details', array('as' => 'get_offers', 'uses' => 'PostsController@get_offers_details'));
		Route::post('/get_notifications', array('as' => 'get_notifications', 'uses' => 'UsersController@get_notification'));
		Route::post('/get_facebook_friends', array('as' => 'get_facebook_friends', 'uses' => 'UsersController@get_facebook_friends'));

		
		
		Route::post('/like_offer', array('as' => 'like_offer', 'uses' => 'PostsController@like_offer'));
		Route::post('/unlike_offer', array('as' => 'unlike_offer', 'uses' => 'PostsController@unlike_offer'));
		Route::post('/get_active_loyaty', array('as' => 'get_active_loyaty', 'uses' => 'PostsController@get_active_loyaty'));
		
		
		Route::post('/follow_user', array('as' => 'follow_user', 'uses' => 'UsersController@follow_user'));
		Route::post('/unfollow_user', array('as' => 'unfollow_user', 'uses' => 'UsersController@unfollow_user'));
		
		
		Route::post('/follow_store', array('as' => 'follow_store', 'uses' => 'UsersController@follow_store'));
		Route::post('/unfollow_store', array('as' => 'unfollow_store', 'uses' => 'UsersController@unfollow_store'));
		
		
		
		Route::post('/get_notification', array('as' => 'get_notification', 'uses' => 'UsersController@get_notification'));
		
		Route::post('/get_membership_list', array('as' => 'get_membership_list', 'uses' => 'UsersController@get_membership_list'));
		Route::post('/update_membership_list', array('as' => 'update_membership_list', 'uses' => 'UsersController@update_membership_list'));
		Route::post('/delete_membership_list', array('as' => 'delete_membership_list', 'uses' => 'UsersController@delete_membership_list'));
		
		Route::post('/search_store', array('as' => 'search_store', 'uses' => 'UsersController@search_store'));
		
 
		

	Route::post('/change_password', array('as' => 'change_password', 'uses' => 'UsersController@newPassword'));
	
	
    Route::post('/get_user_profile', array('as' => 'get_user_profile', 'uses' => 'UsersController@get_user_profile'));
	Route::post('/get_other_user_profile', array('as' => 'get_other_user_profile', 'uses' => 'UsersController@get_other_user_profile'));
	Route::post('/get_other_user_sales_posts', array('as' => 'get_other_user_sales_posts', 'uses' => 'UsersController@get_other_user_sales_posts'));
	
	
	
	Route::post('/update_profile', array('as' => 'update_profile', 'uses' => 'UsersController@update_profile'));
	Route::post('/get_social_links', array('as' => 'get_social_links', 'uses' => 'UserSocialLinksController@get_social_links'));
	Route::post('/update_social_links', array('as' => 'update_social_links', 'uses' => 'UserSocialLinksController@update_social_links'));
	Route::post('/get_push_notification_status', array('as' => 'get_push_notification_status', 'uses' => 'UsersController@get_push_notification_status'));
	Route::post('/update_push_notification_status', array('as' => 'update_push_notification_status', 'uses' => 'UsersController@update_push_notification_status'));
 
	Route::post('/search_users', array('as' => 'search_users', 'uses' => 'UsersController@search_users'));
	
	
 
	 
	
	 Route::post('/store_user_cards', array('as' => 'store_user_cards', 'uses' => 'UsersController@store_user_cards'));
	 
	 
	 Route::post('/get_followers', array('as' => 'get_followers', 'uses' => 'UsersController@get_followers'));
	 Route::post('/get_followings', array('as' => 'get_followings', 'uses' => 'UsersController@get_followings'));
	 Route::post('/get_follow_requests', array('as' => 'get_follow_requests', 'uses' => 'UsersController@get_follow_requests'));
	 Route::post('/accept_reject_follow_request', array('as' => 'accept_reject_follow_request', 'uses' => 'UsersController@accept_reject_follow_request'));
	 
	 Route::post('/report_user', array('as' => 'report_user', 'uses' => 'UsersController@report_user'));
	 Route::post('/report_post', array('as' => 'report_post', 'uses' => 'UsersController@report_post'));
 
     Route::post('/logout', array('as' => 'logout', 'uses' => 'UsersController@logout'));
	 Route::post('/like_dislike', array('as' => 'like_dislike', 'uses' => 'UsersController@like_dislike'));
	 Route::post('/undo_like_dislike', array('as' => 'undo_like_dislike', 'uses' => 'UsersController@undo_like_dislike'));
	 Route::post('/rate_user', array('as' => 'rate_user', 'uses' => 'UsersController@rate_user'));
	 Route::post('/search', array('as' => 'search', 'uses' => 'UsersController@search'));
 
 
 
 
	
	//App Data
	Route::post('/get_app_data', array('as' => 'get_app_data', 'uses' => 'AppDataController@get_app_data'));

  //terms_conditions
    Route::post('/cron_check_auction_expiry', array('as' => 'cron_check_auction_expiry', 'uses' => 'AuctionController@cron_check_auction_expiry'));


//facebook login
Route::post('/facebook_login', array('as' => 'facebook_login', 'uses' => 'UsersController@facebook_login'));

  //routes regarding scheduled jobs
    Route::post('/add_job', array('as' => 'add_job', 'uses' => 'ScheduledJobsController@add_job'));
    Route::post('/delete_job', array('as' => 'delete_job', 'uses' => 'ScheduledJobsController@delete_job'));
    Route::post('/edit_job', array('as' => 'edit_job', 'uses' => 'ScheduledJobsController@edit_job'));

Route::post('/check_auction_expiry', array('as' => 'check_auction_expiry', 'uses' => 'ScheduledJobsController@check_auction_expiry'));
  

// new apis on 26 april 2018




// new apis on 26 april 2018





Route::post('update_private_status', array('as' => 'update_private_status', 'uses' => 'UsersController@update_private_status'));
Route::post('get_private_status', array('as' => 'get_private_status', 'uses' => 'UsersController@get_private_status'));
Route::post('get_2fa_status', array('as' => 'get_2fa_status', 'uses' => 'UsersController@get_2fa_status'));
Route::post('get_pgp_key', array('as' => 'get_pgp_key', 'uses' => 'UsersController@get_pgp_key'));


Route::post('update_preferred_currency', array('as' => 'update_preferred_currency', 'uses' => 'UsersController@update_preferred_currency'));
Route::post('get_preferred_currency', array('as' => 'get_preferred_currency', 'uses' => 'UsersController@get_preferred_currency'));

Route::post('send_pgp_message', array('as' => 'send_pgp_message', 'uses' => 'UsersController@send_pgp_message'));
Route::post('get_pgp_message', array('as' => 'get_pgp_message', 'uses' => 'UsersController@get_pgp_message'));
Route::post('delete_pgp_message', array('as' => 'delete_pgp_message', 'uses' => 'UsersController@delete_pgp_message'));
 
 
 
 
});