@extends('layout.admin')
@section('content')
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-app="myApp">
   <!-- Content Header (Page header) -->
   <section class="content-header">
 
      <ol class="breadcrumb">
         <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
         <li class="active">Orders</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box  " style="display:none">
        
            <nav class="navbar filters-nav">
               <!--- 
                  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1])->get()->count() }})</a></li>
                    <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0])->get()->count() }})</a></li>
                  </ul>
                  
                   -->
            </nav> 
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
             
			 
			 
			 
			 <!------ actual content starts ------>
			 
			 
		 
 <div class="row">
    <div class="col-sm-12">
	    <h2>Order Detail #{{ @$order_id}}</h2>
	</div>
	</div>
  <div class="row">
    <div class="col-sm-12">
	<div class="panel">
	   <h3>Post Details</h3>
	   
	   
 
       <p><b>Title :</b> {{ @$post_details[0]['title'] }}</p>
	   <p><b>Description :</b> {{ @$post_details[0]['description'] }}</p>
	   
	   <?php if ($post_details[0]['price'] != null && @$post_details[0]['price'] != '' )
	   {?>
	    <p><b>Price :</b> {{ @$post_details[0]['currency'] }}{{ @$post_details[0]['price'] }}</p>
	   <?php } ?>
	   
	   
	   
	      <?php if ($post_details[0]['paypal_id'] != null && @$post_details[0]['paypal_id'] != '' )
	   {?>
	    <p><b>Paypal Id :</b> {{ @$post_details[0]['paypal_id'] }}</p>
	   <?php } ?>
	   
	   
	      <?php if ($post_details[0]['terms_and_conditions'] != null && @$post_details[0]['terms_and_conditions'] != '' )
	   {?>
	    <p><b>Terms & Conditions :</b> {{ @$post_details[0]['terms_and_conditions'] }}</p>
	   <?php } ?>
	   
	   
	   
	   
	   <?php if ($post_details[0]['terms_and_conditions'] != null && @$post_details[0]['terms_and_conditions'] != '' )
	   {?>
	    <p><b>Terms & Conditions :</b> {{ @$post_details[0]['terms_and_conditions'] }}</p>
	   <?php } ?>
	   
	      <?php if ($post_details[0]['country'] != null && @$post_details[0]['country'] != '' )
	   {?>
	    <p><b>Country:</b> {{ @$post_details[0]['country'] }}</p>
	   <?php } ?>
	   
	   
	</div> 
    </div> 
	
	
 
 
	
	 <div class="col-sm-4">
     <div class="panel delivery-detail">
	   <h3>Order Details</h3>
      <table class="table">
	    <tr> 
		  <td>Order By</td>
		  <td>{{@$order_by_user_details[0]['first_name']}} {{@$order_by_user_details[0]['last_name']}}</td>
		</tr>
		
		  	     <?php if ($order_details[0]['payment_method'] != null && @$order_details[0]['payment_method'] != '' ) {?>
	     <tr> 
		   <td style="border-top:0px">Payment Method</td>
		   <td style="border-top:0px"  >{{ @$order_details[0]['payment_method'] }}</td>
		 </tr>
		 <?php } ?>
		 
		 
		 
		 		  	     <?php if ($order_details[0]['status'] != null && @$order_details[0]['status'] != '' ) {?>
	     <tr> 
		   <td style="border-top:0px">Status</td>
		   <td style="border-top:0px"  >{{ @$order_details[0]['status'] }}</td>
		 </tr>
		 <?php } ?>
		 
		 
		 
 
	  </table>
	</div> 
    </div> 
	 <div class="col-sm-4">
      <div class="panel">
	   <h3>Seller Details</h3>
      <p class="title">Sold By</p>
      <p> {{ @$order_details[0]['seller_name']}} , {{ @$user_details[0]['country']}}</p>
	  <p>{{ @$order_details[0]['seller_email']}}</p>
	  
	
		 
		 
		 
	</div> 
    </div> 
	
	     <?php if ($post_details[0]['country'] != null && @$post_details[0]['country'] != '' )
	   {?>
	    <p><b>Country:</b> {{ @$post_details[0]['country'] }}</p>
	   <?php } ?>
	 <div class="col-sm-4">
      <div class="panel">
	   <h3>Payment Details</h3>
	    <table class="table">
	   <tbody>
	   
	   
	   
	     <?php if ($post_details[0]['payment_method'] != null && @$post_details[0]['payment_method'] != '' ) {?>
	     <tr> 
		   <td style="border-top:0px">Payment Method</td>
		   <td style="border-top:0px" align="right">{{ @$post_details[0]['payment_method'] }}</td>
		 </tr>
		 <?php } ?>
		 
		 
		 
		      <?php if ($post_details[0]['currency_code'] != null && @$post_details[0]['currency_code'] != '' ) {?>
	     <tr> 
		   <td style="border-top:0px">Currency Code</td>
		   <td style="border-top:0px" align="right">{{ @$post_details[0]['currency_code'] }}</td>
		 </tr>
		 <?php } ?>
		 
		 
		 
		 		      <?php if ($post_details[0]['paypal_transaction_id'] != null && @$post_details[0]['paypal_transaction_id'] != '' ) {?>
	     <tr> 
		   <td style="border-top:0px">Paypal Transaction Id</td>
		   <td style="border-top:0px" align="right">{{ @$post_details[0]['paypal_transaction_id'] }}</td>
		 </tr>
		 <?php } ?>
		 
		 
		 
	 
		 </tbody>
	   </table>
	   
	   

	

	</div> 
    </div> 
	
	
	
		       <div class="col-sm-12">
	<div class="panel">
	   <h3>Feedbacks</h3>
	   
	   
 
  
	   
	   <?php if ($order_details[0]['buyer_ratings'] != null && @$order_details[0]['buyer_ratings'] != '' )
	   {?>
	    <p><b>Buyer Ratings :</b> {{ @$order_details[0]['buyer_ratings'] }} </p>
	   <?php } ?>
	   
	   
	   
	      <?php if ($order_details[0]['buyer_feedback'] != null && @$order_details[0]['buyer_feedback'] != '' )
	   {?>
	    <p><b>Buyer Feedback :</b> {{ @$order_details[0]['buyer_feedback'] }}</p>
	   <?php } ?>
	   
	   
	   <?php if ($order_details[0]['seller_ratings'] != null && @$order_details[0]['seller_ratings'] != '' )
	   {?>
	    <p><b>Seller Ratings :</b> {{ @$order_details[0]['seller_ratings'] }} </p>
	   <?php } ?>
	   
	   
	   
	      <?php if ($order_details[0]['seller_feedback'] != null && @$order_details[0]['seller_feedback'] != '' )
	   {?>
	    <p><b>Seller Feedback :</b> {{ @$order_details[0]['seller_feedback'] }}</p>
	   <?php } ?>
	   
	   
	   
	   <?php
	   if(@$order_details[0]['seller_ratings'] == null && @$order_details[0]['seller_feedback'] == null && @$order_details[0]['buyer_ratings'] == null && @$order_details[0]['buyer_feedback'] == null)
	   { ?>
		   <h4>No Feedbacks are given to this order</h4>
	   <?php } ?>
	   
	   
	</div> 
    </div> 
	
	
  </div>
 	   

			 
			 
			 <!------ actual content ends ------>
			 
			 
			 
			 
			 
			 
			 
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop













 
 

 