@extends('layout.admin')
@section('content')
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <strong>Used Points History</strong>
      </h1>
 
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-info">
         <div class="box-header"  >
            <nav class="navbar navbar-default"  >
               <!---<div class="container-fluid">
                  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1])->get()->count() }})</a></li>
                    <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0])->get()->count() }})</a></li>
                  </ul>
                  
                  </div>-->
            </nav>
		 
         </div>
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Used Points History</h3>
               </div>
               <!-- /.box-header -->
               @if(Session::has('message'))
               <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
               <div id="loading"></div>
               <style>
                  #loading {
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  -webkit-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
                  }
               </style>
               <div id="item-lists">
                  <div class="box-body">
                     <table id="example1" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Sr. No.</th>
                              <th>User</th>
                              <th>Description</th>
                              <th>Points</th>
							  <?php if( auth()->user()->user_type == 'store' or auth()->user()->user_type == 'brand' ) { ?> <th>Approveds</th> <?php } ?>
                              <th>Created</th>
					 
                    
                           </tr>
                        </thead>
                        <tbody>
                           <?php $sr = 0; foreach ($posts as $key => $value) { ?>
                           <tr>
                              <td><?php echo ++$sr; ?></td>
                              <td><?php echo @\App\User::where('id',$value['user_id'])->first(['first_name'])->first_name.' '.@\App\User::where('id',$value['user_id'])->first(['last_name'])->last_name; ?></td>

							 <td><?php 
						 
								   echo  $value['description'];
							  
							  ?></td>
							  
							     <td><?php 
						 
								   echo  $value['points'];
							  
							  ?></td>
							  
							  
							           <td><?php
                                 if($value['approved_status'] == 0 )
                                 	{
                                 	echo "Not Approved";
									?></br>
									<?php if(auth()->user()->user_type == 'store' or auth()->user()->user_type == 'brand'  ) { ?> <a class="btn btn-small btn-success" title="Delete Treasure Box" href="{{ URL::to('/treasure_box_approve_activity/'.$value['id'].'/treasure_box_approve_activity') }}" onclick="return confirm('Are you sure?')">Approve</a> <?php } ?>
                                  <?php }
                                   else
                                   {
                                    echo "Approved";
                                 } ?>
                              </td>
                     
                  
						 
							     <td><?php echo $value['created_at']; ?></td>
						 
							  
							  
                 
                           
                     
                           </tr>
                           <?php }?>
                        </tbody>
                     </table>
                  </div>
                  <!-- /.box-body -->
               </div>
               {!! $posts->render() !!}
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop