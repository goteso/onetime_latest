@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Auction Details</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/auction') }}">Auction</a></li>
            <li class="active">View</li>
          </ol>
        </section
        
        <section class="content">
           <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/auction') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i>View All</a>
                </div><!-- /.box-header -->
            </div>

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
			<h4><b>Auction Images</b> </h4>
							         <!-- Carousel Slideshow -->
                    <div id="carousel-example" class="carousel slide" data-ride="carousel"  >
                        <!-- Carousel Indicators -->
                        <ol class="carousel-indicators">
						
						<?php $y=0;?>
						 @foreach($auction_images as $slide)
                         
							<li data-target="#carousel-example" data-slide-to="0" class="{{ $y == 0 ? ' active' : '' }}"></li>
                            <?php $y++;?>
                            @endforeach
							 
                        </ol>
                        <div class="clearfix"></div>
                        <!-- End Carousel Indicators -->
                        <!-- Carousel Images -->
                        <div class="carousel-inner">
						<?php $x=0;?>
                        @foreach($auction_images as $slide)
                            <div  class="item{{ $x == 0 ? ' active' : '' }}">
                                <img src="{{url('/')}}/users/{{$slide->image}}" class="center-block" style="max-height:500px">
                            </div>
							<?php $x++;?>
                            @endforeach
                             <!-- Carouse Images -->
                        <!-- Carousel Controls -->
                        <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                        <!-- End Carousel Controls -->
                    </div>
					</div>
				
                    <!-- End Carousel Slideshow -->
					
                 <table  class="table table-bordered table-striped">
                    <thead>
					
					<?php
				 
					foreach ($auctions->getAttributes() as $k => $v) 
					{ 
						
						  if($k =='user_id')
						  {
						    echo '<tr><th width="15%">Auctioner Details</th>';
						  }
						  else
						  {
							 echo '<tr><th width="15%">'.camel_case($k).'</th>';  
						  }
						  
						  if($k == 'user_id')
						  {?>
						     <td>Click <a   title="View User" href="{{ URL::to('admin/user/'. $v ) }}">here</a> to view user Details</td>
						  <?php }
						  
						  else if($k == 'profile_image' || $k == 'verify_photo' )
						  {   echo '<td><img src="'.url('/').'/users/'.$v.'" height="200px"></td>';
					   
						  }
						  else  if($k == 'auction_duration' )
						  {   echo '<td>'.$v.' days</td>';
					   
						  }
						  else  if($k == 'expiry_date' )
						  {
						    $seconds = strtotime($v) - time();
                            $days = floor($seconds / 86400);
                            $seconds %= 86400;
                            $hours = floor($seconds / 3600);
                            $seconds %= 3600;
                            $minutes = floor($seconds / 60);
                            $seconds %= 60;
							    
							    echo '<td>'.$v.' <span style="color:#129d12">('.$days.' days , '.$hours.' hrs , '.$minutes.' mins remaining )</span></td>';
						  }
							
                      
							
							
						  else if( $k == 'bid_increment_status' || $k == 'buy_now_option_status' || $k == 'shipping_available_status' || $k == 'international_shipping_status' )
						  {
							   if($v == '1') { $v = 'ON';} if($v == '0') { $v = 'OFF';} 
							   echo '<td>'.$v.'</td>';
						  }
						  else
						  {
						     echo '<td>'.$v.'</td>';
						  }
			
                          echo '</tr>';
						  ?>
						  
						  
						  
						  
						  




<?php

                   }
					?>
                   
                           
                       
                      </tr>
                    </thead>
                    <tbody>
                                                      
                    </tbody>
                    
                  </table>
				 
 			
					
             
            </div><!-- /.box-body -->
           
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop