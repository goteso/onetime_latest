@extends('layout.admin')
@section('content')
<?php use App\Auction;?>

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Auctions</strong>
          </h1>
          <ol class="breadcrumb" style="display:none">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/user') }}">Auctions</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info">
                <div class="box-header">
                  		<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <b class="navbar-brand" >Filter By Status :</b>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="{{ URL::to('admin/auction/1') }}">Open({{ \App\Auction::where(['status' => 1])->get()->count() }})</a></li>
      <li><a href="{{ URL::to('admin/auction/0') }}">Draft({{ \App\Auction::where(['status' => 0])->get()->count() }})</a></li>
	  <li><a href="{{ URL::to('admin/auction/2') }}">Closed({{ \App\Auction::where(['status' => 2])->get()->count() }})</a></li>
	  <li><a href="{{ URL::to('admin/auction/3') }}">Cancelled({{ \App\Auction::where(['status' => 3])->get()->count() }})</a></li>
    </ul>
   
  </div>
</nav>
                </div><!-- /.box-header -->
            </div>
			
			
			
			
			
			
			


	
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Auction Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

			   
			   			<div id="item-lists">
		@include('admin.auctions.data-ajax')
	</div>
			   
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

			   
	  
 





    

     
@stop