                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Title</th>
                        <th>Country</th>
                        <th>Opening Bid</th>
                        <th>Closing On</th>
						<th>Status</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>
	

                   	 <?php $sr = 0; foreach ($auctions as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['title']; ?></td>
                    <td><?php echo $value['country']; ?></td>
                    <td><?php echo $value['currency'].$value['opening_bid']; ?></td>
					<td><?php echo $value['expiry_date']; ?></td>
                   	<td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Draft";
                   	   }
                   	   else if($value->status == 1)
                   	 	{
                   	    echo "Open";
                   		}
                     else if($value->status == 2)
                   	 	{
                   	    echo "Closed";
                   		}
                     else if($value->status == 3)
                   	 	{
                   	    echo "Cancelled";
                   		}						?>
                   	</td>
                    <td><?php echo $value['created_at']; ?></td>
                    <td>
                   <a class="btn btn-small btn-success" title="View Auction" href="{{ URL::to('admin/auction/show/'.$value['id']) }}"><i class="fa fa-info"></i></a>                
                
            	   </td>
				     <td>
				   			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $value['id'];?>">Bids  <span  class="badge"><?php echo $value["bids"]->count();?></span></button>
						</td>
                   	 </tr>
					 
					 <!-- Modal -->
<div id="myModal<?php echo $value['id'];?>" class="modal fade" role="dialog"  >
  <div class="modal-dialog" style="width:80%;margin-left:10%;margin-top:10%" >

    <!-- Modal content-->
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center " style="font-size:26px;font-weight:bold">Total Bids <span style="color:#2baac8">(<?php echo $value["bids"]->count();?>)</span></h4>
      </div>
      <div class="modal-body"  >
 
	  
	  
	                  
                   
                      <div class="row">
                        <div class="col-md-1 col-lg-1 col-sm-1" style="color:#2baac8;font-size:22px;font-weight:bold">Sr.</div>
                        <div class="col-md-4 col-lg-4 col-sm-4" style="color:#2baac8;font-size:22px;font-weight:bold">Bidder</div>
                        <div class="col-md-2 col-lg-2 col-sm-2" style="color:#2baac8;font-size:22px;font-weight:bold">Country</div>
                        <div class="col-md-2 col-lg-2 col-sm-2" style="color:#2baac8;font-size:22px;font-weight:bold">Bid Amount</div>
                        <div class="col-md-3 col-lg-3 col-sm-3" style="color:#2baac8;font-size:22px;font-weight:bold">Bid Time</div>
                      </div>
                  
                   
	
	
 

                   	 <?php $sr2 = 0; foreach ($value["bids"] as $bids => $bid) { $sr2++; ?>
       
					    <div class="row" style="margin-top:1%">
                        <div class="col-md-1 col-lg-1 col-sm-1"><?php echo $sr2;?></div>
                        <div class="col-md-4 col-lg-4 col-sm-4">{{ \App\User::where('id',$bid["user_id"])->first(["name"])->name }}</div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><?php echo $bid["country"];?></div>
                        <div class="col-md-2 col-lg-2 col-sm-2"><?php echo $bid["bid_amount"];;?></div>
                        <div class="col-md-3 col-lg-3 col-sm-3"><?php echo $bid["created_at"];;?></div>
                      </div>
 


                   <?php }?>
                   
               
                    
               
	  
	 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                {!! $auctions->render() !!}