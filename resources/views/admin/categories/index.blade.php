@extends('layout.admin')
@section('content')

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Categories</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/categories') }}">Categories</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/categories/create') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Categories Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Category Name</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($categories as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	   <td><?php echo $value['category_name']; ?></td>
                       <td><?php echo $value['created_at']; ?></td>
                    <td>
              
                   <!--<a class="btn btn-small btn-info" title="Edit Category" href="{{ URL::to('admin/categories/' .  $value['id'] . '/edit') }}"><i class="fa fa-pencil"></i></a>  -->                
                  <a class="btn btn-small btn-danger" title="Delete Category" href="{{ URL::to('admin/categories/' .  $value['id'] . '/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>                 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                {!! $categories->render() !!}
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

    
    

     
@stop