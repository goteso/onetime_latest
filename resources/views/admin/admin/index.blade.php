@extends('layout.admin')
@section('content')

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Admins</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class=" row">
              <div class="col-sm-10">
				  <nav class="navbar filters-nav">
  
    <div class="navbar-header">
      <b class="navbar-brand" >Filter By Status :</b>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1 , 'user_type'=>'admin'])->get()->count() }})</a></li>
      <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0, 'user_type'=>'admin'])->get()->count() }})</a></li>
    </ul>
   
 
</nav>

</div>

<div class="col-sm-2 text-right" style="padding:10px 20px;">
                  <a href="{{ URL::to('admins/admin/create') }}" class="pull-right btn btn-info  "   ><i class="fa fa-plus"></i> Add New</a>
				  </div>
                </div><!-- /.box-header -->
            
			<br class="visible-xs">
			
			
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 


 
			  
                <!--<div class="box-header">
                  <h3 class="box-title">Admin Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
						
					
		 @include('admin.admin.data-ajax')
	</div>
			   {!! $users->render() !!}
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
@stop