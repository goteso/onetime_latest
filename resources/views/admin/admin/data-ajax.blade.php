                <div class="box-body">
                  <table id="example1" class="table  table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['first_name']." ".$value['last_name']; ?></td>
                    <td><?php echo $value['user_type']; ?></td>
                    <td><?php echo $value['email']; ?></td>
                    
                   	<td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Disabled";
                   	   }
                   	   else
                   	   {
                   	    echo "Active";
                   		} ?>
                   	</td>
                    <td><?php echo $value['created_at']; ?></td>
                    <td class="action-btn">
                   <a class="btn btn-small " title="View User" href="{{ URL::to('admins/admin/'.$value['id']) }}"><i class="fa fa-eye"></i></a>                
                   <a class="btn btn-small  " title="Edit User" href="{{ URL::to('admins/admin/'.$value['id'].'/edit') }}"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small " title="Delete User" href="{{ URL::to('admins/admin/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small  " title="Block User" href="{{ URL::to('admins/admin/'.$value['id'].'/block') }}" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small " title="Unblock User" href="{{ URL::to('admins/admin/'.$value['id'].'/unblock') }}" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                