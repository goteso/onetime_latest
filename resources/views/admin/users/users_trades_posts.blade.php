@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>User Bitcoin Adverts</strong>
          </h1>
       
        </section>
        
<div class="container-fluid order-detail">
 
 		 <div class="row  user-profile-data"  > 
	      <div class="col-sm-12 col-md-6 col-lg-4"   >
		    <div class="card info"   >
		     <img src="<?php echo url('/')."/";?>users/<?php echo $user[0]->profile_image;?>" style="display:block;margin-right:0;max-height:200px">
			 <div>
			       <h4> {{ @$user[0]->first_name }} {{ @$user[0]->last_name }} <a href="{{ URL::to('admin/user/'.$user[0]->id.'/edit') }}"><i class="fa fa-edit"></i></a></h4>
			   <h5>{{ @$user[0]->username }}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		   <div class="col-sm-6 col-md-3"   >
		    <div class="card"   >
			<div class="card-data">
		       <h4>{{ @$user[0]["total_orders"]}} Orders</h4>
			   <h5>In Progress</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"    >
		   <div class="card-data">
		        <h4> {{ @$user[0]['total_trades'] }} Trades</h4>
			 <h5>In Progress </h5>
			 </div>
		   </div>
		   </div> 
		 
		 
		 </div>
		 <br><br>
		 
  <div class="row">
    <div class="col-sm-12">

	
	@include('admin.users.users_tabs_bar')    
	
	 
 <!-------------------orders table starts here----->
  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
						<th>Id</th>
                        <th>Type</th>
						<th>Currency</th>
                        <th>Trade Rate</th>
                        <th>Range</th>
						 <th>country</th>
					    <th>Created</th>
                        <th id="action" style="display:none ">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($data as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
			        <td><a href="{{ URL::to('admin/trades_posts_details/'.$value['id']) }}/{{@$user_id}}"  target="_blank"  >#{{ @$value['id'] }}</a></td>
                    <td>{{ @$value['type'] }}</td>
                  	<td> {{ @$value['currency'] }}  	</td>
					<td> <b>Trade Rate :</b>{{ @$value['trade_rate'] }} </br>
                         <b>Trade Rate(%) :</b>{{ @$value['trade_rate_percentage'] }} </td>
					<td><b>Min : </b> {{ @$value['range_min'] }} </br><b>Max : </b> {{ @$value['range_max'] }} 	</td>
					<td>{{ $value['country'] }}</td>
			        <td><?php echo $value['created_at']; ?></td>
					
					
					
					
					
 
                    <td class="action-btn" style="display:none">
                     <a class="btn btn-small  " title="View User" href="{{ URL::to('admin/user/show/'.$value['id']) }}"  target="_blank" ><i class="fa fa-eye"></i></a>           
                   <a class="btn btn-small  " title="Edit User" href="{{ URL::to('admin/user/'.$value['id'].'/edit') }}" target="_blank"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="{{ URL::to('admin/user/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small  " title="Block User" href="{{ URL::to('admin/user/'.$value['id'].'/block') }}" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small  " title="Unblock User" href="{{ URL::to('admin/user/'.$value['id'].'/unblock') }}" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
				  
				  
				 
 
            	   </td>
                   	 </tr>
					 
					 
					 
					 
 



                   <?php }?>
                   
                   </tbody>
 
 
  <!-------------------orders table ends here----->
    </div>  
  </div>
  
  
</div>

      </div><!-- /.content-wrapper -->
@stop