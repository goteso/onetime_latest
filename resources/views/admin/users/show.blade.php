@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>User Details</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/student') }}">Users</a></li>
            <li class="active">View</li>
          </ol>
        </section>
        
<div class="container-fluid order-detail">
 
 		 <div class="row  user-profile-data"  > 
	      <div class="col-sm-12 col-md-6 col-lg-4"   >
		    <div class="card info"   >
		     <img src="<?php echo url('/')."/";?>users/<?php echo $user[0]->profile_image;?>" style="display:block;margin-right:0;max-height:200px">
			 <div>
		       <h4> {{ @$user[0]->first_name }} {{ @$user[0]->last_name }} <a href="{{ URL::to('admin/user/'.$user[0]->id.'/edit') }}"><i class="fa fa-edit"></i></a></h4>
			   <h5>{{ @$user[0]->username }}</h5>
			 </div>
		   </div>
		 </div> 
 	 
		   <div class="col-sm-6 col-md-3"   >
		    <div class="card"   >
			<div class="card-data">
		       <h4>{{ @$user[0]["total_orders"]}} Orders</h4>
			   <h5>In Progress</h5>
			 </div>
		   </div>
		   </div>
		  <div class="col-sm-6 col-md-3"   >
		   <div class="card"    >
		   <div class="card-data">
		            <h4> {{ @$user[0]['total_trades'] }} Trades</h4>
			 <h5>In Progress </h5>
			 </div>
		   </div>
		   </div> 
		 
		 
		 </div>
		 <br><br>
		 
  <div class="row">
    <div class="col-sm-12">

	
	@include('admin.users.users_tabs_bar')    
	
	

<div class="tab-content">
  <div id="basic-details" class="tab-pane fade in active">
  <div class="panel"> 
   <div class="panel-body">
    <p> Email :  {{ @$user[0]->email }}</p>
	<p>Phone : {{ @$user[0]->phone }} </p>
	<p>DOB : {{ @$user[0]->dob }}</p>
	 <p> Country : {{ @$user[0]->country }}</p>
	<p>Gender : {{ @$user[0]->gender }} </p>
	<p>Bio : {{ @$user[0]->email }} </p>
	 <p> Wallet Address : {{ @$user[0]->wallet_address }}</p>
	<p>Verified : {{ @$user[0]->verified }} </p>
	<p>Rating : 3.4/5 </p>
	 <p> Bitcoin Blance :{{ @$user[0]->bitcoin_balance }} BTC</p>
	<p>Followers : {{ @$user[0]->followers_count }} </p>
	<p>Following : {{ @$user[0]->followings_count }} </p>
	</div>
	</div>
  </div>
  <div id="orders" class="tab-pane fade">
  <div class="panel">
   <div class="panel-body">
    <h3>Orders</h3>
    <p>Some content in menu 1.</p>
	</div>
  </div>
  </div>
  <div id="trades" class="tab-pane fade">
  <div class="panel">
   <div class="panel-body">
    <h3>Trades</h3>
    <p>Some content in menu 2.</p>
	</div>
	</div>
  </div>
  <div id="transactions" class="tab-pane fade">
  <div class="panel">
   <div class="panel-body">
    <h3>Transactions</h3>
    <p>Some content in menu 2.</p>
	</div>
	</div>
  </div>
  <div id="posts" class="tab-pane fade">
  <div class="panel">
    <h3>Posts</h3>
    <p>Some content in menu 2.</p>
	</div>
	</div>
 
  <div id="bicoin-adverts" class="tab-pane fade">
  <div class="panel">
   <div class="panel-body">
    <h3>Bitcoin Adverts</h3>
    <p>Some content in menu 2.</p>
	</div>
	</div>
  </div>
</div>
    </div>  
  </div>
  
  
</div>

      </div><!-- /.content-wrapper -->
@stop