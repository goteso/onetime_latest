@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Edit Country</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/country') }}">Users</a></li>
            <li class="active">Edit</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/country') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit Country</h3>
                </div><!-- /.box-header -->
                <!-- form start -->                
                <?php echo Form::open(array('url' => 'admin/country/'.$country->id,'method'=>'put','class'=>'form-horizontal')) ?>

                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="country_name" class="col-md-3 control-label">Country Name(Google Map)</label>
                     <div class="col-md-6">  
                      <input type="text" name="country_name" class="form-control" id="country_name" placeholder="Enter Country Name" value="<?php echo $country->country_name;?>">
                      <div class="error-message">{{ $errors->first('country_name') }}</div>
                    </div>
                    </div>

                    <div class="form-group">
                      <label for="nicename" class="col-md-3 control-label">Display Name</label>
                      <div class="col-md-6"> 
                      <input type="text" name="nicename" class="form-control" id="nicename" placeholder="Enter Last Name" value="<?php echo $country->nicename;?>">
                      <div class="error-message">{{ $errors->first('nicename') }}</div>
                    </div>
                    </div>

					
					
					
					
					     <div class="form-group" style="display:none">
                      <label for="currency" class="col-md-3 control-label">Currency(Symbol)</label>
                      <div class="col-md-6"> 
                      <input type="text" name="currency" class="form-control" id="currency" placeholder="Enter Last Name" value="<?php echo $country->currency;?>">
                      <div class="error-message">{{ $errors->first('currency') }}</div>
                    </div>
                    </div>

              
  

                    <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop