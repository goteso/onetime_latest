                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Country Name(Google Map)</th>
                        <th>Display Name</th>
                        <th>Country</th>
                 
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($countries as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['country_name']; ?></td>
         
                    <td><?php echo $value['nicename']; ?></td>
                    <td><?php echo $value['currency']; ?></td>
            
                    <td><?php echo $value['created_at']; ?></td>
                    <td>
                   <a class="btn btn-small btn-success" title="View Country" href="{{ URL::to('admin/country/'.$value['id']) }}"><i class="fa fa-info"></i></a>                
                   <a class="btn btn-small btn-info" title="Edit Country" href="{{ URL::to('admin/country/'.$value['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete Country" href="{{ URL::to('admin/country/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>                 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                {!! $countries->render() !!}