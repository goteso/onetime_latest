@extends('layout.admin')
@section('content')

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Countries</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/country') }}">Countries</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/country/create') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
            </div>
			
			
			
			
			
			
			
			

	
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Country Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

			   
			   			<div id="item-lists">
		@include('admin.countries.data-ajax')
	</div>
			   
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
@stop