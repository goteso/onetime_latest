@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Brands</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
            <li class="active">Add</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admins/admin') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Add Membership Card Type</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                    {!! Form::open(['route' => ['membership_card_types_store'] ,'files'=>'true' , 'method'=>'POST']) !!}
                
                  <div class="box-body">
                    
					
					<div class="row form-group"  ">
                      <label for="title" class="col-md-3 control-label">Title</label>
                       <div class="col-md-6">  
                      <input type="text" name="title" class="form-control" id="title" placeholder="Card Title Here" value="{{old('title')}}">
                      <div class="error-message">{{ $errors->first('title') }}</div>
                    </div></div>

					
					
					      
       
                    <div class="row form-group">
                      <label for="first_name" class="col-md-3 control-label">Image</label>
                       <div class="col-md-6">  
                           	       <?php  
                                           echo Form::file('image');
		                           ?>
                
                    </div></div>
					
					
					
					   <div class="row form-group">
                      <label for="first_name" class="col-md-3 control-label">Linked Brand</label>
                       <div class="col-md-6">  
                              <select   class="form-control" id="example-gettindg-started" name="linked_brand_id" style="width:100%">
      @foreach($brands as $ct)
 
         <option value="{{ $ct->id }}" >{{ $ct->first_name }}</option>
     @endforeach
</select>
                
                    </div></div>
					 
					
 
				  
				  
				  
		 
				  
		 
                  </div><!-- /.box-body -->

                   <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop