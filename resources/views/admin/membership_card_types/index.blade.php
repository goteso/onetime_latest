@extends('layout.admin')
@section('content')

</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Membership Card Types</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('brand/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('brands/brand') }}">brands</a></li>
            <li class="active">Listing</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-info">
                <div class="box-header">
				                  		<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <b class="navbar-brand" >Filter By Status :</b>
    </div>
    <ul class="nav navbar-nav">
	 <li><a href="{{ URL::to('brands/brand/status/1') }}">Active({{ \App\Admin::where(['status' => 1 , 'user_type' => 'brand'])->get()->count() }})</a></li>
      <li><a href="{{ URL::to('brands/brand/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0 , 'user_type' => 'brand'])->get()->count() }})</a></li>
    </ul>
   
  </div>
</nav>


                  <a href="{{ URL::to('admin/membership_card_types/create') }}" class="pull-right btn btn-info btn-sm"   ><i class="fa fa-plus"></i> Add New</a>
                </div><!-- /.box-header -->
            </div>
			
			
			
	
	
	
	
          <div class="row">
            <div class="col-xs-12">
		 
 

              <div class="box">
 


 
			  
                <div class="box-header">
                  <h3 class="box-title">Card Types Listing</h3>
                </div><!-- /.box-header -->
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

			   <div id="loading"></div>

<style>
#loading {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>
			   			<div id="item-lists">
						
						
					
		 @include('admin.membership_card_types.data-ajax')
	</div>
			   {!! $users->render() !!}
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
@stop