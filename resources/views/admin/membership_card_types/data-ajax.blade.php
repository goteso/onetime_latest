                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Linked_Brand</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
                   	<td><?php echo $value['title']; ?></td>
					<td><img src="<?php echo url('/').'/'.$value->image?>" height="100px" ></td>
      
         
					      <td><?php 
					$linked_brand_name = @\App\Admin::where("id",$value['linked_brand_id'])->first(["first_name"])->first_name;
					echo @$linked_brand_name;
					?> 
			 
					 
					 
				 
					</td>
					<td><?php echo $value['created_at']; ?></td>
              
                    <td>
                              
                   <a class="btn btn-small btn-info" title="Edit Card" href="{{ URL::to('admin/membership_card_types/'.$value['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete Card" href="{{ URL::to('admin/membership_card_types/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   

			 
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                