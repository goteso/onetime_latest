@extends('layout.admin')
@section('content')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
 
<script>tinymce.init({
  selector: "textarea",  // change this value according to your HTML
  
 
});</script>

 


</style>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Settings</strong>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
           
 <div class="row">
            <div class="col-xs-12">
              <div class="box">
             
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

	   
	<ul class="nav nav-tabs nav-tabs1">
 <?php if(auth()->user()->user_type == 'admin')
			{?> <li><a  href="{{URL::asset('admin/terms_conditions')}}" >Terms & Conditions</a></li>	<?php } ?>
  <?php if(auth()->user()->user_type == 'admin')
			{?><li><a  href="{{URL::asset('admin/app_data')}}">Application Data</a></li>	<?php } ?>
 <?php if(auth()->user()->user_type == 'admin')
			{?> <li class="active"><a data-toggle="tab" href="#app-notice">App HomePage Notice</a></li>	<?php } ?>
</ul>

<div class="tab-content">
  <div id="app-notice" class="tab-pane fade in active">
            <div class="panel ">
<br>
                  <!--<div class="panel-heading">Admin Notice</div>-->
                  
                  <div class="panel-body">
                  {{ Form::open(array('url' => 'admin/app_pop_up_update','class'=>'form-horizontal')) }}
                  
				  <textarea class="description" rows="10" name="pop_up_content">{{$content}}</textarea>
                  
		          <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
				  
                  {{ Form::close() }}
                  </div>

            </div>
 
 

			   </div>
			   </div>
			   
			   
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	  
	  
    
    

     
@stop