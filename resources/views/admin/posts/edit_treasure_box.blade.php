@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Treasure Box List</strong>
          </h1>
 
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                      <a href="{{ URL::to('/treasure_box_list/'.Auth::id().'/treasure_box_list') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit Treasure List</h3>
                </div><!-- /.box-header -->
 
                
  
				  
<div class="container"> 


 
			<?php	echo Form::open(array('url' => '/treasure_box/update_treasure_box/','files'=>'true','method'=>'POST')); ?>
 
   <div class="form-group">
     <label for="description">Description</label>
     <textarea class="form-control" rows="5" name="description" id="description" placeholder="Enter Description Here"><?php echo $user["description"];?></textarea>
	   <div class="error-message">{{ $errors->first('description') }}</div>
  </div>
  
  
  
     <div class="form-group">
     <label for="description">Points</label>
     <input type="TEXT" class="form-control"   name="points" id="points" placeholder="Enter Points Here" value="<?php echo $user["points"];?>"> 
	   <div class="error-message">{{ $errors->first('points') }}</div>
  </div>
  
  

  
  <input type="hidden" name="brand_id" id="brand_id" value="<?php echo Auth::id()?>">
   <input type="hidden" name="treasure_box_id" id="treasure_box_id" value="<?php echo $user["id"];?>">
  
 
	
 
	
    <button type="submit" class="btn btn-info">Submit</button>
  </form>
</div>






 
	<?php if($user->image_type =='image') {  echo '<script>checkImage()</script>'; } ?>
	<?php if($user->image_type =='image_text') {  echo '<script>checkText()</script>'; } ?>
            
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop