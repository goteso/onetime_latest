                <div class="box-body">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					    <th>Title</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($posts as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td><?php echo $value['title']; ?></td> 
					<td style="max-width: 100px;overflow: hidden; text-overflow: ellipsis;   white-space: nowrap;"><?php echo $value['description']; ?></td>
                    <td><?php
                   	   if($value->type == 0)
                   	 	{
                   	 	  echo "Normal";
                   	    }
                   	    else
                   	    {
                   	      echo "Multiple Free";
                   		} 
						?>
                   	</td>
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td class="action-btn">
                               
                   <a class="btn btn-small " title="Edit User" href="{{ URL::to('/posts_list/'.$value['id'].'/edit') }}"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small " title="Delete User" href="{{ URL::to('/posts_list/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   

			 
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                