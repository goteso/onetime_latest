@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  
  <style>
  
 .col-md-12{ margin-top:1%;}
#close{
    display:block;
	position:absolute;
	top:0;
	z-index:99999999999;
    float:left;
   
}
  </style>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Post Details</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
            <li class="active">Edit</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
              <!--  <div class="box-header">
                  <a href="{{ URL::to('admins/admin') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row" >
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit Post</h3>
                </div><!-- /.box-header -->
 
                     @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
  <div class="box-body">
				  
<div class="container-fluid" > 


 <div class="row">
	<?php	echo Form::open(array('route' => '/posts_list/update','files'=>'true','method'=>'POST')); ?>
     <input type="hidden" name="post_id" id="post_id" value="<?php echo $user->id;?>">
         <div class="col-md-12">
	      <div class="form-group">
            <label for="file">User</label>
			</br>
	            <a   title="Edit User" href="{{ URL::to('/user/'.$user->user_id.'/edit') }}">
				<?php echo @\App\User::where('id',$user->user_id)->first(['first_name'])->first_name; echo ' '.@\App\User::where('id',$user->user_id)->first(['last_name'])->last_name;
				    $user_status = @\App\User::where('id',$user->user_id)->first(['status'])->status;
				
				?></a>
	  <?php if($user_status == '1') { ?>
				  
                   <a class="btn btn-small btn-danger" title="Block User" href="{{ URL::to('admin/user/'.$user->user_id.'/block') }}" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small btn-danger" title="Unblock User" href="{{ URL::to('admin/user/'.$user->user_id.'/unblock') }}" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>				
          </div> 
	    </div>
	
		<div class="col-md-12">
	      <div class="form-group">
            <label for="file">Title</label>
	            <input type="text" name="title" id="title" placeholder="Enter Title Here" class="form-control" value="{{ $user->title}}">
          </div> 
	    </div>
		
		<div class="col-md-12" >
	      <div class="form-group">
            <label for="file">Description</label>
	            <textarea name="description" id="description" placeholder="Enter Description Here" class="form-control">{{ $user->description}}</textarea>
          </div> 
	    </div>
 
 
 
		<div class="col-md-12">
	      <div class="form-group">
            <label for="file">Type</label>
	            <select id="type" name="type" required class="form-control">
				    <option value="" >Select Post Type</option>
					<option value="normal" <?php if($user->type =='normal') { echo 'SELECTED';} ?>>General</option>
					<option value="sale" <?php if($user->type =='sale') { echo 'SELECTED';} ?>>Sale</option>
				</select>
          </div> 
	    </div>
		
 
		
		<!------POSTS IMAGES---------------------------->
		<div class="col-md-12">  <label for="file">Images</label>
		<div class="form-group">
		 
		<?php @$posts_images = @\App\PostsImages::where('post_id',$user->id)->get(); 
	 
		
		if(sizeof($posts_images) > 0)
		{
			foreach($posts_images as $im)
			{
			  ?>
			   <div id="panel" class="col-md-3"> <img src="<?php echo url('/')."/users/".$im->image?>" style="max-height:200;margin-right:1%" class="img-thumbnail">
			   	<a id="close" class="btn btn-small text-center " title="" href="{{ URL::to('/posts_list/images/'.$im->id.'/delete') }}" style="border-radius:50%;;background-color:#6F6F6F" onclick="return confirm('Are you sure?')"><i class="fa fa-close text-center" style="color:white;font-size:22px; "></i></a></div>
			   
		<?php 
			}
		}		
		?>
       </div>       
	   </div>
	   <!------POSTS IMAGES---------------------------->
	   
	   
	   
	   
	   
 
	   
	   
		<!------POSTS COMMENTS---------------------------->
		<?php $posts_comments = @\App\PostsComments::where('post_id',$user->id)->get(); 
		if(sizeof($posts_comments) > 0)
		{
		?>
  <div class="col-md-12" >  <label for="file">Comments</label>
  <div style="height:800px;overflow-y:auto">
  <table class="table" style="table-layout: fixed; width: 100%;max-height:800px;overflow:auto">
    <thead>
   
    </thead>
    <tbody style="height:800px;overflow-y:auto">
	
	<?php foreach($posts_comments as $c)
			{ ?>
      <tr>
        <td   >{{ $c->comment }}</td>
        <td ><b>by - </b>{{ @\App\User::where('id',$c->user_id)->first(['first_name'])->first_name}}</br>{{ $c->created_at}}</td>
      <td><a   class="btn btn-small text-center "   href="{{ URL::to('/posts_list/comments/'.$c->id.'/delete') }}" style="border-radius:50%;background-color:#F9F9F9" onclick="return confirm('Are you sure?')"><i class="fa fa-close text-center" style="color:#FC6794;font-size:15px; "></i></a></td>
      </tr>
			<?php }?>
    
    </tbody>
  </table>  
  </div>
</div>  
	   
		<?php } ?>
	   <!------POSTS IMAGES---------------------------->
 
		
		
 
		
		
 
 
	
 
	
	
	
 
  <div class="col-md-12">
	<a class="btn btn-small btn-danger" title="Delete Post" href="{{ URL::to('/posts_list/'.$user->id.'/delete') }}" onclick="return confirm('Are you sure?')">Delete Post</a>
    <button type="submit" class="btn btn-info">Submit</button>
	</div>  
  </form>
 


</div>
</div>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
 
 
	<?php if($user->image_type =='image') {  echo '<script>checkImage()</script>'; } ?>
	<?php if($user->image_type =='image_text') {  echo '<script>checkText()</script>'; } ?>
            
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop