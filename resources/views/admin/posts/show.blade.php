@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>User Details</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/student') }}">Users</a></li>
            <li class="active">View</li>
          </ol>
        </section>
        
        <section class="content">
           <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/user') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i>View All</a>
                </div><!-- /.box-header -->
            </div>

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                 <table  class="table  table-striped">
                    <thead>
					
					<?php
				 
					foreach ($user->getAttributes() as $k => $v) 
					{ 
					
						
						  echo '<tr><th width="15%">'.camel_case($k).'</th>';
						  
						  if($k == 'profile_image' || $k == 'verify_photo' )
						  {   echo '<td><img src="'.url('/').'/users/'.$v.'" height="200px"></td>';
					   
						  }
						  else if( $k == 'is_notification_on' || $k == 'status')
						  {
							   if($v == '1') { $v = 'ON';} if($v == '0') { $v = 'OFF';} 
							  echo '<td>'.$v.'</td>'; 
							   
						  }
						  
						    else if( $k == 'verified')
						  {
							  
							  
							   if($v == '1') { $v = '<span style="color:green">Verified</span>';} if($v == '0') { $v = '<span style="color:red"><a href="'.url('/admin/user/verify/'.$user->id ).'">Verify</a></span>'; }
							  
							  echo "<td>".$v."</td>";
							   
							 
							   
						  }
						  else
						  {
						     echo '<td>'.$v.'</td>';
						  }
                          echo '</tr>';
                   }
					?>
                   
                           
                       
                      </tr>
                    </thead>
                    <tbody>
                                                      
                    </tbody>
                    
                  </table>
             
            </div><!-- /.box-body -->
           
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop