@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
		
		<style>
		.ui-menu{ color:black;background-color:white;padding:1%}
	 
	 
		</style>
       <section class="content-header">
          <h1>
            <strong>Stores</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admin/user') }}">Users</a></li>
            <li class="active">Edit</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admin/user') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Edit Stores</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('url' => '/brands/brand/store/'.$id.'/update','class'=>'form-horizontal' , 'id'=>'formMap')) }}
                
                  <div class="box-body">
                   

               
			 <div class="form-group"> 
			   <div class="field">
				<label for="email" class="col-md-3 control-label">Find Location</label>
				 <div class="col-md-6"> 
				<input class="form-control" placeholder="Enter address or name of location to find on map" id="maps-location-search" type="text">
				</div>
			</div>
			</div>
			
			
			
				 <div class="form-group"> 
			   <div class="field">
				<label for="email" class="col-md-3 control-label">Coordinates</label>
				 <div class="col-md-6"> 
				 
				 <div class="col-md-4" style="padding-left:0;margin-left:0">
				<input type="text" id="latitude" class="form-control" name="latitude" placeholder="Latitude" value="<?php echo $user["latitude"];?>">  
				</div>
				<div class="col-md-4">
				<input class="form-control" type="text" id="longitude" name="longitude" placeholder="Longitude" value="<?php echo $user["longitude"];?>">
				 
				</div>
				
				<div class="col-md-4">
			 
				<button type="button" onclick="clearMap();" class="btn btn-md btn-info">Clear Coordinates</button>
				</div>
				</div>
				<div id="gmaps-error" class="error"></div>
			</div>
			</div>
			
			
						 <div class="form-group"> 
			   <div class="field">
				<label for="email" class="col-md-3 control-label"></label>
				 <div class="col-md-6"> 
			 <div class="  element-map" id="gmaps-canvas" class="field" style="clear: both;height:295px;border: 1px solid #999;">
			</div>
				</div>
				<div id="gmaps-error" class="error"></div>
			</div>
			</div>
			
		 
			


                    <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Email</label>
                      <div class="col-md-6">  
                      <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $user["email"];?>">
                      <div class="error-message">{{ $errors->first('email') }}</div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label for="status" class="col-md-3 control-label">Select Status</label>
                      <div class="col-md-6">  
                      <select name="status" id="status" class="form-control" class="col-md-3 control-label" >
                        <option value="">Select Status</option>
                        
                        <option <?php if('1' ==  $user["status"] ){?> selected="selected" <?php } ?> value="1">Active</option>
						<option <?php if('0' == $user["status"] ){?> selected="selected" <?php } ?> value="2">Disabled</option>
                        
                      </select>
                      <div class="error-message">{{ $errors->first('status') }}</div>
                    </div>
                  </div>
				  
				  
				     <div class="form-group">
                      <label for="short_address" class="col-md-3 control-label">Short Address</label>
                      <div class="col-md-6">  
                      <textarea type="text" name="short_address" class="form-control" id="short_address" placeholder="Enter Short Address"  ><?php echo $user["short_address"];?></textarea>
                      <div class="error-message">{{ $errors->first('short_address') }}</div>
                    </div>
                  </div>
				  
				  
				  
				  <div class="form-group">
                      <label for="long_address" class="col-md-3 control-label">Full Address</label>
                      <div class="col-md-6">  
                      <textarea rows="5" type="text" name="long_address" class="form-control" id="long_address" placeholder="Enter Short Address"  ><?php echo $user["long_address"];?></textarea>
                      <div class="error-message">{{ $errors->first('long_address') }}</div>
                    </div>
                  </div>
				  
				  
				   <div class="form-group">
                      <label for="store_notification_text" class="col-md-3 control-label">Notification Text</label>
                      <div class="col-md-6">  
                      <textarea rows="5" type="text" name="store_notification_text" class="form-control" id="store_notification_text" placeholder="Enter Notification Text"  ><?php echo $user["store_notification_text"];?></textarea>
                      <div class="error-message">{{ $errors->first('store_notification_text') }}</div>
                    </div>
                  </div>
				  
				  
				  
				  		  <div class="form-group">
                      <label for="store_beacon_UDID" class="col-md-3 control-label">Beacon UDID</label>
                      <div class="col-md-6">  
                      <textarea rows="5" type="text" name="store_beacon_UDID" class="form-control" id="store_beacon_UDID" placeholder="Enter Beacon UDID"  ><?php echo $user["store_beacon_UDID"];?></textarea>
                      <div class="error-message">{{ $errors->first('store_beacon_UDID') }}</div>
                    </div>
                  </div>
				  
				  
				  
 

            
                  </div><!-- /.box-body -->

                   <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </div>
                {{ Form::close() }}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
					 
			
			
				

	 
		
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js" type="text/javascript"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=AIzaSyCo0S5dcqwj11plZQyOn7Sx6VJPHQPVZko"></script>
		<script>
			var geocoder;
			var map;
			var marker;
			var formName = 'formMap';
			var formLatitudeField = 'latitude';
			var formLongitudeField = 'longitude';
			var formSearchLocation = 'maps-location-search';
			var formErrorText = 'gmaps-error';
			var existingLat;
			var existingLng;
			
			// Initialize Google Maps
			function initialize_google_maps(){
			
				// If no lat/lng values, center the map marker on 0,0 coordinates
				if (existingLat == null) {
					var latlng = new google.maps.LatLng(<?php echo $user["latitude"];?>,<?php echo $user["longitude"];?>);
					var zoomLevel = 3;
				} else {
					var latlng = new google.maps.LatLng(existingLat,existingLng,true);
					var zoomLevel = 7;
				}
			  
				// Set the options for the map
				var options = {
					zoom: zoomLevel,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
			
				// Create the Google Maps Object
				map = new google.maps.Map(document.getElementById("gmaps-canvas"), options);
			
				// Create Google Geocode Object that will let us do lat/lng lookups based on address or location name
				geocoder = new google.maps.Geocoder();
				
				// Add marker. Set draggable to TRUE to allow it to be moved around the map
				marker = new google.maps.Marker({
					map: map,
					draggable: true,
					position: latlng
				});
			
				// Listen for event when marker is dragged and dropped
				google.maps.event.addListener(marker, 'dragend', function() {
					update_ui('', marker.getPosition(), true);
					$('#' + formErrorText).html('');
				});
				
				// Listen for event when marker is dropped (map clicked)
				google.maps.event.addListener(map, 'click', function(event) {
				    marker.setPosition(event.latLng);
				    update_ui(event.latLng.lat() + ', ' + event.latLng.lng(), event.latLng, true);   
				    $('#' + formErrorText).html('');                                             
				});
			  
				// Listen for map zoom changing
				google.maps.event.addListener(map, 'zoom_changed', function() {
					zoomChangeBoundsListener = 
					    google.maps.event.addListener(map, 'bounds_changed', function(event) {
					        if (this.getZoom() > 15 && this.initialZoom == true) {
					            // Change max/min zoom here
					            this.setZoom(15);
					            this.initialZoom = false;
					        }
					    google.maps.event.removeListener(zoomChangeBoundsListener);
					});
				});
			  
			  map.initialZoom = true;
			
			}
			
			// Moves the map marker to a given lat/lng and centers the map on that location
			function update_map( geometry ) {		
				marker.setPosition(geometry.location);
				map.fitBounds(geometry.viewport);
			}
			
			// Updates form fields with address and/or lat/lng info
			function update_ui( address, latLng, plot ) {
				$('#' + formSearchLocation).autocomplete("close");
				
				// If we are plotting a point with the marker, we need to clear out
				// any text in location search.
				if (plot){
					$('#' + formSearchLocation).val('');
				}
				
			
			   	oFormObject = document.forms[formName];
				oFormLat = oFormObject.elements[formLatitudeField].value = latLng.lat();
				oFormLng = oFormObject.elements[formLongitudeField].value = latLng.lng();
			
			}
			
			// Query the Google geocode object
			//
			// type: 'address' for search by address
			//       'latLng'  for search by latLng (reverse lookup)
			//
			// value: search query
			//
			// update: should we update the map (center map and position marker)?
			function geocode_lookup( type, value, update ) {
			  // default value: update = false
			  update = typeof update !== 'undefined' ? update : false;
			
			  request = {};
			  request[type] = value;
			
			  geocoder.geocode(request, function(results, status) {
			    $('#' + formErrorText).html('');
			    if (status == google.maps.GeocoderStatus.OK) {
			      // Google geocoding has succeeded!
			      if (results[0]) {
			        // Always update the UI elements with new location data
			        update_ui( results[0].formatted_address,
			                   results[0].geometry.location,
			                   false )
			
			        // Only update the map (position marker and center map) if requested
			        if( update ) { update_map( results[0].geometry ) }
			      } else {
			        // Geocoder status ok but no results!?
			        $('#' + formErrorText).html("Sorry, something went wrong. Try again!");
			      }
			    } else {
			      // Google Geocoding has failed. Two common reasons:
			      //   * Address not recognised (e.g. search for 'zxxzcxczxcx')
			      //   * Location doesn't map to address (e.g. click in middle of Atlantic)
			
			      if( type == 'address' ) {
			        // User has typed in an address which we can't geocode to a location
			        $('#' + formErrorText).html("Google could not find " + value + ". Try a different search term, or click the map to manually select a location." );
			      }
			    };
			  });
			};
			
			
			
			
			
			
			$(document).ready(function() { 
				
				if( $('#gmaps-canvas').length  ) {
			
					
					initialize_google_maps();
					
					$('#' + formSearchLocation).autocomplete({
					
						// source is the list of input options shown in the autocomplete dropdown.
						// see documentation: http://jqueryui.com/demos/autocomplete/
						source: function(request,response) {
						
							// the geocode method takes an address or LatLng to search for
							// and a callback function which should process the results into
							// a format accepted by jqueryUI autocomplete
							geocoder.geocode( {'address': request.term }, function(results, status) {
								response($.map(results, function(item) {
									return {
										label: item.formatted_address, // appears in dropdown box
										value: item.formatted_address, // inserted into input element when selected
										geocode: item                  // all geocode data
									}
								}));
							})
						},
						
						// event triggered when drop-down option selected
						select: function(event,ui){
							update_ui(  ui.item.value, ui.item.geocode.geometry.location )
							update_map( ui.item.geocode.geometry )
						}
						
					});
					
					// triggered when user presses a key in the address box
				    $('#' + formSearchLocation).bind('keydown', function(event) {
				      if(event.keyCode == 13) {
				        geocode_lookup( 'address', $('#' + formSearchLocation).val(), true );
				  
				        // ensures dropdown disappears when enter is pressed
				        $('#' + formSearchLocation).autocomplete("disable")
				      } else {
				        // re-enable if previously disabled above
				        $('#' + formSearchLocation).autocomplete("enable")
				      }
				    });
				    
					
				
				};
			
			});
			
			function clearMap() {
			
				$('#' + formLatitudeField).val(null);
				$('#' + formLongitudeField).val(null);
				$('#' + formSearchLocation).val(null);
				$('#' + formErrorText).html(''); 
				
				var latLng = new google.maps.LatLng(0,0);
			  	var bounds = new google.maps.LatLngBounds(latLng);
				
				marker.setPosition(latLng);	
				map.setZoom(1);
			}
		</script>     
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop