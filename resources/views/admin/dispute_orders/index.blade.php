@extends('layout.admin')
@section('content')
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-app="myApp">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <strong>  Orders</strong>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
         <li class="active">Orders</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box  " style="display:none">
        
            <nav class="navbar filters-nav">
               <!--- 
                  <div class="navbar-header">
                    <b class="navbar-brand" >Filter By Status :</b>
                  </div>
                  <ul class="nav navbar-nav">
                  <li><a href="{{ URL::to('admins/admin/status/1') }}">Active({{ \App\Admin::where(['status' => 1])->get()->count() }})</a></li>
                    <li><a href="{{ URL::to('admins/admin/status/0') }}">Blocked({{ \App\Admin::where(['status' => 0])->get()->count() }})</a></li>
                  </ul>
                  
                   -->
            </nav> 
         <!-- /.box-header -->
      </div>
      <div class="row">
         <div class="col-xs-12">
		 
		



            <div class="box">
              <!-- <div class="box-header">
                  <h3 class="box-title">Orders Listing</h3>
               </div>
               <!-- /.box-header -->
               @if(Session::has('message'))
               <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
			   
               <div id="loading"></div>
               <style>
                  #loading {
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  -webkit-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
                  }
               </style>
			 <ul class="nav nav-tabs nav-tabs1">
             <li class="active"><a  data-toggle="tab" href="#orders" >Orders</a></li>	 
			 <li><a  href="{{URL::asset('admin/dispute_trades')}}">Trades</a></li>	  
</ul>   
			   
			   
			   <div class="tab-content">
  <div id="orders" class="tab-pane fade in active">
      <div class="panel ">
               <div id="item-lists">
                  @include('admin.dispute_orders.data-ajax')
               </div>
               {!! $result->render() !!}
			   
			   </div>
			   </div>
			   </div>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop