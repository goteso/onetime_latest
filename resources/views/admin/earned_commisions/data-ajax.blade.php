                <div class="box-body">
                  <table id="example1" class="table  table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
					    <th>Type</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Created</th>
                        <th id="action" style="display:none">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($data as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td><?php echo $value['type']; ?></td>
                    <td style=""><?php echo $value['description']; ?></td>
                     
                   	 <td style=""><?php echo abs($value['amount']); ?>  </td>
                 
				 
                    <td><?php echo $value['created_at']; ?></td>
                    <td class="action-btn" style="display:none">
                   <a class="btn btn-small " title="View Details" href="{{ URL::to('/posts_list/'.$value['id'].'/detail') }}"><i class="fa fa-eye"></i></a>  
                   <!--<a class="btn btn-small " title="Edit User" href="{{ URL::to('/posts_list/'.$value['id'].'/edit') }}"><i class="fa fa-edit"></i></a>                  
                  <a class="btn btn-small  " title="Delete User" href="{{ URL::to('/posts_list/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>   --->
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                