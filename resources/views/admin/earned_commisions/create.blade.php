@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Offers</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
            <li class="active">Add</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admins/admin') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Add Offer</h3>
                </div><!-- /.box-header -->
 
                
  
				  
<div class="container"> 
 
	
			<?php	echo Form::open(array('route' => '/offers_list/add_offers','files'=>'true','method'=>'POST')); ?>
    <div class="form-group">
      <label for="title">Title:</label>
      <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"  >
    </div>
   <div class="form-group">
     <label for="description">Description</label>
     <textarea class="form-control" rows="5" name="description" id="description"></textarea>
	   <div class="error-message">{{ $errors->first('first_name') }}</div>
  </div>

  
  <input type="hidden" name="brand_id" id="brand_id" value="<?php echo Auth::id()?>">
  
  <div class="form-group">
     <label >Image Type</label>
  <label class="radio-inline">
      <input type="radio" name="optradio" class="" value="image_text" id="text" onclick="checkText();">Text
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio" class="" id="image" value="image" onclick="checkImage();">Image
    </label>
	</div>
	
	<div id="textDiv">
	   <div class="form-group">
         <label for="image_text">Image Text</label>
         <textarea class="form-control" rows="5" name="image_text" id="image_text"></textarea>
      </div>
	  <div class="form-group">
         <label for="image_color">Image Color</label>
         <input type="color" name="image_color" id="image_color" class="image_color" value="#FFFFFF">
      </div>
	  
	  	  <div class="form-group">
         <label for="image_color">Font Color</label>
         <input type="color" name="font_color" id="font_color" class="image_color" value="#000">
      </div>
	  
	  
	</div>
	
	
	
	 

 
	
	<div id="imageDiv">
	   <div class="form-group">
         <label for="file">Description</label>
	 
		 
		  <?php   echo Form::file('image'); ?>
      </div> 
	</div>
	
    <div class="checkbox">
      <label><input type="checkbox" name="multiple_offers" class="multiple_offers" id="multiple_offers" onclick="valueChecked()"> Please check if you want to choose multiple offers.</label>
    </div>
	
	   <div class="form-group">
      <label for="title">Offers Count:</label>
      <input type="text" class="form-control" id="offers_count" name="offers_count" placeholder="Enter Items Count"  >
    </div>
	
	
	
	<div class="form-group">
     <label for="description">Expiry Date</label>
	 <input type='text' class="form-control" data-format="dd-MM-yyyy hh:mm:ss" id='expiry_date' name="expiry_date"/>
	</div>
	
	 <div class="checkbox">
      <label><input type="checkbox" name="automatic_approval" id="automatic_approval"  class="automatic_approval" onclick="valueChecked()"> Automatic approval</label>
    </div>
	
    <button type="submit" class="btn btn-info">Submit</button>
  </form>
</div>







<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
 
<script>
 $(function () {
               $('#expiry_date').datetimepicker();
           });
</script>
<script type="text/javascript">
   function valueChecked() {
       if ($('.multiple_offers').is(":checked"))
             document.getElementById("multiple_offers").value = '1';
       else
          document.getElementById("multiple_offers").value = '0'; 
          
          
            if ($('.automatic_approval').is(":checked"))
             document.getElementById("automatic_approval").value = '1';
       else
          document.getElementById("automatic_approval").value = '0';
   }
</script>

<script>

document.getElementById('imageDiv').style.display ='none';
 document.getElementById('textDiv').style.display = 'none';
 
function checkText(){
   document.getElementById('imageDiv').style.display ='none';
   document.getElementById('textDiv').style.display = 'block';
    document.getElementById('file').value = '';
}
function checkImage(){
 document.getElementById('textDiv').style.display = 'none';
  document.getElementById('imageDiv').style.display ='block';
  document.getElementById('image_text').value = '';
  document.getElementById('image_color').value = '#FFFFFF';
}
</script>

            
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop