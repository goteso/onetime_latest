                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr. No.</th>
						<th>Image</th>
                        <th>Title</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th>Status</th>
						<th>Stores</th>
                        <th>Created</th>
                        <th id="action">Action</th>                        
                      </tr>
                    </thead>
                   <tbody>

                   	 <?php $sr = 0; foreach ($users as $key => $value) { ?>
                   	<tr>
                   	<td><?php echo ++$sr; ?></td>
					<td><img src="<?php echo url('/').'/'.$value['photo'];?>" height="200px" ></td>
                   	<td><?php echo $value['first_name']." ".$value['last_name']; ?></td>
                    <td><?php echo $value['user_type']; ?></td>
                    <td><?php echo $value['email']; ?></td>
                    
                   	<td><?php
                   	 if($value->status == 0)
                   	 	{
                   	 	echo "Disabled";
                   	   }
                   	   else
                   	   {
                   	    echo "Active";
                   		} ?>
                   	</td>
					      <td><?php 
					$stores_count = @\App\Admin::where("store_brand_id",$value['id'])->get()->count();
					?> 
						<a  href="{{ URL::to('brands/brand/'.$value['id'].'/stores') }}" target="_blank">Stores <?php if($stores_count > 0)
					{ ?> <span class="badge"><?php echo $stores_count;?></span> <?php } ?> 
						</a>
					 
					 
				 
					</td>
					<td><?php echo $value['created_at']; ?></td>
              
                    <td>
                   <a class="btn btn-small btn-success" title="View User" href="{{ URL::to('brands/brand/'.$value['id']) }}"><i class="fa fa-info"></i></a>                
                   <a class="btn btn-small btn-info" title="Edit User" href="{{ URL::to('brands/brand/'.$value['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>                  
                  <a class="btn btn-small btn-danger" title="Delete User" href="{{ URL::to('brands/brand/'.$value['id'].'/delete') }}" onclick="return confirm('Are you sure?')"><i class="fa fa-remove"></i></a>   

				  <?php if($value['status'] == '1') { ?>
				  
                   <a class="btn btn-small btn-danger" title="Block User" href="{{ URL::to('brands/brand/'.$value['id'].'/block') }}" onclick="return confirm('Are you sure to Block this User?')"><i class="fa fa-ban"></i></a>  
				  <?php } else { ?>
                   <a class="btn btn-small btn-danger" title="Unblock User" href="{{ URL::to('brands/brand/'.$value['id'].'/unblock') }}" onclick="return confirm('Are you sure to Unblock this User?')"><i class="fa fa-unlock"></i></a>  
				  <?php } ?>
 
            	   </td>
                   	 </tr>
                   <?php }?>
                   
                   </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
                