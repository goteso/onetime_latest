@extends('layout.admin')
@section('content')
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
          <h1>
            <strong>Brands</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="{{ URL::to('admins/admin') }}">Admins</a></li>
            <li class="active">Add</li>
          </ol>
        </section

        <!-- Main content -->
        <section class="content">
          <div class="box box-info">
                <div class="box-header">
                  <a href="{{ URL::to('admins/admin') }}" class="pull-right btn btn-info btn-sm" ><i class="fa fa-view"></i> View All </a>
                </div><!-- /.box-header -->
            </div>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">

              <!-- general form elements -->
              <div class="box box-primary">

                <div class="box-header with-border">
                  <h3 class="box-title">Add Brand</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('url' => 'brands/brand/store','class'=>'form-horizontal','files'=>'form-true','method'=>'POST')) }}
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="first_name" class="col-md-3 control-label">Title</label>
                       <div class="col-md-6">  
                      <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name" value="{{old('first_name')}}">
                      <div class="error-message">{{ $errors->first('first_name') }}</div>
                    </div></div>

                


                    <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Email</label>
                      <div class="col-md-6">  
                      <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="{{old('email')}}">
                      <div class="error-message">{{ $errors->first('email') }}</div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label for="status" class="col-md-3 control-label">Select Status</label>
                      <div class="col-md-6">  
                      <select name="status" id="status" class="form-control" class="col-md-3 control-label" value="{{old('status')}}">
                        <option value="">Select Status</option>
                        
                        <option <?php if('1' == old('status') ){?> selected="selected" <?php } ?> value="1">Active</option>
						<option <?php if('0' == old('status') ){?> selected="selected" <?php } ?> value="0">Disabled</option>
                        
                      </select>
                      <div class="error-message">{{ $errors->first('status') }}</div>
                    </div>
                  </div>
				  
				  
				  
				      <div class="form-group">
                      <label for="email" class="col-md-3 control-label">Image</label>
                      <div class="col-md-6">  
                      <?php   echo Form::file('image');?>
                    </div>
                  </div>
		
				  
				  
				  

                     <div class="form-group">
                      <label for="password" class="col-md-3 control-label">Password</label>
                      <div class="col-md-6">  
                      <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" maxlength="20">
                      <div class="error-message">{{ $errors->first('password') }}</div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label for="password_confirmation" class="col-md-3 control-label">Confirm Password</label>
                      <div class="col-md-6">  
                      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" maxlength="20" placeholder="Enter Confirm Password">
                      <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                    </div>
                    </div>
                  </div><!-- /.box-body -->

                   <div class="box-footer">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </div>
                {{ Form::close() }}
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@stop