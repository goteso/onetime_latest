@extends('layout.admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <strong>Admin</strong>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>            
            <li class="active">Change password</li>
          </ol>
        </section>

        <!-- Main content -->
    <section class="content">
            <div class="row">
               
                <div class="col-md-12">
                    <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Change Admin Password</h3>
                </div><!-- /.box-header -->
                 @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif
                <!-- form start -->
                <?php echo Form::open(array('url' => 'admin/change-password','method'=>'put','class'=>'form-horizontal')) ?>
                  <div class="box-body ">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Enter New Password</label>
                      <div class="col-sm-4">
                        <input type="password" name="password"  class="form-control" id="inputPassword3" placeholder="New Password" aria-describedby="password"/>
                        <div class="error-message">{{ $errors->first('password') }}</div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label">Confirm Password</label>
                      <div class="col-sm-4">
                        <input type="password" name="password_confirmation" class="form-control" id="inputPassword3" placeholder="Confirm Password" aria-describedby="password_confirmation"/>
                        <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                      </div>
                    </div>
                                    
                  </div><!-- /.box-body -->
                  <div class="box-footer ">
                    <div class="col-sm-offset-3 col-sm-2">
                        <button type="submit" class="btn btn-info">Change Password</button>
                    </div>
                  </div><!-- /.box-footer -->
                {{ Form::close() }}
              </div><!-- /.box -->
                </div>
            </div>
      
    </section>
    <!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script>
      (function () {
    $("#example1").DataTable({
      "aoColumnDefs": [
        { 
          "bSortable": false, 
          "aTargets": [ -1 ] // <-- gets last column and turns off sorting
         } 
     ]
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
      </script>
@stop