@extends('layout.admin')
@section('content')

     <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
           	  <!-- Main content -->
        <section class="content">
		 

          <!-- Small boxes (Stat box) -->
          <div class="row">
          	<!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-brown" style="background-color:#00A9C1;color:white">
                <div class="inner">
                  <h3><?php echo $total_users;?></h3>
                  <p>Total User </p>	 
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
            
              </div>
            </div>
			       <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_trades;?></h3>
                  <p>Total Bitcoin Trades</p>	 
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
            
              </div>
            </div>
		       <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $total_transactions;?></h3>
                  <p>Total Orders</p>	 
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
            
              </div>
            </div>
			       <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3><?php echo $total_posts;?></h3>
                  <p>Total Posts</p>	 
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
            
              </div>
            </div>
 
 
 
          </div><!-- /.......row -->
	<!-- =========================================================== -->
 
 <textarea id="res" style="display:none"></textarea>
   
 
	 
          	<div class="row">
                
				<!--------users graph starts--------->
				<div class="col-md-4">
                    <div class="box box-success">
					 <div class="col-md-12 col-lg-12" style="width:100%; padding:0">
		                             <select class="form-control" id="agent_report_filter" onchange="get_users_data(this.value)" style="width:100%;margin:0;padding:0">
				                        <option value="monthly">Monthly</option>
				                        <option value="yearly">Yearly</option>
                                        <option value="weekly">Weekly</option>
				                
				                      </select>
					 </div>
		              <div class="col-md-12 col-lg-12" id="chartContainer" style="height: 370px;margin:0;padding:0">
		              </div>
				   </div>
			    </div>
				<!--------users graph ends--------->
				
				
				
				
				
				<!--------Likes graph starts--------->
				<div class="col-md-4">
                    <div class="box box-success">
					 <div class="col-md-12 col-lg-12" style="width:100%; padding:0">
		                             <select class="form-control" id="agent_report_filter" onchange="get_transaction_data(this.value)" style="width:100%;margin:0;padding:0">
				                        <option value="monthly">Monthly</option>
				                        <option value="yearly">Yearly</option>
                                        <option value="weekly">Weekly</option>
				                       
				                      </select>
					 </div>
		              <div class="col-md-12 col-lg-12" id="transaction_chart" style="height: 370px;margin:0;padding:0">
		              </div>
				   </div>
			    </div>
				<!--------users graph ends--------->
				
				
				
			
								<!--------Genders--------->
				<div class="col-md-4">
                    <div class="box box-success">
				 
		              <div class="col-md-12 col-lg-12" id="genders_chart" style="height: 400px;margin:0;padding:0">
		              </div>
				   </div>
			    </div>
				<!--------users graph ends--------->
				
				
			
		  </div>
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		            	<div class="row" style="margin-top:1.8%">
                 
				<!--------Likes graph starts--------->
				<div class="col-md-4">
                    <div class="box box-success">
			 
		              <div class="col-md-12 col-lg-12" id="countries_chart" style="min-height: 300px;margin:0;padding:0">
		              </div>
				   </div>
			    </div>
				<!--------users graph ends--------->
				 
		  </div>
		  
		  
		
		  
		  
 
            
          </div> 


        </section> 
     		</div>
     
	
	 
 
 <script type="text/javascript">
function get_users_graph(data)
{
CanvasJS.addColorSet("greenShades",["#8BE182","#FE8D9B","#8C8CFF","#C2C285","#FF80C0"]);
var d = data;
var d2 = d.replace(/\\/g, "");
d2 = d2.substr(1).slice(0, -1);
var chart = new CanvasJS.Chart("chartContainer", {
	//theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
    colorSet:  "greenShades",
	title:{
       text: "Users Registrations",             
        fontColor: "black",
	    fontFamily:"verdana",
		margin:0,
      },
      axisY:{
		  lineColor:"white",
       // interlacedColor: "rgb(255,250,250)",
       gridColor: "#ccc"
      },
	       axisX:{
			     interval: 2,	
		  lineColor:"white",
		  //labelAngle: -30,
		  gridThickness:0
       // interlacedColor: "rgb(255,250,250)",
       // gridColor: "#FFBFD5"
      },
   data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "line",
		   showInLegend: true, 
		   name: "series1",
		    legendText: "Users Count",
		colorSet: "greenShades",
		dataPoints: JSON.parse(d2)
	}
	]
}
 

);
chart.render();

};




function get_transaction_graph(data)
{
CanvasJS.addColorSet("greenShades",["#8BE182","#FE8D9B","#8C8CFF","#C2C285","#FF80C0"]);
var d = data;
var d2 = d.replace(/\\/g, "");
d2 = d2.substr(1).slice(0, -1);
var chart = new CanvasJS.Chart("transaction_chart", {
	//theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
    colorSet:  "greenShades",
	title:{
       text: "Total Transactions",             
        fontColor: "black",
	    fontFamily:"verdana",
		margin:0,
      },
      axisY:{
		  lineColor:"white",
       // interlacedColor: "rgb(255,250,250)",
       gridColor: "#ccc"
      },
	       axisX:{
			     interval: 2,	
		  lineColor:"white",
		  //labelAngle: -30,
		  gridThickness:0
       // interlacedColor: "rgb(255,250,250)",
       // gridColor: "#FFBFD5"
      },
   data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "line",
		   showInLegend: true, 
		   name: "series1",
		    legendText: "Transactions",
		colorSet: "greenShades",
		dataPoints: JSON.parse(d2)
	}
	]
}
 

);
chart.render();

};
 


function get_likes_graph(data){
	 
CanvasJS.addColorSet("greenShades",["#8BE182","#FE8D9B","#8C8CFF","#C2C285","#FF80C0"]);
var d = data;
var d2 = d.replace(/\\/g, "");
d2 = d2.substr(1).slice(0, -1);

var chart2 = new CanvasJS.Chart("likes_chart", {
	//theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
    
	colorSet:  "greenShades",
	title:{
        text: "Offer Likes",
         
        fontColor: "black",
	    fontFamily:"verdana",
		margin:0,
		
      },
      axisY:{
		  lineColor:"white",
       // interlacedColor: "rgb(255,250,250)",
       gridColor: "#ccc"
      },
	       axisX:{
			     interval: 2,	
		  lineColor:"white",
		  //labelAngle: -30,
		  gridThickness:0,
       // interlacedColor: "rgb(255,250,250)",
       gridColor: "#ccc"
      },
   data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "area",
		 markerType: "triangle",
		showInLegend: true, 
		name: "series1",
		legendText: "Offer Likes",
		colorSet: "greenShades",
		dataPoints: JSON.parse(d2)
	}
	]
}
 

);
chart2.render();

};




function get_countries_graph(data){
	 
CanvasJS.addColorSet("greenShades",["#8BE182","#FE8D9B","#8C8CFF","#C2C285","#FF80C0"]);
var d = data;
var d2 = d.replace(/\\/g, "");
d2 = d2.substr(1).slice(0, -1);
 
var chart3 = new CanvasJS.Chart("countries_chart", {
	//theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
    
	colorSet:  "greenShades",
	title:{
       text: "Users by Countries",             
        fontColor: "black",
	    fontFamily:"verdana",
		margin:0,
      },
        axisY:{
		  lineColor:"white",
       // interlacedColor: "rgb(255,250,250)",
        gridColor: "#ccc"
      },
	       axisX:{
			     interval: 1,	
		  lineColor:"white",
		  //labelAngle: -30,
		  gridThickness:0,
      //  interlacedColor: "rgb(255,250,250)",
        gridColor: "#ccc"
      },
   data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "bar",
		showInLegend: true, 
		name: "series1",
		legendText: "Users Count",
		colorSet: "greenShades",
		dataPoints: JSON.parse(d2)
	}
	]
}
 

);
chart3.render();

};






function get_genders_graph(data){
	 
CanvasJS.addColorSet("greenShades",["#8BE182","#FE8D9B","#8C8CFF","#C2C285","#FF80C0"]);
var d = data;
var d2 = d.replace(/\\/g, "");
d2 = d2.substr(1).slice(0, -1);
 
var chart4 = new CanvasJS.Chart("genders_chart", {
	//theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
    
	colorSet:  "greenShades",
	title:{
       text: "Users by Gender",             
        fontColor: "black",
	    fontFamily:"verdana",
		margin:0,
      },
        axisY:{
		  lineColor:"white",
       // interlacedColor: "rgb(255,250,250)",
        gridColor: "#ccc"
      },
	       axisX:{
			     interval: 1,	
		  lineColor:"white",
		  //labelAngle: -30,
		  gridThickness:0,
      //  interlacedColor: "rgb(255,250,250)",
        gridColor: "#ccc"
      },
   data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "pie",
		showInLegend: true, 
		name: "series1",
		 
		colorSet: "greenShades",
		dataPoints: JSON.parse(d2)
	}
	]
}
 

);
chart4.render();

};


</script>



 
      
      <script>
	 //get users data 
    function get_users_data(type='monthly')
    {
		 $.ajax({
                url: '<?php echo url('/')?>/admin/ajax_users_data',
                dataType: 'text',
                type: 'post',
                 data: {_token: 'ss',type:type },
 
                success: function( data, textStatus, jQxhr ){
                    //console.log( data );
                  //document.getElementById('users_res').value = JSON.stringify(data);
				  get_users_graph(JSON.stringify(data));
                },
                error: function( jqXhr, textStatus, errorThrown ){
                 document.getElementById('users_res').value = JSON.stringify(errorThrown);
                }
            });
    }
	get_users_data();
	
	
	
	//Get likes data
	    function get_likes_data(type='monthly')
    {
		 $.ajax({
                url: '<?php echo url('/')?>/admin/ajax_likes_data',
                dataType: 'text',
                type: 'post',
                 data: {_token: 'ss',type:type },
 
                success: function( data, textStatus, jQxhr ){
					//document.getElementById('likes_res').value = JSON.stringify(data);
                   get_likes_graph(JSON.stringify(data));
				  
				   
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    //document.getElementById('res').value = JSON.stringify(errorThrown);
					 
                }
            });
    }
		 
	get_likes_data();
	
	
	
	
	
		//Get likes data
	    function get_transaction_data(type='monthly')
    {
		 $.ajax({
                url: '<?php echo url('/')?>/admin/ajax_transaction_data',
                dataType: 'text',
                type: 'post',
                 data: {_token: 'ss',type:type },
 
                success: function( data, textStatus, jQxhr ){
					//document.getElementById('likes_res').value = JSON.stringify(data);
                   get_transaction_graph(JSON.stringify(data));
			 
				   
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    document.getElementById('res').value = JSON.stringify(jqXhr);
					 //alert('e');
                }
            });
    }
		 
	get_transaction_data();
	
	
	
	
	
	
	
	
	
	
		    function get_countries_data(type='monthly')
    {
		 $.ajax({
                url: '<?php echo url('/')?>/admin/get_countries_data',
                dataType: 'text',
                type: 'post',
                 data: {_token: 'ss',type:'sddsdsd' },
 
                success: function( data, textStatus, jQxhr ){
              
           
				  get_countries_graph(JSON.stringify(data));
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    document.getElementById('countries_res').value = JSON.stringify(errorThrown);
					//alert('error');
                }
            });
    }
		 
	get_countries_data();
	
	
	
	
	 function get_genders_data(type='monthly')
    {
		 $.ajax({
                url: '<?php echo url('/')?>/admin/get_genders_data',
                dataType: 'text',
                type: 'post',
                 data: {_token: 'ss',type:'sddsdsd' },
 
                success: function( data, textStatus, jQxhr ){
              
           
				  get_genders_graph(JSON.stringify(data));
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    //document.getElementById('countries_res').value = JSON.stringify(errorThrown);
					//alert('error');
                }
            });
    }
		 
	get_genders_data();
	
	
      </script>
	  
	  
	  
	  
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
@stop