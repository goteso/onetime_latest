<!-- Left side column. contains the logo and sidebar -->
       <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!--<div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
		  
		  
		  <style>
		  .actives { background-color:#ccc;color:white }
		  
		  </style>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
			 
            <li class="{{ Request::segment(2) === 'dashboard' ? 'active' : null }}">
              <a href="{{ URL::to('admin/dashboard') }}" ><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
             </li>

			 	<?php if(auth()->user()->user_type == 'admin')
			{?>
             <li class="treeview">
              <a  class=" <?php if(request()->segment(count(request()->segments())) == 'user') { echo 'actives';} else { echo '';}?>" href="{{ URL::to('user') }}">
                <i class="fa fa-user"></i>  
                <span>User Management</span>
                <span class="label label-primary pull-right"></span>
              </a>
           
            </li>
			<?php } ?>
			
			
			
			
			
			
			
			
			<?php if(auth()->user()->user_type == 'admin')
			{?>
				
			 
			            <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admins') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'admins') { echo 'actives';} else { echo '';}?>" >
                <i class="fa fa-user"></i>  
                <span>Admin Management</span>
                <span class="label label-primary pull-right"></span>
              </a>
             
            </li>
			<?php } ?>
			
			 
				
     <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/app_data') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'app_data') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-flag" aria-hidden="true"></i> 
                <span>App Data</span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
 
			
			
 
	  
	
	
	
						 	<?php if(auth()->user()->user_type == 'admin')
			{?>
             <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('posts_list') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'posts_list') { echo 'actives';} else { echo '';}?>" >
                <i class="fa fa-sticky-note"></i>  
                <span>Posts Management</span>
                <span class="label label-primary pull-right"></span>
              </a>
           
            </li>
			<?php } ?>
			
			
			
			
			
			
			
			
			
			
									 	<?php if(auth()->user()->user_type == 'admin')
			{?>
             <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/reported_posts') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'reported_posts') { echo 'actives';} else { echo '';}?>" >
                <i class="fa fa-sticky-note"></i>  
                <span>Reported Posts</span>
                <span class="label label-primary pull-right"></span>
              </a>
           
            </li>
			<?php } ?>
			
			
			
			
			
			
	
	
	
							<?php if(auth()->user()->user_type == 'admin')
			{?>
				    <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/post_orders') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'post_orders') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-truck" aria-hidden="true"></i> 
                <span>Orders</span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
 	<?php } ?>
	
	
	
	
								<?php if(auth()->user()->user_type == 'admin')
			{?>
				    <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/trades_orders') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'trades_orders') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-flag" aria-hidden="true"></i> 
                <span>Buy & Sell Bitcoins</span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
 	<?php } ?>
	
	
	
	
	
	
								<?php if(auth()->user()->user_type == 'admin')
			{?>
				    <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/trades') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'trades') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-bitcoin" aria-hidden="true"></i> 
                <span>Bitcoin Trades </span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
 	<?php } ?>
	
	
	
									<?php if(auth()->user()->user_type == 'admin')
			{?>
				    <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/earned_commisions') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'earned_commisions') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-bitcoin" aria-hidden="true"></i> 
                <span>Earned Commisions </span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
 	<?php } ?>
	
	
	
	
	
	
	
	
	
	
	
				<?php if(auth()->user()->user_type == 'admin')
			{?>
			    <li class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
              <a href="{{ URL::to('admin/terms_conditions') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'terms_conditions') { echo 'actives';} else { echo '';}?>" >
                 <i class="fa fa-wrench" aria-hidden="true"></i> 
                <span>Settings</span>
                <span class="label label-primary pull-right"></span>
              </a>
            
            </li>
				<?php } ?>
				
 
 
           
            <li class="{{ Request::segment(2) === 'change-password' ? 'active' : null }}">
              <a href="{{ URL::to('admin/disputed_orders') }}"  class=" <?php if(request()->segment(count(request()->segments())) == 'disputed_orders') { echo 'actives';} else { echo '';}?>" >
			  <i class="fa fa-key"></i> <span>Disputed</span></a>
             </li>

			 


             
            
    
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>