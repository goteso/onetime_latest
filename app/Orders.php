<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{

protected $fillable = [ 'user_id','seller_id','post_id','address','price','shipping_method_id','status','paypal_transaction_id'];
protected $table = 'orders';
	
public function posts(){
  return $this->belongsTo('App\Posts', 'post_id');
}

public function users(){
  return $this->belongsTo('App\User', 'user_id');
}

public function posts_shipping_methods()
{
        return $this->hasMany('App\PostsShippingMethods','post_id');
}
	

public function seller(){
  return $this->belongsTo('App\User', 'seller_id');
}


public function buyer(){
  return $this->belongsTo('App\User', 'user_id');
}
 
 
 

 
 
  public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
	
     public function getSellerNameAttribute($value) {
		 $v = @\App\User::where('id',$this->seller_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->seller_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	  
public function getSellerPhoneAttribute($value) {
		 $v = @\App\User::where('id',$this->seller_id)->first(['phone'])->phone;
		 return $v;
	  }
	  
	  
public function getSellerEmailAttribute($value) {
		 $v = @\App\User::where('id',$this->seller_id)->first(['email'])->email;
		 return $v;
	  }
	  
	  
	  
	  public function getOrderPriceAttribute($value) {
		 $v = @\App\Posts::where('id',$this->post_id)->first(['price'])->price;
		 return $v;
	  }
	  
	  
	  
	  
	  	  public function getOrderCurrencyAttribute($value) {
			  
			  
		 $v = @\App\Posts::where('id',$this->post_id)->first(['currency'])->currency;
		 return $v;
	  }
	  
	  
	  	  public function getPostCurrencyAttribute($value) {
		 $v = @\App\Posts::where('id',$this->post_id)->first(['currency'])->currency;
		 return $v;
	  }
	  
	  	  	  public function getPostTitleAttribute($value) {
		 $v = @\App\Posts::where('id',$this->post_id)->first(['title'])->title;
		 return $v;
	  }
	  
	  
	
  	  public function getUserNameAttribute($value) {
		 $v = @\App\User::where('id',$this->user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->user_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	  
	  
	  
	
	
	
	
	
 
	  public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }
	
 
	
 
 
	
	
}