<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradesPosts extends Model
{
        protected $fillable = [ 'type', 'user_id', 'currency', 'trade_rate_percentage', 'trade_rate', 'country', 'bank_details', 'range_min', 'range_max', 'terms_and_conditions'];
		protected $table = 'trades_posts';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
		 	  public function getSellerNameAttribute($value) {
		 $v = @\App\User::where('id',$this->seller_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->seller_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	  
	  		 	  public function getUserNameAttribute($value) {
		 $v = @\App\User::where('id',$this->user_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->user_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	  
	  
	  
	  
  public function getFinalBuyerNameAttribute($value) {
	  
	      $buyer_id = @\App\Trades::where('trades_posts_id',$this->id)->where('status','completed')->first(['buyer_id'])->buyer_id;		
 
         $v = @\App\User::where('id',$buyer_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$buyer_id)->first(['last_name'])->last_name;
		 return $v;
	  }
 
 

	  
	  
	
	
}