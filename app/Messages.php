<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Messages extends Model
{
        protected $fillable = ['user1','user2','message','type','read_status','trades_posts_id'];
		protected $table = 'messages';
		
			    public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], ' sec', $v);
        $v = str_replace([' minutes', ' minute'], ' min', $v);
        $v = str_replace([' hours', ' hour'], ' h', $v);
        $v = str_replace([' months', ' month'], ' m', $v);
		$v = str_replace([' days', ' month'], ' d', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }
	
	
}