<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trades extends Model
{
        protected $fillable = [ 'trades_posts_id', 'seller_id', 'buyer_id', 'other_amount', 'btc_amount', 'bank_details','status','completed_time','transferred_time'];
		protected $table = 'trades';
		
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
	
	
	 	  public function getBuyerNameAttribute($value) {
		 $v = @\App\User::where('id',$this->buyer_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->buyer_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	   	  public function getBuyerPhoneAttribute($value) {
		 $v = @\App\User::where('id',$this->buyer_id)->first(['phone'])->phone;
		 return $v;
	  }
	  
	   	  public function getBuyerEmailAttribute($value) {
		 $v = @\App\User::where('id',$this->buyer_id)->first(['email'])->email;
		 return $v;
	  }
	  
	  
	  
	  	   	  public function getBuyOrSellTitleAttribute($value) {
		 $v = @\App\TradesPosts::where('id',$this->trades_posts_id)->first(['type'])->type;
		 return $v;
	  }
	  
	  
	  
	  
	   	  public function getSellerNameAttribute($value) {
		 $v = @\App\User::where('id',$this->seller_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$this->seller_id)->first(['last_name'])->last_name;
		 return $v;
	  }
	  	   	  public function getSellerPhoneAttribute($value) {
		 $v = @\App\User::where('id',$this->buyer_id)->first(['phone'])->phone;
		 return $v;
	  }
	  
	   	  public function getBuyerSellerAttribute($value) {
		 $v = @\App\User::where('id',$this->buyer_id)->first(['email'])->email;
		 return $v;
	  }
	  
	  
	  
	  	   	  public function getTradePostDetailsAttribute($value) {
		 $v = @\App\TradesPosts::where('id',$this->trades_posts_id)->first();
		 return $v;
	  }
	  
	  
	  
	    public function getTradesPostsCurrencyAttribute($value) {
	  
	 
	  $v = @\App\TradesPosts::where('id',$this->trades_posts_id)->first(['currency'])->currency;
	  return $v;
	  
  }
  
  
  
	

 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}