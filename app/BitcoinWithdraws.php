<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitcoinWithdraws extends Model
{
        protected $fillable = [ 'user_id', 'destination_wallet','amount'];
		protected $table = 'bitcoin_withdraws';
		
		
		
			
	    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
}