<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
        protected $fillable = [ 'type','user_id','linked_id','description','amount','deleted','commision'];
		protected $table = 'transactions';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
	  	  	  public function getPostTitleAttribute($value) {
		 $v = @\App\Posts::where('id',$this->linked_id)->first(['title'])->title;
		 return $v;
	  }
	  
	
}