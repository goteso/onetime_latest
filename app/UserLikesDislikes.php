<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLikesDislikes extends Model
{
        protected $fillable = [
        'user_id','profile_user_id','type'
    ];
}
