<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockUnblock extends Model
{
        protected $fillable = [
        'user_id','block_user_id','is_block'
    ];

    protected $table = "block_unblock";
    
    public function user()
    {
    return $this->belongsTo('App\User', 'block_user_id');
    }
	
	
		    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}
