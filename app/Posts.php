<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $fillable = [  'title', 'description', 'type', 'payment_method', 'price', 'terms_and_conditions','currency','paypal_id','latitude','longitude','country'];
  protected $table = 'posts';
  
  
    public function posts_images()
    {
        return $this->hasMany('App\PostsImages','post_id')->orderBy('id','DESC');
    }
	
	    public function posts_comments()
    {
        return $this->hasMany('App\PostsComments','post_id');
    }
	
		    public function posts_shipping_methods()
    {
        return $this->hasMany('App\PostsShippingMethods','post_id');
    }
	
	
	
	public function orders()
    {
        return $this->hasOne('App\Orders','post_id');
    }
	
	
	 public function getPostIdAttribute($value)
    {
         return "{$this->id}";
    }



	 public function getCurrencyCodeAttribute($value)
    {

 
       $currency_code = @\App\PaypalCurrencyCodes::where('currency_symbol',$this->currency)->first(['currency_code'])->currency_code;
       return $currency_code;
        
    }



 

    /**
     * Always capitalize the last name when we retrieve it
     */
			    public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }





 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
	
}