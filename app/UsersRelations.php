<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRelations extends Model
{
        protected $fillable = [ 'recipient_id', 'user_id','status'];
		protected $table = 'users_relations';
}