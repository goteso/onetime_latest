<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermsConditions extends Model
{
        protected $fillable = [ 'terms_conditions','images'];
}