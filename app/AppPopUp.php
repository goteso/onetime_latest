<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppPopUp extends Model
{
        protected $fillable = [ 'pop_up_content'];
		protected $table = 'app_pop_up';
}