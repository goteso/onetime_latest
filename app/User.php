<?php

namespace App;

 


use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use \Danjdewhurst\PassportFacebookLogin\FacebookLoginTrait;
use \Adaojunior\Passport\SocialGrantException;
use \Adaojunior\Passport\SocialUserResolverInterface;

class User extends Authenticatable implements SocialUserResolverInterface
{
	
	use HasApiTokens, Notifiable;
	
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
'first_name', 'last_name', 'email', 'dob', 'gender', 'login_type', 'facebook_id', 'phone', 'profile_image', 'profile_image_thumb', 'status', 'password','encrypted_password', 'notification_token', 'notify_new_order', 'notify_following', 'notify_comment', 'notify_like', 'notify_new_trade', 'badge_count', 'address', 'latitude', 'longitude', 'country','one_time_code','about','remember_token','timezone','pgp_key','wallet_pin','wallet_address','verified','username','private_status','preferred_currency'
    ];

	
	
	    public function resolve($network, $accessToken, $accessTokenSecret = null)
    {
        switch ($network) {
            case 'facebook':
                return $this->authWithFacebook($accessToken);
                break;
            default:
                throw SocialGrantException::invalidNetwork();
                break;
        }
    }
    
	
	
	
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password' 
    ];
	
	
	protected $table = 'users';
		public function orders()
    {
        return $this->hasMany('App\Orders','user_id');
    }

	
	public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }


	
 
	
 


 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

 

	
	

}
