<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportedUsers extends Model
{
        protected $fillable = [ 'reported_user_id', 'user_id', 'status'];
		protected $table = 'reported_users';
		
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
 
	
	public function getReportedUserDetailsAttribute($value) {
          return  @\App\User::where('id',$this->reported_user_id)->first();
    }
	
	
		public function getReportedByUserDetailsAttribute($value) {
          return  @\App\User::where('id',$this->user_id)->first();
    }
	
 

 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
	
	
	
}