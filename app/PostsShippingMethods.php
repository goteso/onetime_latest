<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsShippingMethods extends Model
{
        protected $fillable = ['title','price','post_id'];
		protected $table = 'posts_shipping_methods';
		
		
	public function orders()
    {
        return $this->belongsTo('App\Orders','post_id');
    }
	
	
}