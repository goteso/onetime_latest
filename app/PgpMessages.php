<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class PgpMessages extends Model
{
        protected $fillable = ['sender_id','receiver_id','message','read_status'];
		protected $table = 'pgp_messages';
		
		
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}