<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowUnfollow extends Model
{
        protected $fillable = [
        'user_id','follow_user_id','is_follow'
    ];

    protected $table = "follow_unfollow";
}
