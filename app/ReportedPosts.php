<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportedPosts extends Model
{
        protected $fillable = [ 'user_id', 'post_id', 'status'];
		protected $table = 'reported_posts';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
	
		public function getReportedByUserDetailsAttribute($value) {
          return  @\App\User::where('id',$this->user_id)->first();
    }
	
	
			public function getPostDetailsAttribute($value) {
          return  @\App\Posts::where('id',$this->post_id)->first();
    }
	
				public function getPostImageAttribute($value) {
          return  @\App\PostsImages::where('post_id',$this->post_id)->first();
    }
	
	
	
 
	
	
  


 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
	
	
}