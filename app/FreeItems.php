<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeItems extends Model
{
        protected $fillable = [ 'user_id', 'store_id','post_id','consumed_status','text','image'];
		protected $table = 'free_items';
		
			    public function getCreatedAtAttribute($value) {
         return  \Carbon\Carbon::parse($value)->diffforhumans();
    }
	
	
}