<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otps extends Model
{
        protected $fillable = [
        'user_id','otp','pgp_encrypted_message'
    ];
	
	protected $table = 'otps';
    
 
    
}
