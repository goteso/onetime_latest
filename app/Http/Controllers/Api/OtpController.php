<?php

namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use App\Otp;
 
use Hash;
use Mail;
use File;
 
 


class OtpController extends Controller 
{
	
 
	

  //Get User Profile info
    public function insert_otp(Request $request)
    {
	
		 
        $validator = Validator::make($request->all(), [
                'email' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {	
					
					$otp = Otp::firstOrNew(array('email' => $request->email));
                    $otp->otp = rand(1000,9999);
					$otp->save();
					
					  $email = $request->email;       
											 
				$data=array('otp' => $otp->otp );
		Mail::send('emails.verify_email', $data, function($message) use ($email) {
             
                $message->to($email, 'LoyatyApp')->subject('LoyatyApp - Verify Email');
                $message->from('noreply@loyatyapp.com', 'LoyatyApp');
            });
			
			 
			
       
					 
                        $data['status_code']    =   1;
                        $data['status_text']    =   'Success';             
                        $data['message']        =   'OTP Email Sent Successfully';
						$data['data'] = $otp;
                        return $data;  
                   
              
        return $data;
    }
	}

 
 
 
 
 
    // Update user Latitude and Longitude
    public function update_social_links(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
            if(UserSocialLinks::where('user_id',$request->user_id)->count()==0)
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';
                $data['message']        =   'Social Links not found';
                return $data;
            }
            UserSocialLinks::where('user_id',$request->user_id)->update(['facebook'=>$request->facebook,'twitter'=>$request->twitter,'google'=>$request->google,'youtube'=>$request->youtube]);
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Social Links Updated successfully';

        }
        return $data;
    }


 
    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}