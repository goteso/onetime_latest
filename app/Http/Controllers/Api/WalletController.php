<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
 
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class WalletController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
 
   
   
   
   use bitcoin_price; // <-- ...and also this line.
 
   
 
 
    // 1 use to get bitcoins balance of user from transactions table
// 1 use to get bitcoins balance of user from transactions table
 	public function get_user_bitcoin_balance(Request $request)
	{
		      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					// call bitcoins api
					
				 $balance =  \App\Transactions::where('user_id',$request->user_id)->sum('amount');
                 $wallet_address =  \App\User::where('id',$request->user_id)->first(['wallet_address'])->wallet_address;
		 
		 
		          $d = array();
if($balance == '' or $balance == null or $balance == ' ')
{
$balance = 0;
}

		          $d1['balance'] = $balance;
                  $d1['wallet_address'] = $wallet_address;
		          $d[] = $d1;
			 
		            if($balance != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
				    }
					else
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
					}
				   
	            return $data;
				}
	}
	
	

	
	
	
// 2 use to do a one time payment
	public function onetime_payment(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
				// call bitcoins api
					
				 $balance =  \App\Transactions::where('user_id',$request->user_id)->sum('amount');
				 
				 
				 $balance_with_fee = $request->amount * (1+ env('ONETIME_PAYMENTS_COMMISSION') / 100 );
				 
			  
				 if($balance_with_fee > $balance )
				 {
					      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'you do not have enough bitcoins';
                          $data['data']      =   [];  
						  return $data;
				 }
				 
	 
				  
                    $wallet_pin = \App\User::where('id',$request->user_id)->first(['wallet_pin'])->wallet_pin;
					if($wallet_pin == $request->wallet_pin)
					{
						
						  $sender_username = @\App\User::where('id', $request->user_id)->first(['username'])->username;	
                          $receiver_username = @\App\User::where('id', $request->receiver_id)->first(['username'])->username;
                          $receiver_notification_token = @\App\UsersSessions::where('user_id', $request->receiver_id)->pluck('notification_token');						  

						  
						  $p1 = new \App\OneTimePayments();
                          $p1->user_id = $request->user_id;
					      $p1->receiver_id = $request->receiver_id;
						  $p1->amount = floatval($request->amount);
                          $p1->notes= $request->notes;
			              $p1->save();
						  
						  
						  $d1 = new \App\Transactions();
                          $d1->type =  'transfer';
					      $d1->user_id = $request->user_id;
					      $d1->linked_id = $p1->id; // 
						  $d1->amount = -floatval($request->amount);
						  $d1->commision = 'no';
						  
						  $commission_fees = $request->amount * ( env("ONETIME_PAYMENTS_COMMISSION") / 100 );
						  
						  $d1->description = 'You sent a OneTime Payment to '.$receiver_username.' '.floatval($request->amount).' Transaction fee '.$commission_fees.' Btc' ;  
						  $d1->deleted = '0';
					      $d1->save();
						  
						  
						  
				 
						  
						  //deduct 1 percent also
						  $d1 = new \App\Transactions();
                          $d1->type =  'transfer';
					      $d1->user_id = $request->user_id;
					      $d1->linked_id = $p1->id; // 
						  $d1->commision = 'yes';
						  $d1->amount = -$request->amount * ( env('ONETIME_PAYMENTS_COMMISSION') / 100 );
						  $d1->description = 'Transaction Fee taken for one time payment of '.$request->amount." to ".$receiver_username; //
						  $d1->deleted = '0';
					      $d1->save();
						  
						   
						  
						  $d1 = new \App\Transactions();
                          $d1->type =  'transfer';
					      $d1->user_id = $request->receiver_id;
					      $d1->linked_id =   $p1->id; //
						  $d1->amount = floatval($request->amount);
						  $d1->commision = 'no';
						  $d1->description = 'You received from '.$sender_username.' '.floatval($request->amount).' Btc for a OneTime Payment'; //message 1 to receiver
						  $d1->deleted = '0';
					      $d1->save();
						  
						     //send push notification to receiver
						  	 $df["sender_name"] = @$sender_username;
				             
					  
                            $details = array();
							$details['sender_id'] = @$request->user_id;
							$details['amount'] = @$request->amount;
							$details['notification_type'] = 'push';
							//if($notify_following == 1 or $notify_following == '1')
							//	{
									  $this->notification_to_single_identifier($receiver_notification_token , 'onetime_payment_received' , 'msg' , $details , '');
							//	}
						  
						  
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d1;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'You entered wrong wallet pin';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
// 3 use to withdraw bitcoins	
	public function withdraw_bitcoins(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					// comment 2 ---check balance and call bitcoins api , create withdraws table and insert to that , and add id as linked_id below

					// call bitcoins api
					
				 $balance =  \App\Transactions::where('user_id',$request->user_id)->sum('amount');
				 if(floatval($balance) < floatval($request->amount) or floatval($balance) == floatval($request->amount) )
				 {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'you dont have enough bitcoins';
                          $data['data']      =   [];  
						  return $data;
				 }
				    $token = @\App\UsersSessions::where('user_id', $request->user_id)->pluck('notification_token');		
				    $bw = new \App\BitcoinWithdraws();
                    $bw->user_id = $request->user_id;
					$bw->destination_wallet = $request->destination_wallet;
					$bw->amount = floatval($request->amount);
		            $bw->save();
						  
					$wallet_pin = \App\User::where('id',$request->user_id)->first(['wallet_pin'])->wallet_pin;
					if($wallet_pin == $request->wallet_pin)
					{
						  $sm1 = new \App\Transactions();
                          $sm1->type =  'withdraw';
					      $sm1->user_id = $request->user_id;
					      $sm1->linked_id = $bw->id;
						  $sm1->amount = -floatval($request->amount);
						  $sm1->commision = 'no';
						  $sm1->description =  'Withdrawal triggered - ('.$request->destination_wallet.') ('.$request->amount.')';
						  $sm1->deleted = '0';
					      $sm1->save();
						  
						  
					 
						    
						    
						  	     //send push notification  
						  	$df["sender_name"] = @$sender_username;
							$df["amount"] = @floatval($request->amount);
							$new_balance = @\App\Transactions::where('user_id',$request->user_id)->sum('amount');
							$df["new_balance"] =$new_balance;
				            					  
                            $details = array();
							$details['user_id'] = @$request->user_id;
						    $details['amount'] = @$request->amount;
							$details['notification_type'] = 'push';
						//	if($notify_following == 1 or $notify_following == '1')
							// {
									  $this->notification_to_single_identifier($token , 'Withdraw Bitcoins' , '(Deposit Confirmed) ('.$request->amount.') new balance ('.$new_balance.') btc' , $details , '');
							// }
							
							
							
							
							
				 
					  
							 
						 

						  
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $sm1;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'You entered invalid Pin';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
 
 
 
 
 
 
 // 4 add trade to trades_posts table
 	public function add_trade(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					      $user_balance = $this->get_user_bitcoin_balance($request);
						  $user_balance = floatval($user_balance['data'][0]['balance']);
						  
						  $wallet_address = \App\User::where('id',$request->user_id)->first(['wallet_address'])->wallet_address;
						  
						  if($request->type == 'sell')	
{	
						  if($user_balance < floatval($request->range_max))
						  {
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =  'You can’t create a trade until you have a balance in your account. You can deposit Bitcoin in below address to deposit bitcoin to your account.
'.$wallet_address;  
return $data;
							  
						  }
						  
						  
						  				  if(floatval($request->range_max) < floatval($request->range_min))
						  {
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =  'your entered max amount smaller than min amount';  
return $data;
							  
						  }
						  
						  
						  
}


					      $sm1 = new \App\TradesPosts();
                          $sm1->type =  $request->type;
					      $sm1->user_id = $request->user_id;
						  $sm1->currency = $request->currency;
						  $sm1->trade_rate_percentage = $request->trade_rate_percentage;
						  $sm1->trade_rate = $request->trade_rate;
						  $sm1->country = $request->country;
						  $sm1->bank_details = $request->bank_details;
						  
						  
						  $sm1->range_min = $request->range_min;
						  $sm1->range_max = $request->range_max;
					


						  
						  $sm1->terms_and_conditions = $request->terms_and_conditions;
					      $sm1->save();
						  
						  
						  
						  	 
				            					  
                           
							
							
							 
							
							
							
					 
					
					if($sm1 != '')
					{
						  
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $sm1;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Occurred';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	//5 use to edit trade details
	 	public function edit_trade(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    $r = \App\TradesPosts::where('id', $request->trades_posts_id)->update(['type'=>$request->type,'currency'=>$request->currency,'trade_rate_percentage'=>$request->trade_rate_percentage,'trade_rate'=>$request->trade_rate,'country'=>$request->country,'bank_details'=>$request->bank_details,'range_min'=>$request->range_min,'range_max'=>$request->range_max,'terms_and_conditions'=>$request->terms_and_conditions]); 
					  
					$d = \App\TradesPosts::where('id',$request->trades_posts_id)->get();
			 
					if($r == '1' or $r == 1)
					{
						       $data['status_code']    =   1;
                               $data['status_text']    =   'Success';             
                               $data['message']        =   'Operation Successful';
                               $data['data']      =   $d;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Occurred';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	// 6 use to delete trades from trades_posts table
	public function delete_trade(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    $r = \App\TradesPosts::where('id', $request->trades_posts_id)->delete(); 
					
				 
				    			 
					if($r == '1' or $r == 1)
					{
						       $data['status_code']    =   1;
                               $data['status_text']    =   'Success';             
                               $data['message']        =   'Operation Successful';
                               $data['data']      =  [];  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Occurred';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	
	
	
	// 7 used to get sell trades_posts
	public function get_sell_trades_posts(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					 
				    $trades_posts =  @\App\TradesPosts::where('type','buy')->where('user_id','<>',$request->user_id)->paginate(10);
				   
				   	foreach($trades_posts as $tp)
					{
						  $tp->user_details = @\App\User::where('id',$tp->user_id)->get(['id','first_name','last_name','country','email','verified','username']);
						  $tp->bitcoin_price = $this->currency_amount_bitcoin_price( $tp->currency )["data"][0]["bitcoin_price"];
					}
				 
				    if(sizeof($trades_posts) > 0)
					{
						       $data['status_code']    =   1;
                               $data['status_text']    =   'Success';             
                               $data['message']        =   'Fetched Successfully';
                               $data['data']      =   $trades_posts;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Trade Post Found';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	// 8 used to get trades_posts list with type=buy from trades_posts table
	public function get_buy_trades_posts(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					 
					$trades_posts =   @\App\TradesPosts::where('type','sell')->where('user_id','<>',$request->user_id)->paginate(10);
					
					foreach($trades_posts as $tp)
					{
						
					 
						  $tp->user_details = @\App\User::where('id',$tp->user_id)->get(['id','first_name','last_name','country','email','verified','username']);
						  $tp->bitcoin_price = $this->currency_amount_bitcoin_price( $tp->currency )["data"][0]["bitcoin_price"];
					}
				 
			       if(sizeof($trades_posts) > 0)
					{
						  
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $trades_posts;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Trade Post Found';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
 // 9 get trades_posts where user_id = my_user_id from trades_posts table with type=sell
 public function get_sell_my_trades_posts(Request $request)
 {
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					 
				   $trades_posts =   @\App\TradesPosts::where('type','sell')->where('user_id',$request->user_id)->paginate(10);
				   if(sizeof($trades_posts) > 0)
					{
						       $data['status_code']    =   1;
                               $data['status_text']    =   'Success';             
                               $data['message']        =   'Fetched Successfully';
                               $data['data']      =   $trades_posts;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Trade Post Found';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				} 
}

 //10 get trades_posts where user_id = my_user_id from trades_posts table with type=buy
 public function get_buy_my_trades_posts(Request $request)
 {
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					 
				   $trades_posts =   @\App\TradesPosts::where('type','buy')->where('user_id',$request->user_id)->paginate(10);
				   if(sizeof($trades_posts) > 0)
					{
						       $data['status_code']    =   1;
                               $data['status_text']    =   'Success';             
                               $data['message']        =   'Fetched Successfully';
                               $data['data']      =   $trades_posts;  
				    }
					else
					{
						       $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Trade Post Found';
                               $data['data']      =   [];  
					}
				   
	            return $data;
				} 
}
 
 
 
 
 
 
 
 
 
  // 11 use for buying bitcoins
 	public function buy_bitcoins(Request $request)
	{

 
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
				 
				 $balance =  @\App\Transactions::where('user_id',$request->seller_id)->sum('amount');
				 if(floatval($balance) < floatval($request->btc_amount))
				 {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Seller do not have this much bitcoins';
                          $data['data']      =   [];  
						  return $data;
				 }
				
				    $d = array();
					$seller_bank_details = @\App\TradesPosts::where('id',$request->trades_posts_id)->first(['bank_details'])->bank_details;
                    $a['seller_bank_details'] = @$seller_bank_details;
                    $d[] = $a;	
					
				          $buyer_name = @\App\User::where('id',$request->buyer_id)->first(['first_name'])->first_name;
				          $seller_name = @\App\User::where('id',$request->seller_id)->first(['first_name'])->first_name;
				
			              $sm1 = new \App\Transactions();
                          $sm1->type =  'withdraw';
					      $sm1->user_id = $request->seller_id;
					      $sm1->linked_id = $request->trades_posts_id;
						  $sm1->amount = -floatval($request->btc_amount);
						  $sm1->commision = 'no';
						  $sm1->description = $request->btc_amount." are brought by ".$buyer_name." from ".$seller_name;
						  $sm1->deleted = '0';
					      $sm1->save();
						  
						  
						  // comment 4 --- below will be transferred by admin from escrow 
						  /**
						  $sm2 = new \App\Transactions();
                          $sm2->type =  'withdraw';
					      $sm2->user_id = $request->buyer_id;
					      $sm2->linked_id = $request->trades_posts_id;
						  $sm2->amount = intval($request->btc_amount);
						  $sm2->description = $request->btc_amount." are brought by ".$buyer_name." from ".$seller_name;
						  $sm2->deleted = '0';
					      $sm2->save();
						  **/
						  
						  $sm1 = new \App\Trades();
                          $sm1->trades_posts_id =  $request->trades_posts_id;
					      $sm1->seller_id = $request->seller_id;
						  $sm1->buyer_id = $request->buyer_id;
						  $sm1->other_amount = $request->other_amount;
						  $sm1->btc_amount = $request->btc_amount;
						  $sm1->status = 'new';
						  $sm1->bank_details = $seller_bank_details;
			              $sm1->save();
						  
						  
						  
						  	//sending notification 
						  
						     //notification to seller
							 
							 
							// New trade from (username) for (title or amount in bitcoins) (buy/sell if a bitcoin trade)
						  
					  
					  
					    	     //send push notification  
							$buyer_username = 	 @\App\User::where('id',$sm1->buyer_id)->first(['username'])->username;	
						    $seller_notification_token = @\App\UsersSessions::where('user_id',$sm1->seller_id)->pluck('notification_token');					  
                            $details = array();
							$details['trades_posts_id'] = @$request->trades_posts_id; 
						    $details['buyer_id'] =  @$sm1->id;
							$details['id'] =  @$sm1->id;
						 
						//	if($notify_following == 1 or $notify_following == '1')
							// {
									  $this->notification_to_single_identifier($seller_notification_token , 'New Trade' , 'New trade from ('.$buyer_username.') for ('.$request->btc_amount.')' , $details , 'trade');
							// }
							
							
							
						  //sending notification ends
						  
						  
						  
						  
					if($sm1 != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	
	
	
	 
  // 11 use for buying bitcoins
 	public function sell_bitcoins(Request $request)
	{

	$trades_posts_id = $request->trades_posts_id;
 
 
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
				 
				 
				 
				 $balance =  @\App\Transactions::where('user_id',$request->seller_id)->sum('amount');
				 
				 
				 if(floatval($balance) < floatval($request->btc_amount))
				 {
					 
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Seller do not have this much bitcoins';
                          $data['data']      =   [];  
						  return $data;
				 }
				
				          $d = array();
					 
                          $a['seller_bank_details'] = $request->seller_bank_details;
                          $d[] = $a;	
					
				          $buyer_name = @\App\User::where('id',$request->buyer_id)->first(['first_name'])->first_name;
				          $seller_name = @\App\User::where('id',$request->seller_id)->first(['first_name'])->first_name;
				
			              $sm1 = new \App\Transactions();
                          $sm1->type =  'withdraw';
					      $sm1->user_id = $request->seller_id;
					      $sm1->linked_id = $request->trades_posts_id;
						  $sm1->amount = -floatval($request->btc_amount);
						  $sm1->commision = 'no';
						  $sm1->description = $request->btc_amount." are brought by ".$buyer_name." from ".$seller_name;
						  $sm1->deleted = '0';
					      $sm1->save();
						
						  //comment 5 ---- below will be transferred from escrow same as above api
						 /**
						  $sm2 = new \App\Transactions();
                          $sm2->type =  'withdraw';
					      $sm2->user_id = $request->buyer_id;
					      $sm2->linked_id = $request->trades_posts_id;
						  $sm2->amount = intval($request->btc_amount);
						  $sm2->description = $request->btc_amount." are brought by ".$buyer_name." from ".$seller_name;
						  $sm2->deleted = '0';
					      $sm2->save();
						  **/
						  
						  $sm1 = new \App\Trades();
                          $sm1->trades_posts_id =  $request->trades_posts_id;
					      $sm1->seller_id = $request->seller_id;
						  $sm1->buyer_id = $request->buyer_id;
						  $sm1->other_amount = $request->other_amount;
						  $sm1->btc_amount = $request->btc_amount;
		                  $sm1->bank_details = $request->seller_bank_details;
						  $sm1->status = 'new';
			              $sm1->save();
						  
						  //sending notification 
						  
						     //notification to buyer
							 
							 
							 
							 	  
						  	//sending notifications
							@$seller_username = @\App\User::where('id',$request->seller_id)->first(['username'])->username;
							@$buyer_notification_token = @\App\UsersSessions::where('user_id',$request->buyer_id)->pluck('notification_token');
					 
		                    $details = array();
							$details['sender_name'] = @$sender_name;
							$details['sender_id'] = @$request->user1;
							$details['notification_type'] = 'message';
							$details['id'] = $sm1->id;
							
							 
							$this->notification_to_single_identifier($buyer_notification_token , 'New Trade' , 'New trade from ('.$seller_username.') for ('.$request->btc_amount.')' , $details , 'trade');
							 
							 
						  
						  //sending notification ends
						  
						 
						  
					if($sm1 != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	
	
	
	
	
	
	
	
	  // 12 get my transaction history
 	public function my_transaction_history(Request $request)
	{
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
				 
				    $history =  @\App\Transactions::where('user_id',$request->user_id)->where('deleted','0')->paginate(10);
					
					if(sizeof($history)>0)
		           
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Fetched Successful';
                          $data['data']      =   $history;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Transaction history found';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	// 13 delete transaction data
 	public function delete_transaction_history(Request $request)
	{
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
				 
				     
				 @\App\Transactions::where('id',$request->transaction_history_id)->update(['deleted' => 1]);	
				 
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   [];  
				 
				   
	            return $data;
				}
	}
	
	
	
	
	
	
	public function trade_list(Request $request)
	{
		 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					 $trades = @\App\Trades::where('seller_id',$request->user_id)->where('status',$request->status)->orWhere('buyer_id',$request->user_id)->where('status', $request->status)->paginate(10);
					 
					 
			       //  $trades = $trades->where('seller_id',$request->user_id)->orWhere('buyer_id',$request->user_id)->paginate(10);
				 
					 
				 
					 foreach($trades as $t)
					 {
						 
						 $creator_id = @\App\TradesPosts::where('id',$t->trades_posts_id)->first(['user_id'])->user_id;
						 $currency = @\App\TradesPosts::where('id',$t->trades_posts_id)->first(['currency'])->currency;
						 
						 if($creator_id == $request->user_id)
						  {
							    $t->my_trade = '1';
						  }
						 else
							 {
								$t->my_trade = '0';
							 }
							 
							 if($request->user_id == $t->buyer_id )
							 {
								 $seller_name = @\App\User::where('id',$t->seller_id)->first(['first_name'])->first_name;
								 $seller_country = @\App\User::where('id',$t->seller_id)->first(['country'])->country;
								 $t->string1 = 'Seller : <b>'.$seller_name.'</b>';
								 $t->string2 = 'Location : <b>'.$seller_country.'</b>';
							 }
							 
							  if($request->user_id == $t->seller_id )
							 {
								 $buyer_name = @\App\User::where('id',$t->buyer_id)->first(['first_name'])->first_name;
								 $buyer_country = @\App\User::where('id',$t->buyer_id)->first(['country'])->country;
								 $t->string1 = 'buyer : <b>'.$buyer_name.'</b>';
								 $t->string2 = 'Location : <b>'.$buyer_country.'</b>';
								 
							 }
							 
							 
							  $t->currency = $currency;
							// $t->seller_details = @\App\User::where('id',$t->seller_id)->get();
							 //$t->buyer_details = @\App\User::where('id',$t->buyer_id)->get();
					 }
				   if(count($trades))
				    {
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Fetched Successfully';
                          $data['data']      =   $trades;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Trade Found';
                          $data['data']      =   [];  
					}
				   
	            return $data;
					
				}
   }
	
	
	
	
	public function trade_post_details(Request $request)
	{
		$trade_posts_details = @\App\TradesPosts::where('id',$request->trade_posts_id)->get();
		 
		$trade_posts_details[0]['current_rate'] = '1000';
		
		if($trade_posts_details !='')
		     {
					      $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
		                   $data['data']      =   $trade_posts_details;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
					return $data;
		
	}
	
	
	// 13 use for trade details
	public function trade_details(Request $request)
	{
		
		
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
				
				    $trade = @\App\Trades::where('id',$request->trade_id)->get();
					$trades_count = @\App\Trades::where('id',$request->trade_id)->count();
					
					$trades_posts_id = @$trade[0]->trades_posts_id;
					
					 
			
				 
					
					
					  $trade_seller_id = @\App\Trades::where('id',@$request->trade_id)->first(['seller_id'])->seller_id;
					  
					  $seller_feedback = @\App\Trades::where('id',@$request->trade_id)->first(['seller_feedback'])->seller_feedback;
					  $buyer_feedback = @\App\Trades::where('id',@$request->trade_id)->first(['buyer_feedback'])->buyer_feedback;
					  
					  
					  $trades_posts_id = @\App\Trades::where('id',@$request->trade_id)->first(['trades_posts_id'])->trades_posts_id;
					  $currency = @\App\TradesPosts::where('id',@$trades_posts_id)->first(['currency'])->currency;
					  $TradesPostsUserId = @\App\TradesPosts::where('id',$trades_posts_id)->first(['user_id'])->user_id;
					  $trade[0]['currency'] = $currency;
					  
					  $type = @\App\TradesPosts::where('id',$trades_posts_id)->first(['type'])->type;
					  
					  $trade[0]['type'] = $type;
					   $status = @\App\Trades::where('id',$request->trade_id)->first(['status'])->status;
				      $trade_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
					
					$trade_details[0]['trade_details'] = $trade;
					$trade_details[0]['trade_posts_details'] = \App\TradesPosts::where("id",$trades_posts_id)->get();
				 
					
			        if($trades_count > 0)
					{
				      $trade_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
				      $trade_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
 
					  $trade_details[0]['seller_details'] = \App\User::where('id',$trade_seller_id)->get();
					  $trade_details[0]['buyer_details'] = \App\User::where('id',$trade_buyer_id)->get ();
					}
					else
					{
					  $trade_seller_id = '';
				      $trade_buyer_id = '';
					  $trade_details[0]['seller_details'] = [];
					  $trade_details[0]['buyer_details'] = [];
					}
					
					$bitcoin_details = @\App::call('\App\Http\Controllers\Api\BitcoinController@currency_bitcoin_price');
				    $bitcoin_price = $bitcoin_details["data"][0]['bitcoin_price'];
				   
				   
					$trade_details[0]['current_rate'] = $bitcoin_price;
		 
			
					  
					  
					  $created_at = @\App\Trades::where('id',$request->trade_id)->first(['created_at'])->created_at;
					  
					  $buttons_array =array();
					 if($trade_seller_id == $request->user_id)
					 {						 
					  if($status =='new')
					  {
					  $button1["title"] = 'Mark Completed';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = 'completed';

					  $button2["title"] = 'Dispute';
					   $created_at = @\App\Trades::where('id',$request->trade_id)->first(['created_at'])->created_at;
					   $date1 = new Carbon();
                      //$diff = $date1->diffForHumans($created_at,'h');
				 
					 
					  $diff =  $created_at;
					  
					  if($diff < 90)
					  {
						  $button2['Enabled'] = '0';
					  }
					  else
					  {
						  $button2['Enabled'] = '1';
					  }
					  
					  		
					  $button2['ActionStatus'] = 'disputed';
					  }
					  
					  if($status =='transferred')
					  {
					  $button1["title"] = 'Mark Completed';
					  $button1['Enabled'] = '1';
					  $button1['ActionStatus'] = 'completed';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '1';
					  $button2['ActionStatus'] = 'disputed';
					  }
					  
					  if($status =='completed')
					  {
						  
						    if($seller_feedback == 'null' or $seller_feedback == null or $seller_feedback == ' ')
						  { $enabled = '1'; }
						  else { $enabled = '0'; }
					  $button1["title"] = 'Give FeedBack';
					  $button1['Enabled'] = $enabled;
					  $button1['ActionStatus'] = 'feedback';
					  
					  
					 
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = 'disputed';
					  }
					  
					  
				 
					  
					  if($status =='cancelled')
					  {
					  $button1["title"] = 'Cancelled';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
			 
					  
					  
					  		  if($status =='disputed')
					  {
					  $button1["title"] = 'Disputed';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  
					  
					 }
					 
					 
					if($trade_buyer_id == $request->user_id)
					 {						 
					  if($status =='new')
					  {
					  $button1["title"] = 'Mark Transferred';
					  $button1['Enabled'] = '1';
					  $button1['ActionStatus'] = 'transferred';
					  
					  $button2["title"] = 'Cancel Trade';
					  $button2['Enabled'] = '1';
					  $button2['ActionStatus'] = 'cancelled';
					  }
					  
					  if($status =='transferred')
					  {
					  $button1["title"] = 'Transferred';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  if($status =='completed')
					  {
				 
					  
					  
					      if($buyer_feedback == 'null' or $buyer_feedback == null or $buyer_feedback == ' ')
						  { $enabled = '1'; }
						  else { $enabled = '0'; }
					  $button1["title"] = 'Give FeedBack';
					  $button1['Enabled'] = $enabled;
					  $button1['ActionStatus'] = 'feedback';
					  
					  
					  
					  
					  $button2["title"] = 'Cancel Trade';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  
					  
		 
					  
		 
					  
					  
					  
					  
					  if($status =='cancelled')
					  {
					  $button1["title"] = 'Cancelled';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel Trade';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  // time check
					  if($status =='disputed')
					  {
					  $button1["title"] = 'Disputed';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel Trade';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					 }
					 
				 
					 
					  $present = \Carbon\Carbon::now();
					 $diff =  strtotime($present) - strtotime($created_at);
					 if(intval($diff) < 60)
					 {
					    $button2['Enabled'] = '0';
					 }
					 
					 
					 
					  
					  $buttons_array[] = @$button1;
					  $buttons_array[] = @$button2;
				 
					   $trade_details[0]['buttons'] = $buttons_array;
					   
					   
			
				  
					 if($trade_details != '')
					 {
						
							 $unread_message_count = \App\Messages::where('user1',$request->user_id)->where('user2',$TradesPostsUserId)->where('read_status','0')->where('type','trade')->get()->count();
				   
				   if(!isset($unread_message_count) or $unread_message_count == null or $unread_message_count == '')
				   {
					   $unread_message_count = 0;
				   }
				 
				     if($unread_message_count > 99)
				     {
				 	   $unread_message_count = 99;
			         }
				   
				   
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
						  $data['unread_message_count'] =$unread_message_count;
                          $data['data']      =   $trade_details;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	public function mark_completed(Request $request)
	{
	  				 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					
					$sm1 = new \App\Messages();
                    $sm1->user1 =  $request->user1;
					$sm1->user2 = $request->user2;
					$sm1->message = $request->message;
                    $sm1->save();
					 
			 
			        if($sm1 != '')
					{
			 
					   $messages = @\App\Messages::where('user1',$request->user1)->where('user2',$request->user2)->orWhere('user1',$request->user2)->where('user2',$request->user1)->orderBy('id','desc')->paginate(10);
						  
						  foreach($messages as $m)
						  {
							  
							 $m->user1_details = @\App\User::where('id',$m->user1)->get(['id','first_name','last_name','profile_image','verified','username']);
							 $m->user2_details = @\App\User::where('id',$m->user2)->get(['id','first_name','last_name','profile_image','verified','username']);
						  }
			 
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Fetched Successful';
                          $data['data']      =   $messages;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
		
	}
	
	
	
	// 14 send_messages
	public function send_messages(Request $request)
	{
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
				 
					
					$sm1 = new \App\Messages();
                    $sm1->user1 =  $request->user1;
					$sm1->user2 = $request->user2;
					$sm1->message = $request->message;
					$sm1->type = $request->type;
					$sm1->trades_posts_id = $request->trades_posts_id;
					$sm1->read_status = 0;
                    $sm1->save();
	 
			        if($sm1 != '')
					{
			 
					      $messages = @\App\Messages::where("type",$request->type)->where('user1',$request->user1)->where('user2',$request->user2)->orWhere('user1',$request->user2)->where('user2',$request->user1)->orderBy('id','asc')->paginate(10);
						  
						  $trades_post_currency = @\App\TradesPosts::where('id',$request->trade_post_id)->first(['currency'])->currency;
						  $trades_rate = @\App\TradesPosts::where('id',$request->trade_post_id)->first(['trade_rate'])->trade_rate;
						  
				          $user1 = @$request->user1;	
                          $user2 = @$request->user2;	
                          $trades_posts_id	= @$request->trades_posts_id;			 
				 
                          $trade_details = @\App\Trades::where(function($q) use ($user1 , $user2 , $trades_posts_id )
						  { $q->where('buyer_id',$user1)->where('seller_id',$user2)->where('trades_posts_id',$trades_posts_id);})->orWhere(function($q) use ($user1 , $user2 , $trades_posts_id ){ $q->where('buyer_id',$user2)->where('seller_id',$user1)->where('trades_posts_id',$trades_posts_id);})->get();

						 
					 
 
                          $range_min = @$trade_details[0]['trade_post_details']["range_min"];
						  $range_max = @$trade_details[0]['trade_post_details']["range_max"];
						  $currency = @$trade_details[0]['trade_post_details']["currency"];
  
 
  
						  
						  	//sending notifications
							@$sender_name = @\App\User::where('id',$request->user1)->first(['first_name'])->first_name." ".@\App\User::where('id',$request->user1)->first(['last_name'])->last_name;
							@$sender_username = @\App\User::where('id',$request->user1)->first(['username'])->username;
							@$token = @\App\UsersSessions::where('user_id',$request->user2)->pluck('notification_token');
					 
		                    $details = array();
							$details['sender_name'] = @$sender_name;
							$details['username'] = @$sender_username;
							$details['sender_id'] = @$request->user1;
							$details['id'] = @$trades_posts_id;
							$details['notification_type'] = 'message';
							
							 
							 
					 
							 if($request->type == 'chat')
							 {
								  $this->notification_to_single_identifier($token , 'Trade Message' , $sender_name.' sent you a message' , $details , 'chat');
							 }
							  if($request->type == 'trade')
							 {
								  $this->notification_to_single_identifier($token , 'Trade Message' , $sender_name.' sent you a message for '.$currency.$range_min." @ ".$currency.$range_max."/BTC" , $details , 'trade');
							 }
					       
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
					 if($request->type == 'trade')
					  {
				 	   
			  	   
				    
						  $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'trade')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'trade')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->take(1)->get();
 
 
						    
					  }
					  
					  	 else if($request->type == 'post')
					  {
						 
						   
						   $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'post')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'post')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->take(1)->get();

 
		 	    
					  }
					  
					  
					  else
					  {
						 						  					  $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'chat')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'chat')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->take(1)->get();
				   }
					  
					 
					  				  foreach($messages as $m)
						  {
							  
							  
							   
							 $m->user1_details = @\App\User::where('id',$m->user1)->get(['id','first_name','last_name','profile_image','verified','username']);
							 $m->user2_details = @\App\User::where('id',$m->user2)->get(['id','first_name','last_name','profile_image','verified','username']);
						  }
					  
					  
					  
					  
					  
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Fetched Successful';
                          $data['data']      =   $messages;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	// 15 get_messages
	public function get_messages(Request $request)
	{
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					$trades_posts_id = $request->trades_posts_id;
					$user2 = $request->user2;
					$user1 = $request->user1;
				 
					
					  if($request->type == 'trade')
					  {
						  
				
						   
						  $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'trade')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'trade')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->orderBy('id','DESC')->paginate(12);


 
						    
					  }
					  
					  	 else if($request->type == 'post')
					  {
						  
						  	
						 
						
						   
						  $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'post')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'post')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->paginate(12);

 
						    
					  }
					  
					  
					  else
					  {
						  
						  					  $messages = @\App\Messages::where(function ($query) use ($trades_posts_id,$user1,$user2) {
    $query->where('trades_posts_id',$trades_posts_id)->where("type",'chat')->where('user1',$user1)->where('user2',$user2);
})
->orWhere(function ($query) use ($trades_posts_id,$user1,$user2) {
 $query->where('trades_posts_id',$trades_posts_id)->where("type",'chat')->where('user1',$user2)->where('user2',$user1);
})->orderBy('id','DESC')->paginate(12);
					  }
						 
		 
						  
						  
					 
						  foreach($messages as $m)
						  {
							  
							 $m->user1_details = @\App\User::where('id',$m->user1)->get(['id','first_name','last_name','verified','username']);
							 $m->user2_details = @\App\User::where('id',$m->user2)->get(['id','first_name','last_name','verified','username']);
						  }
			       $flight = \App\Messages::where('user2',$request->user1)->where('type',$request->type)->update(['read_status' => 1]);
			        if(sizeof($messages) > 0)
					{
			    
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Fetched Successful';
                          $data['data']      =   $messages;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Messages Found';
                          $data['data']      =   [];  
					}
				   
	            return $data;
				}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
 
public function phparraysort($Array, $SortBy=array(), $Sort = SORT_REGULAR) {
    if (is_array($Array) && count($Array) > 0 && !empty($SortBy)) {
            $Map = array();
            foreach ($Array as $Key => $Val) {
                $Sort_key = '';
foreach ($SortBy as $Key_key) {
$Sort_key .= $Val[$Key_key];
}                
                $Map[$Key] = $Sort_key;
            }
            asort($Map, $Sort);
            $Sorted = array();
            foreach ($Map as $Key => $Val) {
                $Sorted[] = $Array[$Key];
            }
            return array_reverse($Sorted);
    }
    return $Array;
}


 
 
	
	
 
    // 15 get_messages
	public function get_chats_lists(Request $request)
	{
			 $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
						 
						 $chat_user1_ids = \App\Messages::where('type','chat')->where("user1",$request->user_id)->orderBy('id','DESC')->distinct()->pluck('user2')->toArray();
						 $chat_user2_ids = \App\Messages::where('type','chat')->where("user2",$request->user_id)->orderBy('id','DESC')->distinct()->pluck('user1')->toArray();
						  
						  
						  
						  $type= $request->type;
						  $user_id = $request->user_id;
						  
						 $ma =array();
						 $users_ids = array_values(array_unique(array_merge( $chat_user1_ids,$chat_user2_ids)));
						 
						 
						 for($x=0;$x<sizeof($users_ids);$x++)
						 {
				 
							 
							 $another_user_id = $users_ids[$x];
							
							 
							 		     $m = @\App\Messages::where(function($q) use ($another_user_id , $type , $user_id )
						  { $q->where('user1',$user_id)->where('user2',$another_user_id)->where('type',$type);})
						  
						  ->orWhere(function($q) use ($another_user_id , $type , $user_id )
						  { $q->where('user1',$another_user_id)->where('user2',$user_id)->where('type',$type);})->orderBy('id','DESC')->take(1)->first(['id','message','user1','user2','created_at']);

						  $ma[]= $m;
						  
						  
						 }
				
				
			 
           foreach($ma as $u)
		   {
			   if(@$u->user1 == @$request->user_id)
			   {
				   $u->sender_name = @\App\User::where('id',$u->user2)->first(['first_name'])->first_name." ".@\App\User::where('id',$u->user2)->first(['last_name'])->last_name;
				   $u->sender_profile_image = @\App\User::where('id',@$u->user2)->first(['profile_image'])->profile_image;
                   $u->username = @\App\User::where('id',@$u->user2)->first(['username'])->username;
                   $u->verified = @\App\User::where('id',@$u->user2)->first(['verified'])->verified;
				   $u->sender_id = @$u->user2;
				   $u->created_at_formatted = @$u->created_at;
			   } 
			   
			     if(@$u->user2 == @$request->user_id)
			   {
				   $u->sender_name = @\App\User::where('id',$u->user1)->first(['first_name'])->first_name." ".@\App\User::where('id',$u->user1)->first(['last_name'])->last_name;
				   $u->sender_profile_image = @\App\User::where('id',$u->user1)->first(['profile_image'])->profile_image;
				   $u->sender_id = @$u->user1;
                   $u->username = @\App\User::where('id',@$u->user1)->first(['username'])->username;
                   $u->verified = @\App\User::where('id',@$u->user1)->first(['verified'])->verified;
				   $u->created_at_formatted = @$u->created_at;
			   }
		 
		   }
		   
		   $d = $this->paginateWithoutKey($ma, $perPage = 5, $page = null, $options = []);
				
		 	 	
				$ar =  $d['data'];
				
				$ma = array();
$final_array = array();
$db_id = array();


 
for($f=0;$f<sizeof($ar);$f++)
{
	if($ar[$f]['user1'] == $request->user_id)
	{
		 if (in_array($ar[$f]['user2'], $final_array)) {} else { $final_array[] = $ar[$f]['user2'];  $db_id[] = $ar[$f]['id'];}
		 
		 
		
	}
	
		if($ar[$f]['user2'] == $request->user_id)
	{
		 if (in_array($ar[$f]['user1'], $final_array)) {} else { $final_array[] = $ar[$f]['user1'];  $db_id[] = $ar[$f]['id']; }
	}
}	


//return json_encode($final_array)."---".json_encode($db_id);


rsort($db_id);
 
 
 


$final_user_id = array();

for($t=0;$t<sizeof($db_id);$t++)
{
	$m1= @\App\Messages::where('id',$db_id[$t])->where('user1',$request->user_id)->get();
	
	if(sizeof($m1) >  0) 
	{
		
		
		
		$final_user_id[] = $m1[0]['user2'];
	}
	
	
	
	
	$m2= @\App\Messages::where('id',$db_id[$t])->where('user2',$request->user_id)->get();
	
	if(sizeof($m2) >  0) 
	{ 
		$final_user_id[] = $m2[0]['user1'];
	}
}
 
 
 
 
 
 
 
 
 $users_ids = $final_user_id;
 
 
 
 					 for($x=0;$x<sizeof($users_ids);$x++)
						 {
				 
							 
							 $another_user_id = $users_ids[$x];
							
							 
							 		     $m = @\App\Messages::where(function($q) use ($another_user_id , $type , $user_id )
						  { $q->where('user1',$user_id)->where('user2',$another_user_id)->where('type',$type);})
						  
						  ->orWhere(function($q) use ($another_user_id , $type , $user_id )
						  { $q->where('user1',$another_user_id)->where('user2',$user_id)->where('type',$type);})->orderBy('id','DESC')->take(1)->first(['id','message','user1','user2','created_at']);

						  $ma[]= $m;
						  
						  
						 }
				
				
			 
           foreach($ma as $u)
		   {
			   if($u->user1 == $request->user_id)
			   {
				   $u->sender_name = @\App\User::where('id',$u->user2)->first(['first_name'])->first_name." ".@\App\User::where('id',$u->user2)->first(['last_name'])->last_name;
				   $u->sender_profile_image = @\App\User::where('id',@$u->user2)->first(['profile_image'])->profile_image;
                   $u->username = @\App\User::where('id',@$u->user2)->first(['username'])->username;
                   $u->verified = @\App\User::where('id',@$u->user2)->first(['verified'])->verified;
				   $u->sender_id = @$u->user2;
				   $u->created_at_formatted = @$u->created_at;
			   } 
			   
			     if($u->user2 == $request->user_id)
			   {
				   $u->sender_name = @\App\User::where('id',$u->user1)->first(['first_name'])->first_name." ".@\App\User::where('id',$u->user1)->first(['last_name'])->last_name;
				   $u->sender_profile_image = @\App\User::where('id',$u->user1)->first(['profile_image'])->profile_image;
				   $u->sender_id = @$u->user1;
                   $u->username = @\App\User::where('id',@$u->user1)->first(['username'])->username;
                   $u->verified = @\App\User::where('id',@$u->user1)->first(['verified'])->verified;
				   $u->created_at_formatted = @$u->created_at;
			   }
		 
		   }
		   
		   $d = $this->paginateWithoutKey($ma, $perPage = 5, $page = null, $options = []);
		   
		    
				
				 
			$data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =  'Chat List Fetched Successfully';
            $data['data'] = $this->paginateWithoutKey($ma, $perPage = 5, $page = null, $options = []);
            return $data;		
					 $paginate = 2;
    $page = Input::get('page');
    

    $offSet = ($page * $paginate) - $paginate;  
    $itemsForCurrentPage = array_slice($ma, $offSet, $paginate, true);  
    $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($ma), $paginate, $page);
	 
	 
    $result = $result->toArray();
     }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
				 
				 
				 	$model = new \App\UsersReviews();
                    $model->recipient_id = $order_buyer_id;
					$model->user_id = $user_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
					
					
					
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
				 
				 	$model = new \App\UsersReviews();
                    $model->recipient_id = $order_seller_id;
					$model->user_id = $user_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
		 		
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
     public function create_notification($notification_type,$notification_message,$user_id,$notification_by_user_id, $details , $df='')
    {               
               $for_user_notification = User::select('id','notification_token')->where('id',$user_id)->first();    
                    App::setLocale($for_user_notification['language']);
					
				 
			        $sub_title = @\App\NotificationTriggers::where('notification_type','app')->where('trigger_type',$notification_type)->first(['sub_title'])->sub_title;
			        $sub_title = $this->get_notification_string(@$sub_title , @$df);
			  
                    $input = array();                
                    $input['notification_type'] = @$notification_type;        
                    $input['notification_message'] = @$sub_title;
                    $input['user_id'] = @$user_id;
					$input['offer_id'] =@$details['post_id'];
			        $input['is_read'] = 0;
                   
                    $input['status'] = 1;
                    $notification = \App\Notification::create($input);
                    $badge_count = $for_user_notification['badge_count']+1;
                    @\App\User::where('id',$for_user_notification['id'])->update(['badge_count'=>$badge_count]);
    }
	
	
	
	

	
	
	
	
	
 	
	public function update_trade_order_status(Request $request)
	{
	     $trade_id = $request->trade_id;
		 $payment_method = $request->payment_method;
		 $status = $request->status;
		 $user_id = $request->user_id;
		 $wallet_pin = $request->wallet_pin;
		 
		 $seller_id = @\App\Trades::where('id', $request->trade_id)->first(['seller_id'])->seller_id;
		 $seller_notification_token = @\App\UsersSessions::where('user_id',$seller_id)->pluck('notification_token');
		 
	 
		 
		 $buyer_id = @\App\Trades::where('id', $request->trade_id)->first(['buyer_id'])->buyer_id;
		 $btc_amount = @\App\Trades::where('id', $request->trade_id)->first(['btc_amount'])->btc_amount;
         $buyer_notification_token = @\App\UsersSessions::where('user_id',$buyer_id)->pluck('notification_token');
		 
		 
		 $seller_details = @\App\User::where('id',$seller_id)->get();
		 $buyer_details = @\App\User::where('id',$buyer_id)->get();
		 
	     if($status == 'completed')
		 {
			 
			 $user_wallet_pin = @\App\User::where('id',$request->user_id)->first(['wallet_pin'])->wallet_pin;
			 
			 if($wallet_pin =! $user_wallet_pin)
			 {
			    	 $data1['status_code']    =   0;
				     $data1['status_text']    =   'Failed';
                     $data1['message']        =   'Invalid Wallet Pin';
				     return $data1;
			 }
			  
						  $sm2 = new \App\Transactions();
                          $sm2->type =  'purchase';
					      $sm2->user_id = $buyer_id;
					      $sm2->linked_id = $trade_id;
						  $sm2->amount = floatval($btc_amount);
						  $sm2->commision = 'no';
						  $sm2->description = floatval($btc_amount).' btc added to your account for trade';
						  $sm2->deleted = '0';
					      $sm2->save();
						  
						  
						  
						  //sending push notifications code starts
						  
						  //send push notification 1 to seller 
						  
						  
							$seller_username = $seller_details[0]['username'];
							$buyer_username = $buyer_details[0]['username'];
						  
						  	$df1["sender_name"] = @$sender_username;
							$df1["amount"] = floatval($btc_amount);
							$new_balance = @\App\Transactions::where('user_id',$request->user_id)->sum('amount');
							
							
							$df1["new_balance"] =$new_balance;
				            					  
                            $details1 = array();
							$details1['seller_id'] = @$seller_id;
							$details1['id'] = $request->trade_id;
							$details1['buyer_id'] = @$buyer_id;
						    $details1['amount'] = floatval($btc_amount);
						    //	if($notify_following == 1 or $notify_following == '1')
							// {
									  $this->notification_to_single_identifier($seller_notification_token , 'Trade Completed' , 'Trade completed(Sold) To ('.$buyer_username.') ('.floatval($btc_amount).')' , $details1 , 'trade');
							// }
							
							
							
							
							
							//send push notification 2 to buyer 
						  	$df2["sender_name"] = @$sender_username;
							$df2["amount"] = floatval($btc_amount);
							$new_balance = @\App\Transactions::where('user_id',$request->user_id)->sum('amount');
							$df2["new_balance"] =$new_balance;
				            					  
                            $details2 = array();
							$details2['seller_id'] = @$seller_id;
							$details2['id'] = $request->trade_id;
							$details2['buyer_id'] = @$buyer_id;
						    $details2['amount'] = floatval($btc_amount);
							$details2['notification_type'] = 'push';
							//	if($notify_following == 1 or $notify_following == '1')
							// {
									  $this->notification_to_single_identifier($buyer_notification_token , 'Trade Completed' , 'Trade completed(Purchase) From ('.$seller_username.') ('.floatval($btc_amount).')' , $details2 , 'trade');
							// }
							
							
							
							
							
							
							
			$current_date_time = @\Carbon\Carbon::now().""; 
		 
            @\App\Trades::where('id', $request->trade_id)->update(['status' => $status]);
			  @\App\Trades::where('id', $request->trade_id)->update(['completed_time' => $current_date_time]);
			
		 
			 
			$data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Trade Status Updated Successfully';
			return $data;

			
		 }
		 else if($status == 'cancelled')
		 {
			@\App\Transactions::where('linked_id', $request->trade_id)->where('type','purchase')->delete();
				
		    @\App\Trades::where('id', $request->trade_id)->update(['status' => $status]);
			 
			$data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Trade Status Updated Successfully';
			return $data;
			 
		 }
			else if( $status == 'feedback' ) { 
			   @\App\Trades::where('id', $request->trade_id)->update(['status' => 'completed']);
			   
			   	$data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Trade Status Updated Successfully';
			return $data;
			 }  
		 else
		 {
		    @\App\Trades::where('id', $request->trade_id)->update(['status' => $status]);
			
			if($status == 'transferred')
			{
					 $current_date_time = @\Carbon\Carbon::now().""; 
					 @\App\Trades::where('id', $request->trade_id)->update(['transferred_time' => $current_date_time]);
			}
			 
			$data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Trade Status Updated Successfully';
			return $data;
		 }
   }
	
	
	/**
	Trade complete (Purchase/Sold/) From or Too (username) (amount) and the transaction fee if sold...

Push notification - Trade Successfully complete, Your new balance is (balance) Btc

Push notification - New trade from (username) for (title or amount in bitcoins) (buy/sell if a bitcoin trade)  -- [it will be sent to creator , pending ]

**/
	
	
	
	
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}