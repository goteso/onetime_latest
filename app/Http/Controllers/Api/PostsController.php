<?php

namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use App\FreeItems;
use App\ScanActivity;
use App\CoinbaseLog;
 
use App\Traits\one_signal; // <-- you'll need this line...
 
use Hash;
use Mail;
use File;
 
 


class PostsController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
public function traits(){
	 return $this->one_signal();
 }
   
   
       // 1 use to get bitcoins balance of user from transactions table
 	public function get_user_bitcoin_balance(Request $request)
	{
		      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					// call bitcoins api
					
				 $balance =  \App\Transactions::where('user_id',$request->user_id)->sum('amount');
		 
		 
		          $d = array();
if($balance == '' or $balance == null or $balance == ' ')
{
$balance = 0;
}

		          $d1['balance'] = $balance;
		          $d[] = $d1;
			 
		            if($balance != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Bitcoin Balance Fetched Successfully';
                          $data['data']      =   $d;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error Occurred';
                          $data['data']      =   $d;  
					}
				   
	            return $data;
				}
	}
   
   
   
	// function to get country name from lat long
	function get_country_name($latitude , $longitude)
	{
 
		$data = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false&key=AIzaSyCmFaoOPzGV65KYGvIRp9nlMj8oV_fzDN0');
        $data_arr = json_decode($data);
        $results =  $data_arr->results;
 
          for($i=0;$i<sizeof($results);$i++)
              {
	              $c =  $results[$i]->address_components;
                      for($j=0;$j<sizeof($c);$j++)
                       {
		                   if($c[$j]->types[0] == 'country')
			               {
				             return $c[$j]->long_name;
			               }
	                    }
              }
	}
	
	
	
   
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
   
  } else if ($unit == "M") {
      return ($miles);
  }  else {
      return $miles;
  }
}



//add post
 public function add_post(Request $request)
 {




 


      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					$model = new \App\Posts();
                    $model->title = @$request->title;
					$model->user_id = @$request->user_id;

if(@$request->description == '' or @$request->description == null ) { $description = '';} else { $description =  @$request->description;}
					$model->description = @$description;
					$model->type = @$request->type;
					$model->payment_method = @$request->payment_method;
					$model->price = @$request->price;

if(!isset($request->paypal_id) or @$request->paypal_id =='' or @$request->paypal_id == null) { $paypal_id = 'Na';} else { $paypal_id = $request->paypal_id;}
					$model->paypal_id =  $paypal_id;
					$model->currency = @$request->currency;
					$model->latitude = @$request->latitude;
					$model->longitude = @$request->longitude;
					
 
					
					$country = $this->get_country_name( @$request->latitude , @$request->longitude );
					
					 $model->country = $country;
					
					if($country == null or $country == '' or $country == ' ')
					{
						$model->country = @\App\User::where('id',$request->user_id)->first(['country'])->country;
					}
					
					
					$model->terms_and_conditions = @$request->terms_and_conditions;
					$model->save();
					
					
	 
					
					if($model != '')
					{
					    $images_array = explode(",",$request->images);
						if(sizeof($images_array) > 0)
						{
						  for($y=0;$y<sizeof($images_array);$y++)
						  {
							$model1 = new \App\PostsImages();
							$model1->post_id = $model->id;
							$model1->image =str_replace(" ","",$images_array[$y]);
                                                        $model1->save();
						  }
						}
						
						
					@$shipping_methods =  json_decode(@$request->shipping_methods);
					
					
					if($shipping_methods !='')
					{
					   foreach($shipping_methods as $sm)
					    {
					      $sm1 = new \App\PostsShippingMethods();
                                              $sm1->post_id =  $model->id;
					      $sm1->title = $sm->title;
					      $sm1->price = $sm->price;
					      $sm1->save();
					    }
					}
					
					
						
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Post Submitted Successfully';
                       $data['data']      =   $model;  
					 
					 
					}
					 
					else
					{
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 
 
 
 
  public function edit_post(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					$model = \App\Posts::find($request->post_id);
                    $model->title = @$request->title;
			        $model->description = @$request->description;
					$model->type = @$request->type;
					$model->payment_method = @$request->payment_method;
					$model->price = @$request->price;
					$model->paypal_id = @$request->paypal_id;
					$model->currency = @$request->currency;
					$model->terms_and_conditions = @$request->terms_and_conditions;
					$model->save();

                    $deleted= @\App\PostsImages::where('post_id',$request->post_id)->delete();
					
					if($model != '')
					{
					    $images_array = explode(",",$request->images);
						if(sizeof($images_array) > 0)
						{
						  for($y=0;$y<sizeof($images_array);$y++)
						  {
							$image_name = str_replace(" ","",$images_array[$y]);
							  
							  
					 
							$exist = @\App\PostsImages::where('post_id',$request->post_id)->where('image',$image_name)->count();
							
					 
							if($exist < 1)
							{
							$model1 = new \App\PostsImages();
							$model1->post_id = $model->id;
							$model1->image =$image_name;
                                                        $model1->save();
							}
						  }
						}
						
						
					@$shipping_methods =  json_decode(@$request->shipping_methods);
					
					
					@\App\PostsShippingMethods::where('post_id',$model->id)->delete();
					
					
					if($shipping_methods !='')
					{
					   foreach($shipping_methods as $sm)
					    {
					      $sm1 = new \App\PostsShippingMethods();
                          $sm1->post_id =  $model->id;
					      $sm1->title = $sm->title;
					      $sm1->price = $sm->price;
					      $sm1->save();
					    }
					}
					
					   $post_details = \App\Posts::where('id',$request->post_id)->with('posts_images')->with('posts_shipping_methods')->get();
						
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Post Updated Successfully';
                       $data['data']      =   $post_details;  
					 
					 
					}
					 
					else
					{
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 
 
 
 
 
 //add post
 public function buy_post(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					 
					 
					  $user_bitcoins_balance = $this->get_user_bitcoin_balance($request);
					  $user_bitcoins_balance  = $user_bitcoins_balance["data"][0]["balance"];
					  
					  if(floatval($user_bitcoins_balance) < floatval($request->price))
					  {
					    $data1['status_code']    =   0;
                        $data1['status_text']    =   'Failed';             
                        $data1['message']        =   'You do not have enough bitcoins';
						$balance_array = array();
						$d1["bitcoin_balance"] = floatval($user_bitcoins_balance);
						$balance_array[] = $d1;
                        $data1['data']      =   $balance_array;
						return $data1;					   
					  }
					  
					  
	                $model = new \App\Orders();
                    $model->seller_id = $request->seller_id;
					$model->user_id = $request->user_id;
					$model->post_id = $request->post_id;
				    $model->price = $request->price;
					$model->status = 'new';
					$model->address = $request->address;
					$model->shipping_method_id = $request->shipping_method_id;
					$model->payment_method = $request->payment_method;
					$model->paypal_transaction_id = $request->paypal_transaction_id;
					$model->save();
					
					$user_first_name = \App\User::where('id',$request->user_id)->first(['first_name'])->first_name;
					//$post_payment_method = \App\Posts::where('id',$request->post_id)->first(['payment_method'])->payment_method;
					
					
					
		
							
							
					
					 
					if($model != '')
					{
						 
						if($request->payment_method == 'bitcoin')
						{
							
					      $sm1 = new \App\Transactions();
                          $sm1->type =  'order';
					      $sm1->user_id = $request->user_id;
					      $sm1->linked_id = $request->post_id;
						  $sm1->amount = -$request->price;
						  $sm1->description = 'Amount deducted for order #'.$model->id;
						  $sm1->deleted = '0';
					      $sm1->save();
						}
						  
					$token = @\App\UsersSessions::where('user_id',$request->seller_id)->pluck('notification_token');
					
				    $details = array();
		            $details['notification_type'] = 'order';
					$details['id'] = $model->id;
					
				       $this->notification_to_single_identifier($token , 'Buy Request' , $user_first_name.' buy your post' , $details ,'order');
						 
							
							
							
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'placed';
                       $data['data']      =   $model;  
					 }
					else
					{
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 
 
 
 
 	//get most recent questions
	public function get_posts(Request $request)
	{
		
	 
		 $validator = Validator::make($request->all(), [
	            'user_id' => 'required',
	        
	             ]);
	           if ($validator->errors()->all()) 
	            {
		            $data['status_code']    =   0;
		            $data['status_text']    =   'Failed';             
		            $data['message']        =   $validator->errors()->first();                   
	            }
	            else
                {   
                    if(User::where('id',$request->user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
 
 
                    $login_timezone = \App\User::where('id',$request->user_id)->first(['timezone'])->timezone;
                    $blocked_data = \App\BlockUnblock::select('user_id')->where('block_user_id',$request->user_id)->where('is_block',1)->get();
					$followings_data = \App\UsersRelations::select('recipient_id')->where('user_id',$request->user_id)->where('status','1')->get();
				 
			 
					
				      $followings_ids = array();
                      foreach ($followings_data as $key2 => $value2) 
					           {
							     if(User::where('id',$value2['recipient_id'])->count()==0)
                                  {  }
                                $followings_ids[] = $value2['recipient_id'];
                               }
				 
			 
                    if(count($blocked_data)>0)
                    {   
                        $blocked_ids = array();
                        foreach ($blocked_data as $key => $value) {
							 if(User::where('id',$value['user_id'])->count()==0)
                            { }
                             $blocked_ids[] = $value['user_id'];
                        }
						
						$followings_ids[] = intval($request->user_id);
						
						 
						 $posts = \App\Posts::whereIn('user_id', $followings_ids)->whereNotIn('user_id',$blocked_ids)->orWhere('user_id',$request->user_id)->with('posts_images');
					     $posts = $posts->orderBy('id','desc')->paginate(20)->toArray();
				 
					 }
                    else
                    {
						
						
					   $posts = \App\Posts::whereIn('user_id', $followings_ids)->orWhere('user_id',$request->user_id)->with('posts_images');
					    $posts = $posts->orderBy('id','desc')->paginate(20)->toArray();
					 }
					
					
					  
		 
		             for($p=0;$p<sizeof($posts['data']);$p++)
					 {

 	 
						 //$post_id = $posts['data'][$p]['id'];
						 $posts['data'][$p]['commentsCount'] = \App\PostsComments::where('post_id', $posts['data'][$p]['id'])->count();
						 $posts['data'][$p]['likes_count'] = \App\PostsLikes::where('post_id', $posts['data'][$p]['id'])->count();
						  $posts['data'][$p]['user_details'] = \App\User::where('id', $posts['data'][$p]['user_id'])->get(['id','first_name','last_name','profile_image','profile_image_thumb','address','country','private_status','username','verified']);
						 
					 	 
						 $is_liked = \App\PostsLikes::where('user_id',$request->user_id)->where('post_id', $posts['data'][$p]['id'])->count();
					 
						 if( $is_liked > 0)
						 {
							 $posts['data'][$p]['is_liked'] ='1';
							 
				 
							  if($login_timezone != '')
							 {
							 $posts['data'][$p]['created_at']=Carbon::parse( $posts['data'][$p]['created_at'])->setTimezone($login_timezone)->diffForHumans();
							 }
						 }
						 else
						 {
							 $posts['data'][$p]['is_liked']='0';
						 
							 if($login_timezone != '')
							 {
							 $posts['data'][$p]['created_at']=Carbon::parse( $posts['data'][$p]['created_at'])->setTimezone($login_timezone)->diffForHumans();
							 }
							 
							 	 //$auction["expiry_date"]=Carbon::parse($auction["expiry_date"])->setTimezone($request->timezone)->format('F d Y , h:i A');
						 }

$posts['data'][$p]['current_post'] ='0';
					 }
				 
					$following_data = \App\UsersRelations::select('user_id')->where('user_id',$request->user_id)->where('status','1')->get();
                    if(count($following_data)>0)
                    {
						$followings_id = array();
                        foreach ($following_data as $key => $value) {
                            if(User::where('id',$value['recipient_id'])->count()==0)
                            {
                                continue; 
                            }
                        $followings_id[] = $value['recipient_id'];
                        }
                    }
                    else
                    {
                        $followings_id = array();
                    }
					
					
				 
 


                    foreach($posts['data'] as $key => $value) {



 $is_follow = \App\UsersRelations::select('user_id')->where('user_id',$request->user_id)->where('recipient_id',$value['user_id'])->where('status','1')->count();

if($is_follow > 0)
{
$posts['data'][$key]['user']['is_follow'] = 1;

}
else
{
 $posts['data'][$key]['user']['is_follow'] = 0;
}
 
  
                    }
					
		$users_data = array();		

	                                        $d1["posts_count"] = '100';
						$d1["user_id"] = '';
						$d1["first_name"] = 'OneTime';
						$d1["last_name"] = '100';
$d1["username"] = 'OneTime';
$d1["verified"] = '1';
                                            
						$d1["profile_image"] = 'http://onetimeapp.co.uk/onetime/public/backend/dist/img/76-x-76.png';
$d1["created_at"] = '';

 	$users_data[] = $d1;
					
$data2 = \App\User::leftJoin('posts', 'users.id','=', 'posts.user_id')
                        ->select(  DB::raw('max(posts.user_id) AS posts_count'), 'posts.user_id','users.first_name','users.last_name','users.username','users.profile_image','users.verified' )
                        ->where('posts.user_id','<>',$request->user_id)->groupBy('posts.user_id')
                        ->take(5)->first();

if($data2 !='' && $data2 != null && $data2 != ' ')
{
 $users_data[]= $data2;
}

                    $unread_message_count = \App\Messages::where('user2',$request->user_id)->where('read_status','0')->where('type','chat')->get()->count();
				   
				 
				   if($unread_message_count > 99)
				   {
					   $unread_message_count = 99;
				   }
	 
	            $data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Posts Fetched sucessfullys';
					$data['unread_message_count'] = $unread_message_count;
                    $data['data']        =   $posts;
					$data['top_users']        =   $users_data;
	            }
	    return $data;
	}
	
	
	
	
	
		 	//get most recent questions
	public function search_posts(Request $request)
	{
		
	 
		 $validator = Validator::make($request->all(), [
	            'user_id' => 'required',
	        
	             ]);
	           if ($validator->errors()->all()) 
	            {
		            $data['status_code']    =   0;
		            $data['status_text']    =   'Failed';             
		            $data['message']        =   $validator->errors()->first();                   
	            }
	            else
                {   
                    if(User::where('id',$request->user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
 
 
                    $login_timezone = \App\User::where('id',$request->user_id)->first(['timezone'])->timezone;
                  
			  
			  
			  
			  		$followings_data = \App\UsersRelations::select('recipient_id')->where('user_id',$request->user_id)->where('status','1')->get();
				 
			 
					
				      $followings_ids = array();
                      foreach ($followings_data as $key2 => $value2) 
					           {
							     if(User::where('id',$value2['recipient_id'])->count()==0)
                                  {  }
                                $followings_ids[] = $value2['recipient_id'];
                               }
							   
							   
						
					   $posts = \App\Posts::select('posts.id', 'posts.user_id', 'posts.title','posts.description','posts.type','posts.payment_method','posts.currency', 'posts.price','posts.paypal_id', 'posts.terms_and_conditions','posts.latitude','posts.longitude','posts.country','posts.created_at','posts.updated_at')->where(function($q) use ($request) {
                                $q->where( DB::raw("CONCAT(title,' ',description)"),'like', '%'.$request->search_text.'%');
                            })->join('users', function ($join)  use  ($followings_ids){
            $join->on('users.id', '=', 'posts.user_id')->where('users.private_status', '=', '0');
               // ->where('users.private_status', '=', '0')->orWhereIn('posts.user_id',$followings_ids);
        })->with('posts_images')->paginate(20)->toArray();




					   $posts2 = \App\Posts::select('posts.id', 'posts.user_id', 'posts.title','posts.description','posts.type','posts.payment_method','posts.currency', 'posts.price','posts.paypal_id', 'posts.terms_and_conditions','posts.latitude','posts.longitude','posts.country','posts.created_at','posts.updated_at')->with('posts_images')->paginate(20)->toArray();


 
		
	 	 

  $postsss = \App\Posts::where(function($q) use ($request) {
                                $q->where( DB::raw("CONCAT(title,' ',description)"),'like', '%'.$request->search_text.'%')->get();
                            })->with('posts_images')->join('users', function ($join)  use  ($followings_ids){
            $join->on('posts.user_id', '=', 'users.id');
               // ->where('users.private_status', '=', '0')->orWhereIn('posts.user_id',$followings_ids);
        })->paginate(20)->toArray();
				 
					 
				 
					
		             $new_array = array();
		             for($p=0;$p<sizeof($posts['data']);$p++)
					 {
						 
						 
					 
						 
						 $post_user_id = $posts['data'][$p]['user_id'];
						 $private_status = @\App\User::where('id',$post_user_id)->first(['private_status'])->private_status;
						
						
						
						if($private_status =='0' or $private_status == 0)
						 {
						 $posts['data'][$p]['commentsCount'] = \App\PostsComments::where('post_id', $posts['data'][$p]['id'])->count();
						 $posts['data'][$p]['likes_count'] = \App\PostsLikes::where('post_id', $posts['data'][$p]['id'])->count();
						 $posts['data'][$p]['user_details'] = \App\User::where('id', $posts['data'][$p]['user_id'])->get(['id','first_name','last_name','username','verified','profile_image','profile_image_thumb','address','country']);
						 
						 
						 
						 $is_liked = \App\PostsLikes::where('user_id',$request->user_id)->where('post_id', $posts['data'][$p]['id'])->count();
					 
////return $posts['data'][$p]['id'];
$posts['data'][$p]['post_images'] =@\App\PostsImages::where('post_id',$posts['data'][$p]['id'])->get();
						 if( $is_liked > 0)
						 {
							 $posts['data'][$p]['is_liked'] ='1';
							 
				 
							  if($login_timezone != '')
							 {
							 $posts['data'][$p]['created_at']=Carbon::parse( $posts['data'][$p]['created_at'])->setTimezone($login_timezone)->diffForHumans();
							 }
						 }
						 else
						 {
							 $posts['data'][$p]['is_liked']='0';
						 
							 if($login_timezone != '')
							 {
							 $posts['data'][$p]['created_at']=Carbon::parse( $posts['data'][$p]['created_at'])->setTimezone($login_timezone)->diffForHumans();
							 }
							 
							 	 //$auction["expiry_date"]=Carbon::parse($auction["expiry_date"])->setTimezone($request->timezone)->format('F d Y , h:i A');
						 }
					 }
					 
				 
				 
					 
					 }
				 
	 
 
					 

                    foreach($posts['data'] as $key => $value) {



 $is_follow = \App\UsersRelations::select('user_id')->where('user_id',$request->user_id)->where('recipient_id',$value['user_id'])->where('status','1')->count();

if($is_follow > 0)
{
$posts['data'][$key]['user']['is_follow'] = 1;

}
else
{
 $posts['data'][$key]['user']['is_follow'] = 0;
}
 
  
                    }









	            	$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Posts Fetched sucessfully';
                    $data['data']        =   $posts;
	            }
	    return $data;
	}
	
	
	
	    public function currency_bitcoin_price(Request $request)
    {        
	 
	               // $countries = Country::get(["country_name","nicename"]);
					$allarray = array();
			 
			        $data_array = array();
					$d["bitcoin_price"] = 1000;
					$data_array[] = $d;
			         
				 	$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Bitcoin Price Fetched';
                    $data['data']      =   $data_array;
		            return $data;
	}
	
	
	 	//get most recent questions
	public function get_post_detail(Request $request)
	{
		 $validator = Validator::make($request->all(), [
	            'user_id' => 'required',
	        
	             ]);
	           if ($validator->errors()->all()) 
	            {
		            $data['status_code']    =   0;
		            $data['status_text']    =   'Failed';             
		            $data['message']        =   $validator->errors()->first();                   
	            }
	            else
                {   
                    if(User::where('id',$request->user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }

                   $bitcoin_details = @\App::call('\App\Http\Controllers\Api\BitcoinController@currency_bitcoin_price');
				   $bitcoin_price = $bitcoin_details["data"][0]['bitcoin_price'];
			 
			 
			 
                   $post = @\App\Posts::where('id',$request->post_id)->with('posts_images')->with('posts_shipping_methods')->get();
				   $post[0]["commentsCount"] = @\App\PostsComments::where("post_id",$request->post_id)->count();

                                   $user_details = @\App\User::where("id",$post[0]["user_id"])->get(['id','first_name','last_name','username','verified','country','private_status','profile_image']);


                                   $post_user_id = $user_details[0]->id;
 

                                   $is_follow = @\App\UsersRelations::where('user_id',$request->user_id)->where('recipient_id',$post_user_id )->where('status','1')->count();

 

if($is_follow > 0)
{
$user_details[0]->is_follow = '1';
}
else

{
$user_details[0]->is_follow  = '0';
}

 
				   $post[0]["user_details"] = $user_details;
				   $post[0]["likesCount"] = \App\PostsLikes::where("post_id",$request->post_id)->count();
				   $is_liked= \App\PostsLikes::where("post_id",$request->post_id)->where("user_id",$request->user_id)->count();
				   
		 
				   if($is_liked > 0)
				   {
					   $post[0]["is_liked"] = '1';
				   }
				   else
				   {
					   $post[0]["is_liked"] = '0';
				   }
				 
				 
				 $post[0]["is_liked"] = '0';
				 
				 
				 
				 

	            	$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Post Detail Fetched sucessfully';
                    $data['data']        =   $post;
					$data['bitcoin_price']        =   $bitcoin_price;
	            }
	    return $data;
	}
	
	
	 
  

  
  
//add post
public function write_post_comment(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					$model = new \App\PostsComments();
                    $model->post_id = $request->post_id;
					$model->user_id = $request->user_id;
					$model->comment = $request->comment;
                    $model->save();
					 
					if($model != '')
					{
                        //sending notifications
						@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['username'])->username;
					
						$post_user_id = @\App\Posts::where('id',$request->post_id)->first(['user_id'])->user_id;
			  
						@$token = @\App\UsersSessions::where('user_id',$post_user_id)->pluck('notification_token');
						  
						@$notify_comment = @\App\User::where('id',$post_user_id)->first(['notify_comment'])->notify_comment; 
					 
						$inc = \App\User::where("id",$post_user_id)->increment('badge_count');
						$details = array();
					    $sub_title = @\App\NotificationTriggers::where('trigger_type','comment')->where('notification_type','push')->first(['sub_title'])->sub_title;
						 
						$details1['user_id'] = @$request->user_id;
						$details1['id'] = @$request->post_id;
						$details1['notification_type'] = 'comment';
  
						if($post_user_id != $request->user_id)
						{
							
							
							$user_id = $request->user_id;
							 $df["sender_name"] = @$follower_first_name;
							 $this->create_notification('post', $follower_first_name.' just commented on your post',      $post_user_id,       $user_id,   $details1 , $df);
							 
							 // $this->create_notification($notification_type,$notification_message,      $user_id,               $notification_by_user_id,    $details , $df='');
						}
						 
						
						
						
						if($notify_comment == 1 or $notify_comment == '1')
						{
						 
							 $df["sender_name"] = @$follower_first_name;
				 
	 
							$sub_title =  $this->get_notification_string($sub_title, $df);
							
							if($post_user_id != $request->user_id)
							{
								
						 
							    $this->notification_to_single_identifier($token , 'Comment' , $sub_title , $details1 , 'post');
							}
						}
                    //sending notification ends
					
					
		 
                    $model2 =  \App\PostsComments::where('post_id',$request->post_id)->select(DB::raw('id,comment,user_id,created_at'))->orderBy('id','desc')->paginate( 20);
                    foreach($model2 as $m)
					{
						 $m->user_details = \App\User::where('id',$m->user_id)->get(['id','first_name','username','verified','profile_image_thumb','profile_image'])->first();
					}
					 
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Comments Fetched Successfully';
                       $data['data']      =   $model2;  
					 
					}

					//  $post_comments = \App\PostsComments::where('post_id',$request->post_id )->where('user_id',$request->user_id)->orderBy('id','desc')->take(20)->get();
		 
					 
					else
					{
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 
 
 
 
 
	
     public function create_notification($notification_type,$notification_message,$user_id,$notification_by_user_id, $details , $df='')
    {               
                    $for_user_notification = User::select('id','notification_token')->where('id',$user_id)->first();    
                    App::setLocale($for_user_notification['language']);
					
				 
			        $sub_title = @\App\NotificationTriggers::where('notification_type','app')->where('trigger_type',$notification_type)->first(['sub_title'])->sub_title;
			        $sub_title = $this->get_notification_string(@$sub_title , @$df);
			  
                    $input = array();                
                    $input['notification_type'] = @$notification_type;        
                    $input['notification_message'] = @$sub_title;
					if($sub_title == null or $sub_title == '')
					{
						$input['notification_message'] = @$notification_message;
					}
					
                    $input['user_id'] = @$user_id;
					$input['linked_id'] =@$details['id'];
					 
                    $input['is_read'] = 0;
                   
                    $input['status'] = 1;
                    $notification = \App\Notification::create($input);
                    $badge_count = $for_user_notification['badge_count']+1;
                    @\App\User::where('id',$for_user_notification['id'])->update(['badge_count'=>$badge_count]);
    }
	
 
 
  public function get_post_comment(Request $request)
 {
              $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	                $model =  \App\PostsComments::where('post_id',$request->post_id)->select(DB::raw('id,comment,user_id,created_at'))->orderBy('id','desc')->orderBy('id','DESC')->paginate( 20);
                    foreach($model as $m)
					{
						 $m->user_details = \App\User::where('id',$m->user_id)->get(['id','first_name','username','verified','profile_image_thumb','profile_image'])->first();
					}
					if(count($model) > 0)
					 {
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Comments Fetched Successfully';
                       $data['data']      =   $model;  
					 }
					else
					 {
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
					 }
	            return $data;
				}
 }
 
 
 
 
 
   
 
 
 
 
 
  public function get_users_reviews2(Request $request)
 {
              $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	                $model =  \App\UsersReviews::where('recipient_id',$request->other_user_id )->orderBy('id','desc')->paginate(10);
					
					

                    foreach($model as $m)
					{
												 $m->user_details = \App\User::where('id',$m->user_id)->get(['id','first_name','profile_image_thumb','username','verified','profile_image'])->first();
                                                 $review_user_ratings = @\App\UserRatings::where('profile_user_id', $m->user_id)->where('user_id',$request->user_id)->sum('rating');
												 
												 
												 
												 
												 
												 
										 

                                                 if($review_user_ratings == '' or $review_user_ratings == null)
                                                 {
                                                     $m->user_ratings = '0';
                                                 }
                                                 else
                                                 {
													$m->user_ratings = $review_user_ratings;
												 }
												 
												 
												 
												 //Average Ratings
												 					$user_id = '1';
					$total_trades_ratings_as_seller_count = \App\Trades::where('seller_id',$user_id)->where('seller_ratings','<>','')->count();
					$total_trades_ratings_as_buyer_count = \App\Trades::where('buyer_id',$user_id)->where('buyer_ratings','<>','')->count();
					
					
		 
					$total_trades_ratings_as_seller_sum = \App\Trades::where('seller_id',$user_id)->where('seller_ratings','<>','')->sum('seller_ratings');
					if($total_trades_ratings_as_seller_sum =='') {$total_trades_ratings_as_seller_sum = 0;}
					
					$total_trades_ratings_as_buyer_sum = \App\Trades::where('buyer_id',$user_id)->where('buyer_ratings','<>','')->sum('buyer_ratings');
					if($total_trades_ratings_as_buyer_sum =='') {$total_trades_ratings_as_buyer_sum = 0;}
					
				    $total_trades_ratings_count = $total_trades_ratings_as_seller_count + $total_trades_ratings_as_buyer_count;
					$total_trades_ratings_sum = $total_trades_ratings_as_seller_sum + $total_trades_ratings_as_buyer_sum;
					
					 
					 if(@$total_trades_ratings_count > 0)
					 {
					 
					@$trades_ratings_avg = @$total_trades_ratings_sum / @$total_trades_ratings_count;
					 }
					 else{
						 @$trades_ratings_avg = 0;
					 }
					
					
					
					
				 
					
					$total_orders_ratings_as_seller_count = \App\Orders::where('seller_id',$user_id)->where('seller_ratings','<>','')->count();
					
				 
					$total_orders_ratings_as_buyer_count = \App\Orders::where('user_id',$user_id)->where('buyer_ratings','<>','')->count();
					
					$total_orders_ratings_as_seller_sum = \App\Orders::where('seller_id',$user_id)->where('seller_ratings','<>','')->sum('seller_ratings');
					if($total_orders_ratings_as_seller_sum =='') {$total_orders_ratings_as_seller_sum = 0;}
					
					$total_orders_ratings_as_buyer_sum = \App\Orders::where('user_id',$user_id)->where('buyer_ratings','<>','')->sum('buyer_ratings');
					if($total_orders_ratings_as_buyer_sum =='') {$total_orders_ratings_as_buyer_sum = 0;}
					
				    $total_orders_ratings_count = $total_orders_ratings_as_seller_count + $total_orders_ratings_as_buyer_count;
					$total_orders_ratings_sum = $total_orders_ratings_as_seller_sum + $total_orders_ratings_as_buyer_sum;
					
					
						 if(@$total_orders_ratings_count > 0)
					 {
					 
					 
					$orders_ratings_avg = $total_orders_ratings_sum."/".$total_orders_ratings_count;
					 }
					 else{
						  
					$orders_ratings_avg = 0;
					 }
					 
					
					
					
	                $sum = $total_trades_ratings_sum+$total_orders_ratings_sum;
					$count = $total_trades_ratings_count+$total_orders_ratings_count;
	               
			 
				    
					return $count;
					
					
					
					if($count > 0)
					{
						$ratings_average = $sum/$count;
					}
					else
					{
						$ratings_average = 0;
					}
					$m->user_ratings = $ratings_average;
					}
					
					if(count($model) > 0)
					 {
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Reviews Fetched Successfully';
                       $data['data']      =   $model;  
					 }
					else
					 {
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Review Found';
                               $data['data']      =   [];  
					 }
	            return $data;
				}
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  public function get_users_reviews(Request $request)
 {
              $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	                $model =  \App\UsersReviews::where('recipient_id',$request->other_user_id )->orderBy('id','desc')->paginate(10);
					
					foreach($model as $m)
					{
						$m->user_details = @\App\User::where('id',$m->user_id)->get(['id','first_name','last_name','profile_image','username','verified']);
						
					}
					
                    
					 
					
					if(count($model) > 0)
					 {
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Reviews Fetched Successfully';
                       $data['data']      =   $model;  
					 }
					else
					 {
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'No Review Found';
                               $data['data']      =   [];  
					 }
	            return $data;
				}
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	
	
 
 
 
 
 
  //like offer
 public function like_post(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	                $exist = @\App\PostsLikes::where("post_id",$request->post_id)->where("user_id",$request->user_id)->count();
					if($exist < 1)
					{
					 $model = new \App\PostsLikes();
                    $model->user_id = $request->user_id;
					$model->post_id = $request->post_id;
			        $model->save();
					
					
					
					
					
									
					//sending notifications
					@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['username'])->username;
					$post_user_id = @\App\Posts::where('id',$request->post_id)->first(['user_id'])->user_id;
	                @$token = @\App\UsersSessions::where('user_id',$post_user_id)->pluck('notification_token');
	                @$notify_like = @\App\User::where('id',$post_user_id)->first(['notify_like'])->notify_like; 
					
					 
					  
					$inc = \App\User::where("id",$post_user_id)->increment('badge_count');
					$details = array();
		            $details['user_id'] = @$request->user_id;
		            $details['notification_type'] = 'like';
					$details['id'] = $request->post_id;
				 
					
					if($post_user_id != $request->user_id)
					{
					  $df['sender_name'] = $follower_first_name;
				      $this->create_notification('post', $follower_first_name.' just liked your post',$post_user_id,$request->user_id,$details , $df);
					}
					 
					 
                  
		            if($notify_like == 1 or $notify_like == '1')
		            {
						if($post_user_id != $request->user_id)
						{
							$df["sender_name"] = @$follower_first_name;
				 
							$sub_title = @\App\NotificationTriggers::where('trigger_type','like')->where('notification_type','push')->first(['sub_title'])->sub_title;
							$sub_title =  $this->get_notification_string($sub_title, $df);
							
							if($post_user_id != $request->user_id)
							{
								 $this->notification_to_single_identifier($token , 'like' , $sub_title , $details ,'post');
							}
							 
							 
						}
		            }
                   //sending notification ends
		
		
		
		
					   $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Post liked Successfully';
                $data['user_data']      =   [];  
					}
					else
					{
							   $data['status_code']    =   0;
                $data['status_text']    =   'Success';             
                $data['message']        =   'You Already liked this Post';
                $data['user_data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 
 
   //dislike offer
 public function unlike_post(Request $request)
 {
 
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	             $exist = \App\PostsLikes::where("post_id",$request->post_id)->where("user_id",$request->user_id)->delete();
		 
				 $data['status_code']    =   1;
                 $data['status_text']    =   'Success';             
                 $data['message']        =   'Post Unliked Successfully';
                 $data['user_data']      =   [];  
				 return $data;
				}
				
 }
 
 
 
 
 
 
 
////////////////////////Listings Api Starts Here///////////////////////////////////////////////

public function my_listings(Request $request)
{
 
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	  $posts = \App\Posts::where("user_id",$request->user_id)->where("type",'sale')->with('posts_images')->orderBy('id','DESC')->paginate(10);
				 
				 
	       
		 
				 $data['status_code']    =   1;
                 $data['status_text']    =   'Success';             
                 $data['message']        =   'Fetched Successfully';
                 $data['user_data']      =   $posts;  
				 return $data;
				}
				
}







public function delete_posts(Request $request)
{

$post_id = $request->post_id;

@\App\Posts::where('id',$post_id)->delete();
@\App\PostsImages::where('post_id',$post_id)->delete();
@\App\PostsComments::where('post_id',$post_id)->delete();
@\App\PostsLikes::where('post_id',$post_id)->delete();
@\App\PostsShippingMethods::where('post_id',$post_id)->delete();
@\App\ReportedPosts::where('post_id',$post_id)->delete();

		 $data['status_code']    =   1;
                 $data['status_text']    =   'Success';             
                 $data['message']        =   'Post Deleted Successfully';
                 $data['user_data']      =   [];  
				 return $data;

}




public function my_buyings(Request $request)
{
 
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	             $posts = \App\Orders::where("user_id",$request->user_id)->where('status',$request->status)->with('posts')->paginate(10);
				 
			 
				foreach($posts as $p)
				{
					  $p->posts_images = \App\PostsImages::where('post_id',$p->posts->id)->get();
				}
          
				  if(count($posts) > 0)
				  {
				   $data['status_code']    =   1;
                   $data['status_text']    =   'Success';             
                   $data['message']        =   'Fetched Successfully';
                   $data['user_data']      =   $posts;  
				   return $data;	  
				  }
				  else
				  {
				   $data['status_code']    =   0;
                   $data['status_text']    =   'Failed';             
                   $data['message']        =   'No Products Found';
                   $data['user_data']      =   [];  
				   return $data;				   
					  
				  }
		 
		         

				}
				
}





public function my_sellings(Request $request)
{
 
 
 
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					
					 
	             $posts = \App\Orders::where("seller_id",$request->user_id)->where('status',$request->status)->with('posts')->paginate(10);
          
		  
		 
		 
		    	foreach($posts as $p)
				{
					  $p->posts_images = \App\PostsImages::where('post_id',@$p->id)->get();
				}
				
				
		  
				  if(count($posts) > 0)
				  {
				   $data['status_code']    =   1;
                   $data['status_text']    =   'Success';             
                   $data['message']        =   'Fetched Successfully';
                   $data['user_data']      =   $posts;  
				   return $data;	  
				  }
				  else
				  {
				   $data['status_code']    =   0;
                   $data['status_text']    =   'Failed';             
                   $data['message']        =   'No Products Found';
                   $data['user_data']      =   [];  
				   return $data;				   
					  
				  }
		 
		         

				}
				
}






public function order_details(Request $request)
{
  
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	    $posts = \App\Orders::where("id",$request->order_id)->with('posts')->with(array('users'=>function($query){
        $query->select("id","first_name","last_name","email","country","username","verified");
    }))->with(array('seller'=>function($query){
        $query->select("id","first_name","last_name","email","country","username","verified");
    }))->with(array('buyer'=>function($query){
        $query->select("id","first_name","last_name","email","country","username","verified");
    }))->paginate(10);
	
	
 
		$payment_method = $posts[0]['posts']["payment_method"];
		
		 
		  		 foreach($posts as $p)
				{
					
					$post_price = $p->posts->price;
					
					$payment_method = $p->payment_method;
					if($payment_method == 'bitcoin')
				    {
						 $rate = @$this->currency_bitcoin_price($request);
					     $bitcoin_price = $rate['data'][0]['bitcoin_price'];
					     $p->posts->price  = sprintf( '%0.6f', floatval($post_price)/floatval($bitcoin_price) );
						 $p->order_price = sprintf( '%0.6f', floatval($p->order_price)/floatval($bitcoin_price));
						 @$p->post_currency_formatted = '';
					}
					else { @$p->post_currency_formatted = $p->post_currency;}
					  
					  @$p->posts_images = @\App\PostsImages::where('post_id',$p->posts->id)->get();
					  
					  
					  $post_shipping_methods = @\App\PostsShippingMethods::where('id',$p->shipping_method_id)->get();
					 
	                                  if($payment_method == 'bitcoin')
				          {					 
					    $post_shipping_methods[0]['price'] =  sprintf( '%0.6f', $post_shipping_methods[0]['price'] / floatval($bitcoin_price) );
					  }
					  
					   @$p->posts_shipping_method =$post_shipping_methods ;
					   
				
					  
					  
					 
					  
					 // return $bitcoin_price;
					 
					 
					  
					   
				}
		   
	 
		   $seller_id = @\App\Orders::where("id",$request->order_id)->first(['seller_id'])->seller_id;
		   
		   
		   
		   $user_id = @\App\Orders::where("id",$request->order_id)->first(['user_id'])->user_id;
		   $status = @\App\Orders::where("id",$request->order_id)->first(['status'])->status;
		   $created_at = @\App\Orders::where("id",$request->order_id)->first(['created_at'])->created_at;
		   $seller_feedback = @\App\Orders::where("id",$request->order_id)->first(['seller_feedback'])->seller_feedback;
           $buyer_feedback = @\App\Orders::where("id",$request->order_id)->first(['buyer_feedback'])->buyer_feedback;			
		   
		 
		   $buttons_array =array();
		   
	 
					 if($seller_id == $request->user_id)
					 {			
 
					  if($status =='new')
					  {
					  $button1["title"] = 'Accept Order';
					  $button1['Enabled'] = '1';
					  $button1['ActionStatus'] = 'accepted';
					  
					  $button2["title"] = 'Cancel Order';
					  $button2['Enabled'] = '1';
					  $button2['ActionStatus'] = 'cancelled';
					  }
					  
					  if($status =='accepted')
					  {
					  $button1["title"] = 'Mark Shipped';
					  $button1['Enabled'] = '1';
					  $button1['ActionStatus'] = 'shipped';
					  
					  $button2["title"] = 'Cancel Order';
					  $button2['Enabled'] = '1';
					  $button2['ActionStatus'] = 'cancelled';
					  }
					  
					  if($status =='shipped')
					  {
					  $button1["title"] = 'Shipped';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel Order';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					   
					  
					  if($status =='cancelled')
					  {
					  $button1["title"] = 'Cancelled';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel Order';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  if($status =='disputed')
					  {
						  

					  $button1["title"] = 'Disputed';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Cancel Order';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  
					  
					  if($status =='completed')
					  {
						    if($seller_feedback == 'null' or $seller_feedback == null or $seller_feedback == ' ')
						  { $enabled = '1'; }
						  else { $enabled = '0'; }
					  $button1["title"] = 'Give FeedBack';
					  $button1['Enabled'] = $enabled;
					  $button1['ActionStatus'] = 'feedback';
						
				        $button2["title"] = 'Disputed';
					    $button2['Enabled'] = '0';
					    $button2['ActionStatus'] = '';

						
					  
					   
					  }
					  
					  		  
					  if($status =='feedback')
					  {
						   $button1["title"] = 'Feedback';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  
					    if($seller_feedback == 'null' or $seller_feedback == null or $seller_feedback == ' ')
						  { $enabled = '1'; }
						  else { $enabled = '0'; }
					  $button1["title"] = 'Give FeedBack';
					  $button1['Enabled'] = $enabled;
					  $button1['ActionStatus'] = 'feedback';
					  
					  
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  
					  
				 
						  
					  
					  
					  
					  }
					  
					 }
					 
					
 
 				
					if($user_id == $request->user_id)
					 {	
				 
			 
			 
		 
					  if($status =='new')
					  {
					  $button1["title"] = 'Release Payment';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = 'completed';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = 'disputed';
					  }
					  
					  if($status =='accepted')
					  {
					  $button1["title"] = 'Release Payment';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = 'completed';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = 'disputed';
					  }
					  
					  if($status =='shipped')
					  {
					  $button1["title"] = 'Release Payment';
					  $button1['Enabled'] = '1';
					  $button1['ActionStatus'] = 'completed';
					
					
					 
					if($payment_method != 'bitcoin')
					{
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = 'disputed';
					}
					else{
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '1';
					  $button2['ActionStatus'] = '';
					}

					  }
					  
					  if($status =='completed')
					  {
						 
						
						  if($buyer_feedback == 'null' or $buyer_feedback == null or $buyer_feedback == ' ')
						  { $enabled = '1'; }
						  else { $enabled = '0'; }
					  $button1["title"] = 'Give FeedBack';
					  $button1['Enabled'] = $enabled;
					  $button1['ActionStatus'] = 'feedback';
					  
					  
					  
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  
					  
					  }	
					  
					  if($status =='cancelled')
					  {
					  $button1["title"] = 'Cancelled';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  if($status =='disputed')
					  {
					  $button1["title"] = 'Disputed';
					  $button1['Enabled'] = '0';
					  $button1['ActionStatus'] = '';
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  }
					  
					  
					  if($status =='feedback')
					  {
						 $button1["title"] = 'Feedback';
					     $button1['Enabled'] = '0';
					     $button1['ActionStatus'] = '';
					  
					  
					  if($buyer_feedback == 'null' or $buyer_feedback == null or $buyer_feedback == ' ')
					  {  
					   $button1["title"] = 'Give FeedBack';
					   $button1['Enabled'] = '1';
					   $button1['ActionStatus'] = 'feedback';
					  }
						  
					  
					  
					  
					  
					  
					  $button2["title"] = 'Dispute';
					  $button2['Enabled'] = '0';
					  $button2['ActionStatus'] = '';
					  
					  
					  }
					  
				 
					  
					  
					  
					  
					  
					 }
					 
					 

					 
					 
					 $present = \Carbon\Carbon::now();
					 $diff =  strtotime($present) - strtotime($created_at);
					 if(intval($diff) < 60)
					 {
					    $button2['Enabled'] = '0';
					 }
					  
					  $buttons_array[] = $button1;
					  $buttons_array[] = $button2;
				 
					  $p->buttons = $buttons_array;
					   
				   
					$unread_message_count = \App\Messages::where('user2',$request->user_id)->where('read_status','0')->where('type','post')->get()->count();
				   
				   
			 
			 
				   
				   if(!isset($unread_message_count) or $unread_message_count == null or $unread_message_count == '')
				   {
					   $unread_message_count = 0;
				   }
				 
				     if($unread_message_count > 99)
				     {
				 	   $unread_message_count = 99;
			         }
					   
		 		  
				  if(count($posts) > 0)
				  {
				   $data['status_code']    =   1;
                   $data['status_text']    =   'Success';             
                   $data['message']        =   'Fetched Successfully';
				   $data['unread_message_count']  =  $unread_message_count;
                   $data['user_data']      =   $posts;  
				   return $data;	  
				  }
				  else
				  {
				   $data['status_code']    =   0;
                   $data['status_text']    =   'Failed';             
                   $data['message']        =   'No Products Found';
                   $data['user_data']      =   [];  
				   return $data;				   
					  
				  }
		 
		         

				}
				
}



public function update_order_status(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
		 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$order_id = $request->order_id;
		    $status = $request->status;
			
		
		    
			if($status == 'completed')
			{
			 $wallet_pin = $request->wallet_pin;
			 $user_wallet_pin = @\App\User::where('id',$request->user_id)->first(['wallet_pin'])->wallet_pin;
		
		    if($user_wallet_pin != $wallet_pin)
			{
			    $data['status_code']    =   0;
                $data['status_text']    =   'Failed';
                $data['message']        =   'You entered invalid wallet pin';
				return $data;
				
			}
			}

			
			    $seller_id = @\App\Orders::where('id',$request->order_id)->first(['seller_id'])->seller_id;
				$order_price = @\App\Orders::where('id',$request->order_id)->first(['price'])->price;
				
				
			
				if($status == 'completed' && $request->payment_method == 'bitcoin')
			{
				          $sm1 = new \App\Transactions();
                          $sm1->type =  'order';
					      $sm1->user_id = $seller_id ;
					      $sm1->linked_id = $request->order_id;
						  $sm1->amount = $order_price;
						  $sm1->description = 'Payment received for order #'.$request->order_id;
						  $sm1->deleted = '0';
					      $sm1->save();
						  
						  
						  //deduct 1 percent also
						  $d1 = new \App\Transactions();
                          $d1->type =  'order';
					      $d1->user_id = $seller_id;
					      $d1->linked_id = $request->order_id; // 
						  $d1->amount = -$order_price * ( env('ORDERS_COMMISSION') / 100 );
						  $d1->description = 'Commission fee taken for order #'.$request->order_id;
						  $d1->deleted = '0';
					      $d1->save();
				
			}
		 
		 
		 if( $request->status == 'feedback' ) { $status = 'completed';} else { $status = $request->status;}
		 @\App\Orders::where('id',$request->order_id)->update([ 'status' => $status]);
		
         
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Status Updated successfully';

        }
        return $data;
    }
	
	
	
	
	
	
	
	public function add_orders_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$order_id = @$request->order_id;
			$trade_id = @$request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
			
			
		  
		  
		   if($order_id !='' && $order_id != null && $order_id != ' ')
		   {
		  
		    $order_seller_id = @\App\Orders::where('id',$request->order_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Orders::where('id',$request->order_id)->first(['user_id'])->user_id;
			
			
			$seller_token = @\App\UsersSessions::where('user_id',$order_seller_id)->pluck('notification_token');
			$buyer_token = @\App\UsersSessions::where('user_id',$order_buyer_id)->pluck('notification_token');
			
			$seller_name = @\App\User::where('id',$order_seller_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$order_seller_id)->first(['last_name'])->last_name;
			$buyer_name = @\App\User::where('id',$order_buyer_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$order_buyer_id)->first(['last_name'])->last_name;
			
			if($user_id == $order_seller_id)
			{
				    @\App\Orders::where('id',$request->order_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
				 
				    $model = new \App\UsersReviews();
                    $model->recipient_id = $order_buyer_id;
					$model->user_id = $order_seller_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
					 
		            $details['notification_type'] = 'feedback';
					$details['id'] = @$request->order_id;
				 
					   $this->notification_to_single_identifier($buyer_token , 'Feedback' , $buyer_name.' review you for order #'.$request->order_id , $details ,'feedback');
			      
		 		
			}
			
			if($user_id == $order_buyer_id)
			{
				
				    @\App\Orders::where('id',$request->order_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
				 
				  	$model = new \App\UsersReviews();
                    $model->recipient_id = $order_seller_id;
					$model->user_id = $order_buyer_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
				 
		            $details['notification_type'] = 'feedback';
					$details['id'] = @$request->order_id;
				  
			     
		 		  $this->notification_to_single_identifier($seller_token , 'Feedback' , $seller_name.' review you for order #'.$request->order_id , $details ,'feedback');
		    }
		   }
		   
		   
		    
			
			
			
		   if($trade_id !='' && $trade_id != null && $trade_id != ' ')
		   {
		  
		    $order_seller_id = @\App\Trades::where('id',$trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$trade_id)->first(['buyer_id'])->buyer_id;
			
			
			$seller_token = @\App\UsersSessions::where('user_id',$order_seller_id)->pluck('notification_token');
			$buyer_token = @\App\UsersSessions::where('user_id',$order_buyer_id)->pluck('notification_token');
			
			$seller_name = @\App\User::where('id',$order_seller_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$order_seller_id)->first(['last_name'])->last_name;
			$buyer_name = @\App\User::where('id',$order_buyer_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$order_buyer_id)->first(['last_name'])->last_name;
			
			
			
			if($user_id == $order_seller_id)
			{
				 \App\Trades::where('id',$trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
				 
				 
				    $model = new \App\UsersReviews();
                    $model->recipient_id = $order_buyer_id;
					$model->user_id = $order_seller_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
					
					 
		            $details['notification_type'] = 'feedback';
					$details['id'] = @$request->trade_id;
				  $this->notification_to_single_identifier($buyer_token , 'Feedback' , $buyer_name.' review you for order #'.$request->order_id , $details ,'feedback');
					
					
			     
		   }
			 
			if($user_id == $order_buyer_id)
			{
				 \App\Trades::where('id',$trade_id)->update([ 'buyer_ratings' =>$ratings , 'buyer_feedback' => $feedback]);
				 
				    $model = new \App\UsersReviews();
                    $model->recipient_id = $order_seller_id;
					$model->user_id = $order_buyer_id;
					$model->review = $feedback;
					$model->rating = $ratings;
                    $model->save();
					
					
				 
		            $details['notification_type'] = 'feedback';
					$details['id'] = @$request->trade_id;
				  
			      
					   $this->notification_to_single_identifier($seller_token , 'Feedback' , $seller_name.' review you for order #'.$request->order_id , $details ,'feedback');
					
		    }
			
			
		   }
		  
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }
 

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}