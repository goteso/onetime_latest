<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use App\Traits\one_signal; // <-- you'll need this line...
use Hash;
use Mail;
use File;

use App\CoinbaseLog;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\CoinBasePayments;
//use Coinbase\CoinBasePayments;

use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Account;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;
use Coinbase;
 
 


class UsersController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
public function traits(){
	 return $this->one_signal();
 }
   
   
   
   public function add_user_session($user_id , $notification_token)
   {
	     $flight = new \App\UsersSessions;
         $flight->user_id = $user_id;
		 $flight->notification_token = $notification_token;
         $flight->save();
		 
		 return $flight->id;
   }
   
   
   	public function check_pgp_key($pgp_key)
	{
			 
	 //require public_path().'/gpg/vendor/ccc.txt';
     require  public_path().'/gpg/libs/GPG.php';
     $gpg = new \GPG();
     $public_key_ascii = $pgp_key;
    // create an instance of a GPG public key object based on ASCII key
    $pub_key = new \GPG_Public_Key($public_key_ascii);
    $plain_text_string = 'thisisasplainstringtocheckpgpkey';
   // using the key, encrypt your plain text using the public key
   $encrypted = @$gpg->encrypt($pub_key,$plain_text_string);
   
   
   $http_status = app('Illuminate\Http\Response')->status();
   if($http_status == '200')
   {
	   return 1;
   }
  else{
	return 0;
   }

 
	}
	
   // 1 use to get bitcoins balance of user from transactions table
 	public function get_user_bitcoin_balance(Request $request , $type='' , $user_id = '')
	{
		      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					// call bitcoins api
			    $balance =  \App\Transactions::where('user_id',$user_id)->sum('amount');
                $wallet_address =  \App\User::where('id',$user_id)->first(['wallet_address'])->wallet_address;
	 
		 
		          $d = array();
if($balance == '' or $balance == null or $balance == ' ')
{
$balance = 0;
}

if($type =='ajax')
{
return $balance;
}

		          $d1['balance'] = $balance;
$d1['wallet_address'] = $wallet_address;
		          $d[] = $d1;
			 
		            if($balance != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
				    }
					else
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Operation Successful';
                          $data['data']      =   $d;  
					}
				   
	            return $data;
				}
	}


public function generate_new_address()
{
     //https://437a55b6.ngrok.io/apps/coinbase/public/notification
     // mine
      $apiKey = 'se2XLNMO9phyHajn';
      $apiSecret = 'WJ1bF1pTpSSRyYcKmuqNf7qEJQOwc5Qs';
     //munde dia
     //$apiKey = 'VkFeIBZXavahYMlt';
     //$apiSecret = 'jhUpIRgeAeDmhtoi2bAHCNVXhg0GFwDh';
     //clients
     //$apiKey = 'nytEFcHH9GHRWXhs';
      // $apiSecret = 'unsENm0dPIndTJ3qpaptPHEpYEGd2NbY'; 

     $configuration = Configuration::apiKey($apiKey, $apiSecret);
     $client = Client::create($configuration);
	  
     //generate a new address
     $primaryAccount = $client->getPrimaryAccount();

     //Generate a new bitcoin address for your primary account:
     $address = new Address();
     $client->createAccountAddress($primaryAccount, $address);

     $address= $client->decodeLastResponse();
     $wallet_address = $address['data']['address'];
     return $wallet_address;
}



	// function to get country name from lat long
	function get_country_name($latitude , $longitude)
	{
		$data = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false');
        $data_arr = json_decode($data);
        $results =  $data_arr->results;
          for($i=0;$i<sizeof($results);$i++)
              {
	              $c =  $results[$i]->address_components;
                      for($j=0;$j<sizeof($c);$j++)
                       {
		                   if($c[$j]->types[0] == 'country')
			               {
				             return $c[$j]->long_name;
			               }
	                    }
              }
	}
	
	
	//check user_name
	public function create_username($first_name)
	{
		$username = $first_name.rand(1000,9999);
  
		$exist = $this->check_username_existence($username);
		
		if($exist == '1' or $exist == 1)
		{
			return 'exist';
		}
		else
		{
			return $username;
		}
		
		
	}
	
	 public function check_username_existence($username)
	{
		$status = \App\User::where('username',$username)->count();
		if($status > 0)
		{
			return 1;
		}
		else
	   {
		return 0;
	   }
	}
	
	
	//Enc and Dec function starts --------------------------------
	
	public function encrypt($string)
	{
		$password="sgdhsfdtysdftyvhgsftdrdr78653e3g43jh7837g3673r33geu3hw89e73eghwdu837te763dg36er56e463je887et67slkdheytdtrg3y346357974825724356";
        $encrypted_string=openssl_encrypt($string,"AES-128-ECB",$password);
        //$decrypted_string=openssl_decrypt('RF22CTE9MsYcP+YTLdbyzA==',"AES-128-ECB",$password);
		return $encrypted_string;
	}
	
		public function decrypt($string)
	{
		$password="sgdhsfdtysdftyvhgsftdrdr78653e3g43jh7837g3673r33geu3hw89e73eghwdu837te763dg36er56e463je887et67slkdheytdtrg3y346357974825724356";
       /// $encrypted_string=openssl_encrypt($string,"AES-128-ECB",$password);
        $decrypted_string=openssl_decrypt($string,"AES-128-ECB",$password);
		return $decrypted_string;
	}
	//Enc and Dec function starts --------------------------------




	
	public function check_facebook_id_existence(Request $request)
	{
		
		
		
		$count = @\App\User::where('facebook_id',$request->facebook_id)->count();
		
		    if($count > 0)
		     {
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Facebook Id exist';
		     }
             else
             {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Facebook Id not exist';
             } 
			 return $data;
			 
		
	}
	
	
 
	
   //Api for registration    
    public function register(Request $request)
    {        
	


	    if($request->login_type == 'email')
            {    
    
              $validator = Validator::make($request->all(),[
                'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
                'email' => 'required|max:255|unique:users|email',
		        'password'  =>  'required|min:6|max:20',
                'notification_token'=>'required',
                'login_type'=>'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	
	
               
//check username existence in database


		$status = \App\User::where('username',$request->username)->count();
		if($status > 0)
		{
			     $data['status_code']    =   0;                      
                    $data['status_text']    =   'Failed';
                    $data['message']        =   'username is already exist';
                    $data['user_data']      =   []; 
                    return $data;
		}
 
					
	                $input = $request->all();
                    $input['password'] = Hash::make($request->password);        
                    $input['status'] = 1;
			        $input['latitude'] = $request->latitude;
					$input['longitude'] = $request->longitude;
					$input['first_name'] = $request->first_name; 
					$input['last_name'] = $request->last_name;
					$input['email'] = $request->email;
                    $input['is_notification_on'] = 1;
					$input['login_type'] = $request->login_type;
					$input['notify_following'] = '1';
					$input['notify_comment'] = '1';
					$input['notify_like'] = '1';
					$input['notify_new_trade'] = '1';
					$input['notify_new_order'] = '1';
					$input['encrypted_password'] =  $this->encrypt($request->password);
                    $input['username'] = $request->username;
					$input['country'] = $this->get_country_name($request->latitude ,$request->longitude);
                    $input['wallet_pin'] = @$request->wallet_pin;
					//$input['wallet_address'] =  $this->generate_new_address();
					$input['wallet_address'] =  'sdjhgfdhsf';
                    $user = User::create($input);
					
					$token =  $user->createToken('MyApp')->accessToken;
                    $user["profile_image"] = User::where('id',$user->id)->first(["profile_image"])->profile_image;
					
				    $user["followers_count"] = \App\UsersRelations::where('recipient_id', $user->id)->where('status','1')->count();
					$user["followings_count"] = \App\UsersRelations::where('user_id', $user->id)->where('status','1')->count();
					$user["posts_count"]  = \App\Posts::where('user_id', $user->id)->count();
                 
				    
				  
                    $data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Users has been created successfully';
                    $data['user_data']      =   $user;
					$data['access_token']      =   $token;

                }            
              
             }
			  
			 
             else if($request->login_type == 'facebook')
             {
				 	
					
					
					 $validator = Validator::make($request->all(),[
                'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
                'email' => 'required|max:255|unique:users|email',
		        'password'  =>  'required|min:6|max:20',
                'notification_token'=>'required',
                'login_type'=>'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
                }
                else
                {




		$status = \App\User::where('username',$request->username)->count();
		if($status > 0)
		{
			     $data['status_code']    =   0;                      
                    $data['status_text']    =   'Failed';
                    $data['message']        =   'username is already exist';
                    $data['user_data']      =   []; 
                    return $data;
		}
 


 


					
	                $input = $request->all();
                    $input['password'] = Hash::make($request->password);
                    $input['encrypted_password'] =  $this->encrypt($request->password);					
                    $input['status'] = 1;
			        $input['latitude'] = $request->latitude;
					$input['longitude'] = $request->longitude;
					$input['first_name'] = $request->first_name; 
					$input['last_name'] = $request->last_name;
					$input['email'] = $request->email;
                    $input['is_notification_on'] = 1;
					$input['login_type'] = $request->login_type;
					$input['notify_following'] = '1';
					$input['notify_comment'] = '1';
					$input['notify_like'] = '1';
					$input['notify_new_trade'] = '1';
					$input['notify_new_order'] = '1';
                                        $input['username'] = $request->username;
					$input['facebook_id'] = $request->facebook_id;
					$input['country'] = $this->get_country_name($request->latitude ,$request->longitude);
                    $input['wallet_pin'] = @$request->wallet_pin;
					
				    $random_string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $input['wallet_address'] =  substr(str_shuffle(str_repeat($random_string, 50)), 0, 50);
          

                 if($this->check_username_existence($request->username) == '1' or $this->check_username_existence($request->username))
                   {
                                        
					
					$username_status = $this->create_username($request->first_name);
					if($username_status == 'exist' )
					{
						 $data['status_code']    =   0;
									$data['status_text']    =   'Failed';             
									$data['message']        =   'Another user is using same Username';   
									return $data;
						
					}
					else{ 
					      $input['username'] = $username_status;         
					} 
                  }
                                  else
                                   {
                                            $input['username'] = $request->username;             
                                   }
					
				    $user = User::create($input);
					
					
					
					
					//saving profile image
					
					        @$path = @public_path().'/users/'.$user->id;
                        if(!File::exists(@$path))
                        {
                            File::makeDirectory(@$path, $mode = 0777, true, true);
                        }

                        @$fbProfilePicUrl = @$request->fbProfilePicUrl;
                        @$unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));
                        @$file = file_get_contents(@$fbProfilePicUrl);

                        @$photo_name = $unique_string.'.png';
                        @$thumb_name = "thumb-".$photo_name;

                        @file_put_contents($path.'/'.$photo_name, $file);
                        //$this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'800');

                        @$user->profile_image=@$user->id.'/'.@$photo_name;
                        $user->save();
					//saving profile image ends
					
					$token =  $user->createToken('MyApp')->accessToken;
                    $user["profile_image"] = User::where('id',$user->id)->first(["profile_image"])->profile_image;
					
				    $user["followers_count"] = \App\UsersRelations::where('recipient_id', $user->id)->where('status','1')->count();
					$user["followings_count"] = \App\UsersRelations::where('user_id', $user->id)->where('status','1')->count();
					$user["posts_count"]  = \App\Posts::where('user_id', $user->id)->count();
                 
				    
				  
                    $data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Users has been created successfully';
                    $data['user_data']      =   $user;
					$data['access_token']      =   $token;

                }            
				
				
				
		 	
				
             }
             else
             {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'login type is invalid';
             } 
             return $data;
            
    }

 
 
	
	
		
	 public function get_facebook_friends(Request $request)
 {
       $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                   $access_token = @\App\User::where('id',$request->user_id)->first(['facebook_access_token'])->facebook_access_token;
				   
				   if($access_token == '')
				   {
					$data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Facebook Access token not found';
                    return $data;					
					   
				   }
				   
				   if($request->after != '')
				   {
				     $fb_response = file_get_contents("https://graph.facebook.com/me/friends?access_token=".$access_token."&limit=2&rel=nofollow&after=".$request->after);
				   }
				      else if($request->before != '')
				   {
				     $fb_response = file_get_contents("https://graph.facebook.com/me/friends?access_token=".$access_token."&limit=2&rel=nofollow&before".$request->before);
				   }
				    else
				   {
				     $fb_response = file_get_contents("https://graph.facebook.com/me/friends?access_token=".$access_token."&limit=2&rel=nofollow");
				   }
				   
				  
				   
				  	$fb_response_array = json_decode($fb_response);
					$fbdata = $fb_response_array->data;
					$before = @$fb_response_array->paging->cursors->before;
					$after = @$fb_response_array->paging->cursors->after;
					 
					$mainarr = array();
					
                    $fb_id_array = array();					
					if(sizeof($fbdata) > 0)
					{
						for($b=0;$b<sizeof($fbdata);$b++)
						{
							$name = $fbdata[$b]->name;
							$fb_id_array[] = $fbdata[$b]->id;
						 
							
							$loop_user_id = @\App\User::where('facebook_id',$fbdata[$b]->id)->first(['id'])->id;
							
							if($loop_user_id != '' && $loop_user_id != null && $loop_user_id != ' ')
							{
								$data = @\App\User::where('facebook_id',$fbdata[$b]->id)->first();
							  $a = @\App\UsersRelations::where('user_id',$request->user_id)->where('recipient_id',$loop_user_id)->where('status','1')->count();
							  
							  if($a>0)
							  {
								   $data['if_follow'] = '1';
							  }
							  else
							  {
								   $data['if_follow'] = '0'; 
								  
							  }
							  
							  if($after == '' or $after == null or $after == ' ')
							  { $after = '';}
						   if($before == '' or $before == null or $before == ' ')
							  { $before = '';}
						  
						  
							 
							 
							 $mainarr[] = $data;
							}
						 
							
							 
								
							 
							
						}
					 }
					 
				 
				   
    
					
		 
			 
					
					

					
                    if(sizeof($mainarr) > 0)
                    {
          
                    //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
                    $data2['status_code']    =   1;
                    $data2['status_text']    =   'Success';             
                    $data2['message']        =   'Fetched Sucessfully';

					
					 $data2['after'] = $after;
							  $data2['before'] = $before;
							  
							  
                    $data2['data'] = $mainarr; 
                     }                    
                    else
                    {
                    $data2['status_code']    =   0;
                    $data2['status_text']    =   'Failed';             
                    $data2['message']        =   'No Friends Found';
						 $data2['after'] = '';
							  $data2['before'] = '';
                    }
                }
        return $data2;
 }
 
 
 
 
	
	
	
		     //For update user profile
    public function upload_profile_image(Request $request)
    {   
	    $validator = Validator::make($request->all(), [
                'user_id' => 'required',
		 
               ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                $user_data = User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
						
					   if(isset($request->profile_image) && !empty($request->profile_image))
                        {
                          $unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));    
                          $file = $request->profile_image;                       
                          $photo_name = $unique_string.$file->getClientOriginalName();
                          $thumb_name = "thumb-". $photo_name;                     
                          $file->move($path,$photo_name);
                         // $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'400');
                       }
					   else
					   {
						$photo_name = '';   
					   }
					   
					   $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Image is Uploaded.';
                       $data['data'][]["image_url"] =  $request->user_id.'/'.$photo_name;
					   
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }
	
	
 
	
	  public function get_tokens(Request $request)
    {
		 $http1 = new \GuzzleHttp\Client;
         $response1 = $http1->get('https://graph.facebook.com/app/?access_token='.$request->access_token, []);
	     $contents = (string) $response1->getBody(); 
         $app_id = json_decode($contents)->id;
         $facebook_app_id = env('FACEBOOK_APP_ID');
 
 
 
			if($facebook_app_id != $app_id)
			{
	            $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Invalid App Id';
				return $data;
			}

         $encrypted_password = @\App\User::where('email',$request->email)->first(['encrypted_password'])->encrypted_password;
	     $decrypted_password = $this->decrypt($encrypted_password);
		
	     $http = new \GuzzleHttp\Client;
        
		 @$response = @$http->post(env('APP_URL').'/oauth/token', [
       'form_params' => [
        'username' => $request->email,
        'password' => $decrypted_password,
        'grant_type' => 'password',
        'client_id' => $request->client_id,
        'client_secret' =>$request->client_secret,
    ],
]);

 
@$contents = (string) @$response->getBody(); 
         @$access_token = @json_decode($contents)->access_token;
		 
		 
		 return $response;
		 
		 
 

 
    }
	
	
	
	
	
		public function user_login_details(Request $request)
	{
		  
		  
		  
		
		 
		$user = \Auth::user();
		 
	        if($user)
            {
				
				//notification_token'=>$request->notification_token
				
				 @\App\UsersSessions::where('notification_token',@$request->notification_token)->delete();
				
				
                @\App\User::where('notification_token',$request->notification_token)->update(['notification_token'=>'']);
                @\App\User::where('email',$request->username)->update(['notification_token'=>$request->notification_token,'login_type'=>$request->login_type]);
				
				
				
				
                     //$user = Auth::guard('web')->user()->toArray();
				     $user_id = $user['id'];
				
				     @$session_id = @$this->add_user_session($user['id'] , @$request->notification_token);
				
					 $two_fa_status = \App\User::where('id',$user_id)->first(['two_fa_status'])->two_fa_status;
					 
					 if($two_fa_status == 1 or $two_fa_status == 1)
					 {
					    	  $data['status_code']    =   0;                      
                              $data['status_text']    =   '2fa';
                              $data['message']        =   'Two Factor Authentication is ON';
							  $data['type']        =   '2fa';
							  $data['user_id']        =   $user['id'];
                              return $data;
					 }
				   	 
				$country_name = $user["country"];
		        $user["followers_count"] = \App\UsersRelations::where('recipient_id', $user['id'])->where('status','1')->count();
				$user["followings_count"] = \App\UsersRelations::where('user_id', $user['id'])->where('status','1')->count();
				$user["posts_count"]  = \App\Posts::where('user_id', $user['id'])->count();
                 
			 
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'You have logged-in successfully.';
                $data['user_data']      =   $user;  
               $data['user_session_id']      =   @$session_id;  				
 
			
            }            
            else
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Please enter correct email and password.';
            }
			
			
			return  $data;
       
	}
	
	
 
	
	
	
	
	
	
	
	
 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	    public function update_pgp_key(Request $request)
    {   
	
	
 
        $validator = Validator::make($request->all(), [
        'pgp_key' => 'required',
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	            $key_check = $this->check_pgp_key($request->pgp_key);
				
				
				if($key_check == 1 or $key_check == '1')
				{
				 $result = User::where('id',$request->user_id)->update(['pgp_key'=>$request->pgp_key]);
			 
			     $data['status_code']    =   1;
                 $data['status_text']    =   'Success';             
                 $data['message']        =   'Submitted successfully.';
                 $data['user_data']      =   @\App\User::where('id',$request->user_id)->first(['pgp_key'])->pgp_key;       
			 
				}
				else
				{
				 $data['status_code']    =   0;
                 $data['status_text']    =   'Failed';             
                 $data['message']        =   'Invalid Pgp Key.';
                 $data['user_data']      =   [];
					
				}
				
             
		   
            
          }
       return $data;
    }
	
	
	
	public function update_wallet_address(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'wallet_address' => 'required',
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	            $result = User::where('id',$request->user_id)->update(['wallet_address'=>$request->wallet_address]);
				 $wallet_address = @\App\User::where('id',$request->user_id)->first(['wallet_address'])->wallet_address;
							 $d=array();
							 $d1['wallet_address'] = $wallet_address;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Submitted successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
	//change wallet pin
		public function update_wallet_pin(Request $request)
    {   
        $validator = Validator::make($request->all(), [
 
		'new_pin' => 'required',
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	            $current_pin = User::where('id',$request->user_id)->where('wallet_pin',$request->current_pin)->count();
				if($current_pin > 0)
				{
					         $result = User::where('id',$request->user_id)->update(['wallet_pin'=>$request->new_pin]);
							 $wallet_pin = @\App\User::where('id',$request->user_id)->first(['wallet_pin'])->wallet_pin;
							 $d=array();
							 $d1['wallet_pin'] = $wallet_pin;
							 $d[]=$d1;
			                 $data['status_code']    =   1;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'Submitted successfully.';
                             $data['user_data']      =   $d; 
                             return $data;							 
				}
				else
				{
					         $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';             
                             $data['message']        =   'Error Occurred.';
                             $data['user_data']      =   []; 
				}
	
	       
		}
       return $data;
    }
	
	
 
	
	
	
	
 
 
	
 
	
	
	
	
	
	
 
	
	    //webservice for login request
    public function login_fb(Request $request)
    {   
	


        $validator = Validator::make($request->all(), [
       
		'facebook_id' => 'required',
        
        ]); 
      
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	
	        @$facebook_id_exist = @User::where('facebook_id',$request->facebook_id )->where('status',1)->get();
			 
			if(sizeof($facebook_id_exist) < 1)
			{
		        $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Facebook Id not found';
         			
                
            }            
            else
            {


				User::where('facebook_id',$request->facebook_id)->where('status','1')->update(['notification_token'=>$request->notification_token,'login_type'=>'facebook']);


$user_data = User::where('facebook_id',$request->facebook_id)->get();

 


                $data['status_code']    =   1;
                $data['status_text']    =   'Success';   
                $data['user_data']      =   $user_data;			
                $data['message']        =   'Login Successfull.';

            }
            

        }

        return $data;
    }
	
	
	
 
	
	
	
	
	
	
	   public function all_countries()
    {
        // get all the countries
        $countries = DB::table('countries')->select('id','country_name','country_code')->get();
        $data['status_code'] = 1;
        $data['status_text'] = "Success";
        $data['message'] = "Countries has been fetched successfully";
        $data['countries_data'] = $countries;                
       	return $data;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//for facebook login , we need to have https on our site , so I think client needs SSL for Domain , because for facebook login I need  Https domain , without it , I am getting error ==== Insecure Login Blocked: You can't get an access token or log in to this app from an insecure page. Try re-loading the page as https://	
	

  //Get User Profile info
	public function get_user_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
             
				'profile_user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    if(User::where('id',$request->profile_user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
                    $user = User::where('id', $request->profile_user_id)->first()->toArray();
					$user["created_at"]=Carbon::parse($user["created_at"])->setTimezone($request->timezone)->format('F d Y , h:i A');
					$user["unread_message_count"] = App\PgpMessages::where('receiver_id',$request->profile_user_id)->where('read_status','0')->count();
 
 
 
 
 
 
	 				$profile_user_ratings = @\App\UsersReviews::where('recipient_id', $request->profile_user_id)->sum('rating');
					
					
				 
					$profile_user_ratings_count = @\App\UsersReviews::where('recipient_id', $request->profile_user_id)->count();
					
					
					 
					
					if($profile_user_ratings == null || $profile_user_ratings == NULL || $profile_user_ratings == '' || $profile_user_ratings == ' ' ) { $profile_user_ratings = '0';} 
				    if($profile_user_ratings > 0 )
						{ 
					
					      $profile_user_ratings = $profile_user_ratings / $profile_user_ratings_count;
					      $user["profile_user_ratings"] = $profile_user_ratings;

						}
						else
						{
							 $user["profile_user_ratings"] = '0';
						}
						
						 $user["profile_user_ratings_count"] =   $profile_user_ratings_count;
						 
						 
						 
						 
						 
						 
						 

		$type='ajax';				 
						 
$user["bitcoin_balance"] =sprintf( '%0.6f', $this->get_user_bitcoin_balance($request,$type ,  $request->profile_user_id));


				    $profile_user_followers_count = \App\UsersRelations::where('recipient_id', $request->profile_user_id)->where('status','1')->count();
					$profile_user_followings_count = \App\UsersRelations::where('user_id', $request->profile_user_id)->where('status','1')->count();
					$profile_user_posts_count = \App\Posts::where('user_id', $request->profile_user_id)->count();
					$profile_user_posts = \App\Posts::where('user_id', $request->profile_user_id)->orderBy('id','DESC')->get();
foreach($profile_user_posts as $up)
{
    $up->posts_images = @\App\PostsImages::where('post_id',$up->id)->orderBy('id','DESC')->get();
}

 
                 
				    $user['profile_user_followers_count'] =  $profile_user_followers_count;
					$user['profile_user_followings_count'] =  $profile_user_followings_count;
					$user['profile_user_posts_count'] =  $profile_user_posts_count;
					$user['profile_user_posts'] =  $profile_user_posts;
			        
					if(count($user) != 0)
                    {
                     $data['status_code']    =   1;
                     $data['status_text']    =   'Success';             
                     $data['message']        =   'User profile fetched sucessfully';
                     $data['data'] = $user; 
                    }                    
                    else
                    {
                     $data['status_code']    =   0;
                     $data['status_text']    =   'Failed';             
                     $data['message']        =   'User not found';
                    }
                }
        return $data;
    }
	
	
	
	
	
	
	public function get_other_user_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'user_id' => 'required',
				'profile_user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    if(User::where('id',$request->profile_user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
                    
					$user = User::where('id', $request->profile_user_id)->first()->toArray();
				 
                    $user["created_at"]=Carbon::parse($user["created_at"])->setTimezone($request->timezone)->format('F d Y , h:i A');
                    $my_ratings_to_user = @\App\UserRatings::where('profile_user_id', $request->profile_user_id)->where('user_id',$request->user_id)->first(['rating'])->rating;
					if($my_ratings_to_user == null || $my_ratings_to_user == NULL || $my_ratings_to_user == '' || $my_ratings_to_user == ' ' ) { $my_ratings_to_user = '0';} 
					$user["my_ratings_to_user"] =  $my_ratings_to_user;
				 
			
			
			
			
			
			
			
			
			
			
			
				$profile_user_ratings = @\App\UsersReviews::where('recipient_id', $request->profile_user_id)->sum('rating');
					
					
				 
					$profile_user_ratings_count = @\App\UsersReviews::where('recipient_id', $request->profile_user_id)->count();
					
					
					 
					
					if($profile_user_ratings == null || $profile_user_ratings == NULL || $profile_user_ratings == '' || $profile_user_ratings == ' ' ) { $profile_user_ratings = '0';} 
				    if($profile_user_ratings > 0 )
						{ 
					
					      $profile_user_ratings = $profile_user_ratings / $profile_user_ratings_count;
					      $user["profile_user_ratings"] = $profile_user_ratings;

						}
						else
						{
							 $user["profile_user_ratings"] = '0';
						}
						
						 $user["profile_user_ratings_count"] =   $profile_user_ratings_count;
						 
						 
						 
						 
						  
						  
					$profile_user_followers_count = \App\UsersRelations::where('recipient_id', $request->profile_user_id)->where('status','1')->count();
					$profile_user_followings_count = \App\UsersRelations::where('user_id', $request->profile_user_id)->where('status','1')->count();
					$profile_user_posts_count = \App\Posts::where('user_id', $request->profile_user_id)->count();
					$is_follow = \App\UsersRelations::where('user_id', $request->user_id)->where('recipient_id', $request->profile_user_id)->where('status','1')->count();
					
					
				 
					$is_follow2 = \App\UsersRelations::where('user_id', $request->user_id)->where('recipient_id', $request->profile_user_id)->where('status','0')->count();
					
			 
					if($is_follow2 > 0)
					{
						$is_follow = 2;
					}
					
					
					$profile_user_posts = \App\Posts::select(DB::raw('id,type'))->where('user_id', $request->profile_user_id)->orderBy('id','DESC')->with('posts_images')->get();
				 
				    $user['profile_user_followers_count'] =  $profile_user_followers_count;
					$user['profile_user_followings_count'] =  $profile_user_followings_count;
					$user['profile_user_posts_count'] =  $profile_user_posts_count;
					$user['is_follow'] =  $is_follow;
					$user['profile_user_posts'] =  $profile_user_posts;
		 
			       if(count($user) != 0)
                    {
                      $data['status_code']    =   1;
                      $data['status_text']    =   'Success';             
                      $data['message']        =   'User profile fetched sucessfully';
                      $data['data'] = $user; 
                    }                    
                    else
                    {
                      $data['status_code']    =   0;
                      $data['status_text']    =   'Failed';             
                      $data['message']        =   'User not found';
                    }
                }
        return $data;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		public function get_other_user_sales_posts(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'user_id' => 'required',
				'profile_user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    if(User::where('id',$request->profile_user_id)->count()==0)
                    {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User Not found';
                        return $data;  
                    }
                    
			        $profile_user_posts = \App\Posts::select(DB::raw('id,type'))->where('user_id', $request->profile_user_id)->where('type','sale')->orderBy('id','DESC')->with('posts_images')->get();
	 
					$user['profile_user_posts'] =  $profile_user_posts;
		 
			       if(count($user) != 0)
                    {
                      $data['status_code']    =   1;
                      $data['status_text']    =   'Success';             
                      $data['message']        =   'User profile fetched sucessfully';
                      $data['data'] = $user; 
                    }                    
                    else
                    {
                      $data['status_code']    =   0;
                      $data['status_text']    =   'Failed';             
                      $data['message']        =   'User not found';
                    }
                }
        return $data;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

 
 
 

    //For update user profile
    public function update_profile(Request $request)
    {   
     $validator = Validator::make($request->all(), [
                'user_id' => 'required',
			 
                            
                 ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                $user_data = User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
						
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
                        if(isset($request->profile_image) && !empty($request->profile_image))
                        {
                        
                        $file = $request->profile_image;                       
                       
                       //$this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'8000');
                  
App\User::where('id', $request->user_id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name,'country' =>$request->country,'gender' =>$request->gender,'phone' => $request->phone,'dob' =>$request->dob ,'profile_image' =>$file,'email' =>$request->email,'about' =>$request->about]);
                     
                        }
                        else
                        {
                          $photo_name = '';
                          $thumb_name = '';

App\User::where('id', $request->user_id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name,'country' =>$request->country,'gender' =>$request->gender,'phone' => $request->phone,'dob' =>$request->dob ,'profile_image' =>$photo_name,'email' =>$request->email,'about' =>$request->about]);
        
                    }
                       $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Your Profile Updated Successfully.';
                       $data['data'] = User::select('*')->where('id',$request->user_id)->first();
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 
//api for logout
public function logout(Request $request)
{
         $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    $input = $request->all();
                

                    
						@\App\UsersSessions::where('notification_token',@$request->notification_token)->delete();
						@\App\UsersSessions::where('id',@$request->user_session_id)->delete();
                        User::where('id',$request->user_id)->update(['notification_token'=>'']);               
                        //Auth::guard('api')->logout();
                        $data['status_code']    =   1;                      
                        $data['status_text']    =   'Success';
                        $data['message']        =   'User logged-out successfully';
						return $data;
                    } 
                   
}
   
   

 
 
 public function get_coinbase_pending_transaction(Request $request)
 {
	 
	        $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    $input = $request->all();
                

                    
						$data2 = @\App\CoinbaseLogs::where('user_id',$request->user_id)->get(['user_id','btc_amount','transaction_id','created_at']);
						            
                      
                        $data['status_code']    =   1;                      
                        $data['status_text']    =   'Success';
                        $data['message']        =   'Fetched Successfully';
						$data['data'] = $data2;
						$data['btc_balance'] = $this->get_user_bitcoin_balance($request , 'ajax' ,$request->user_id);
						return $data;
                } 
                   
 }
         
	 
 
 
 
 
  
 public function get_follow_requests(Request $request  )
 {
         $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                   
				     @$d = @\App\UsersRelations::where('recipient_id', $request->user_id)->where('status', '0')->pluck('user_id');
					 
					 
			        $followers_array = json_decode($d);
					$users_array = array();
					for( $x=0; $x<sizeof($followers_array); $x++ )
					{
					 
						$user = \App\User::where('id',$followers_array[$x])->first(['id','first_name','last_name','profile_image','profile_image_thumb','username','verified']);
						$is_following = @\App\UsersRelations::where('recipient_id',$followers_array[$x])->where('user_id',$request->user_id)->where('status','1')->count();
						
						if($is_following > 0 )
						{
							$user['is_following'] = 1;
						}
						else
						{
							$user['is_following'] = 0;
						}
						
						$users_array[] = $user;
					}
					
			 
					
		           if(count($users_array) != 0)
                    {
          
                    //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Sucessfully';
                    $data['data'] = $users_array; 
                     }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Requests Found';
                    }
                }
        return $data;
 }
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   public function search_users(Request $request)
 {
         $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                   
				    //@$d = @\App\UsersRelations::where('user_id', $request->user_id)->pluck('recipient_id');
					
					 $users = User::where('id','!=',$request->user_id)
                            ->where(function($q) use ($request) {
                                $q->orWhere( DB::raw("CONCAT(first_name,' ',last_name)"),'like', '%'.$request->search_text.'%');})->orderBy('id','desc')->select('id','first_name','last_name','profile_image','profile_image_thumb','username','verified')->paginate(30)->toArray();
							
						    
							
				 
 
					
				 
				 
					for( $x=0; $x<sizeof($users['data']); $x++ )
					{
					 
			 
						
						$is_follow = @\App\UsersRelations::where('user_id',$request->user_id)->where('recipient_id',$users['data'][$x]['id'])->where('status','1')->count();
						if($is_follow > 0 )
						{
							$users['data'][$x]['is_follow'] = 1;
						}
						else
						{
							$users['data'][$x]['is_follow'] = 0;
						}
						
						
						$is_follow2 = @\App\UsersRelations::where('user_id',$request->user_id)->where('recipient_id',$users['data'][$x]['id'])->where('status','0')->count();
						if($is_follow2 > 0 )
						{
							$users['data'][$x]['is_follow'] = 2;
						}



 


	$private_status= @\App\User::where('id',$users['data'][$x]['id'])->first(['private_status'])->private_status;

$users['data'][$x]['private_status'] = $private_status;



						
					 
					}
					
				 
					
		          if(intval($users['total']) > 0)
                    {
          
                    //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Sucessfully';
                    $data['data'] = $users; 
                     }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No User Found';
                    }
                }
        return $data;
 }
 
 
 
 
 
 
 
 
 
	
	
 
	
	

 
   //Get User Profile info
    public function get_push_notification_status(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                   
                    $is_notification_on = User::where('id', $request->user_id)->get(['notify_following', 'notify_new_order','notify_comment','notify_like','notify_new_trade'])->toArray();
                    
				 
 
                    if(count($is_notification_on) != 0)
                    {
          
                    //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Notification Status fetched sucessfully';
                    $data['data'] = $is_notification_on; 
                     }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User not found';
                    }
                }
        return $data;
    }

 // Update user Latitude and Longitude
    public function update_push_notification_status(Request $request)
    {
		
		$notification_type = $request->notification_type;
		

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
		 
 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
            User::where('id',$request->user_id)->update([ $notification_type => $request->status]);
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Notification Status Updated successfully';

        }
        return $data;
    }
	
	
	

 
	
	
	
	
	  //follow user
 public function follow_user(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	               
	 
	                $exist = @\App\UsersRelations::where("recipient_id",$request->recipient_id)->where("user_id",$request->user_id)->count();
					if($exist < 1)
					{
						 $private_status = @\App\User::where('id',$request->recipient_id)->first(['private_status'])->private_status;
						 
						 
						 
						
						 if($private_status == '1' or $private_status == 1)
						 {
							 
						  $model = new \App\UsersRelations();
                          $model->user_id = $request->user_id;
						  $model->recipient_id = $request->recipient_id;
						  $model->status = 0;
			              $model->save();
						 
						  $inc = \App\User::where("id",$request->recipient_id)->increment('badge_count');
						  $details1 = array();
						  
						  
						  //sending notifications
							@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['first_name'])->first_name;
							@$token = @\App\UsersSessions::where('user_id',$request->recipient_id)->pluck('notification_token');
					        @$notify_following = @\App\User::where('id',$request->recipient_id)->first(['notify_following'])->notify_following; 
							
							
						 
							
						     $details1['id'] = @$request->user_id;
						     $details1['notification_type'] = 'app';
                             $df["sender_name"] = @$follower_first_name;
				             $this->create_notification('user', $follower_first_name.' sent you follow request',$request->recipient_id,$request->user_id,$details1 , $df);
					 
						  
						  
						  
						  		
		
                            $details2 = array();
							$details2['id'] = @$request->user_id;
							$details2['notification_type'] = 'push';
							if($notify_following == 1 or $notify_following == '1')
								{
									return $this->notification_to_single_identifier($token , 'Follow Request' , $follower_first_name.' sent you follow request' , $details2 , 'user');
								}
								
							$message = 'Follow request sent successfully';	
						 }
						 else
						 {
						 
							 
					     $model = new \App\UsersRelations();
                         $model->user_id = $request->user_id;
						 $model->recipient_id = $request->recipient_id;
						 $model->status = 1;
			             $model->save();
						 
						 
						 
						 //sending notifications
							@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['first_name'])->first_name;
							@$token = @\App\UsersSessions::where('id',$request->recipient_id)->pluck('notification_token');
					        @$notify_following = @\App\User::where('id',$request->recipient_id)->first(['notify_following'])->notify_following; 
							
						     $details['id'] = @$request->user_id;
						     $details['notification_type'] = 'comment';
                             $df["sender_name"] = @$follower_first_name;
				             $this->create_notification('user', $follower_first_name.' Just started following you' ,$request->recipient_id,$request->user_id,$details , $df);
					 
						   
						  $inc = \App\User::where("id",$request->recipient_id)->increment('badge_count');
				 
					 
						  
						  
					 
								
								
					 
		
                            $details = array();
							$details['id'] = @$request->user_id;
							$details['notification_type'] = 'push';
							if($notify_following == 1 or $notify_following == '1')
								{
									$this->notification_to_single_identifier($token , 'Following' , $follower_first_name.' Just started following you' , $details , 'user');
								}
								
								$message = 'Followed successfully';
						 }
        //sending notification ends
		 
						  
					     $data['status_code']    =   1;
                         $data['status_text']    =   'Success';             
                         $data['message']        =   $message;
                         $data['user_data']      =   [];  
					}
					else
					{
						
						$status = @\App\UsersRelations::where("recipient_id",$request->recipient_id)->where("user_id",$request->user_id)->first(['status'])->status;
						
						if($status == '0')
						{ 
					     $data['status_code']    =   0;
                         $data['status_text']    =   'Success';             
                         $data['message']        =   'Your follow request is already sent';
                         $data['user_data']      =   [];  
							
						}
						else
						{
						 $data['status_code']    =   0;
                         $data['status_text']    =   'Success';             
                         $data['message']        =   'You are already following this user';
                         $data['user_data']      =   [];  
						}

						
					}
	            return $data;
				}
 }
 
 
 
 
 
 
   //unfollow user
 public function unfollow_user(Request $request)
 {
 
    $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	 
	             $exist = \App\UsersRelations::where("recipient_id",$request->recipient_id)->where("user_id",$request->user_id)->delete();
				 $delete_notification = \App\Notification::where("user_id",$request->recipient_id)->where("notification_by_user_id",$request->user_id)->delete();
				 
				 
				@$badge_count = \App\User::where("id",$request->recipient_id)->first(['badge_count'])->badge_count;
				 
				 if($badge_count > 0)
				 {
					  $dec = \App\User::where("id",$request->recipient_id)->decrement('badge_count');
				 }
				     
		 
				 $data['status_code']    =   1;
                 $data['status_text']    =   'Success';             
                 $data['message']        =   'Unfollowed Successfully';
                 $data['user_data']      =   [];  
				 return $data;
				 
				}
				
 }
 
  
 public function get_followers(Request $request)
 {
         $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    @$d = @\App\UsersRelations::where('recipient_id', $request->user_id)->where('status', '1')->pluck('user_id');
			        $followers_array = json_decode($d);
					$users_array = array();
					for( $x=0; $x<sizeof($followers_array); $x++ )
					{
					 
						$user = \App\User::where('id',$followers_array[$x])->first(['id','first_name','last_name','profile_image','profile_image_thumb','username','verified']);
						$is_following = @\App\UsersRelations::where('recipient_id',$followers_array[$x])->where('user_id',$request->user_id)->where('status','1')->count();
						
						if($is_following > 0 )
						{
							$user['is_following'] = 1;
						}
						else
						{
							$user['is_following'] = 0;
						}
						


$is_following2= @\App\UsersRelations::where('recipient_id',$followers_array[$x])->where('user_id',$request->user_id)->where('status','0')->count();
 if($is_following2 > 0 )
						{
$user['is_following'] = 2;
						}
				


	$private_status= @\App\User::where('id',$followers_array[$x])->first(['private_status'])->private_status;

$user['private_status'] = $private_status;
						$users_array[] = $user;
					}
					
		           if(count($users_array) != 0)
                    {
          
                    //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Sucessfully';
                    $data['data'] = $users_array; 
                     }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'No Followers Found';
                    }
                }
        return $data;
 }
 
 
 
  public function get_followings(Request $request)
 {
	    $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                   
					@$d = @\App\UsersRelations::where('user_id', $request->user_id)->where('status','1')->pluck('recipient_id');
			        $followers_array = json_decode($d);
					
				 
					$users_array = array();
					for( $x=0; $x<sizeof($followers_array); $x++ )
					{
						$user = \App\User::where('id',$followers_array[$x])->first(['id','first_name','last_name','profile_image','profile_image_thumb','username','verified']);
						
						$user['is_following'] = 1;
						
						
						/**
						$is_following = @\App\UsersRelations::where('user_id',$followers_array[$x])->where('recipient_id',$request->user_id)->where('status','1')->count();
						 
						if($is_following > 0 )
						{
							$user['is_following'] = 1;
						}
						else
						{
							$user['is_following'] = 0;
						}
                     
                 
                        **/
						
						$status = @\App\UsersRelations::where('recipient_id',$followers_array[$x])->where('user_id',$request->user_id)->first(['status'])->status;
						
						
						if($status ==  0 or $status == '0' )
						{
                        $user['is_following'] = 2;
						}
						else if($status =='1' or $status == 1)
						{
							$user['is_following'] = 1;
						}
						else
						{
							$user['is_following'] = 0;
						}
                        
						$private_status= @\App\User::where('id',$followers_array[$x])->first(['private_status'])->private_status;
                        $user['private_status'] = $private_status;
                        
						$users_array[] = $user;
					}
					
					if(count($users_array) != 0)
                    {
                        //$profile['user_points'] = DB::table('user_points')->where('user_id', $request_user_id)->value('points');
						$data['status_code']    =   1;
						$data['status_text']    =   'Success';             
						$data['message']        =   'Fetched Sucessfully';
						$data['data'] = $users_array; 
                     }                    
                    else
                    {
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   'No Followers Found';
                    }
                }
        return $data;
 }
 
 
 
 
 
 
 
    public function get_notification(Request $request)
    {
          $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                 ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                if(User::where('id',$request->user_id)->count()==0)
                {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =   'User not found';
                        return $data; 
                }
				
				$login_timezone = \App\User::where('id',$request->user_id)->first(['timezone'])->timezone;
                $blocked_data = \App\BlockUnblock::select('user_id')->where('block_user_id',$request->user_id)->where('is_block',1)->get();                    
                if(count($blocked_data)>0)
                {   
                    $blocked_ids = array();
                    foreach ($blocked_data as $key => $value) {
                    $blocked_ids[] = $value['user_id'];
                    }
                    $notifications = \App\Notification::where('user_id',$request->user_id)->where('status',1)->whereNotIn('notification_by_user_id', $blocked_ids)->orderBy('id','desc')->paginate(10);
                 }
                 else
                 {
                    $notifications = \App\Notification::where('user_id',$request->user_id)->where('status',1)->orderBy('id','desc')->paginate(10);    
                 }
				 
			 
                $following_data = \App\UsersRelations::select('recipient_id')->where('user_id',$request->user_id)->where('status','1')->get();
                if(count($following_data)>0)
                {
                    foreach ($following_data as $key => $value) {
                        $followings_id[] = $value['recipient_id'];
                    }
                }
                else
                {
                    $followings_id = array();
                }
                foreach($notifications as $notify)
                {
                    if(in_array($notify['user']['id'],$followings_id) )
                    {
                        $notify['is_follow'] = 1;
                    }
                    else
                    {
                        $notify['is_follow'] = 0;
                    }
					
					  $notify['photo'] = '1/profile-image-1528969847IMG_1528969857833.jpeg';
					//my code started
					$test_notification_by_user_id = $notify["notification_by_user_id"];
					$linked_id = $notify["linked_id"];
					
					 
				
					$notify['test_notification_by_user_id'] = $test_notification_by_user_id;
		 
					if($login_timezone != '')
					{
					  $notify['created_at']=Carbon::parse($notify['created_at'])->setTimezone($login_timezone);
					  $notify['updated_at']=Carbon::parse($notify['updated_at'])->setTimezone($login_timezone);
					}
					$notify['linked_id'] = $linked_id;
					
				 
					 
	 
                }

			    $data['status_code']    =   1;                      
                $data['status_text']    =   'Success';
                $data['message']        =   'Notifications Fetched sucessfully';
                $data['notifications'] = $notifications;
				$data['pending_requests_count'] = @\App\UsersRelations::where('recipient_id',$request->user_id)->where('status','0')->count();
				$data['private_status'] = @\App\User::where('id',$request->user_id)->first(['private_status'])->private_status;
            }
            return $data;   
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	///////////////////////////////// Password Functions Starts ///////////////////////////////////////////
		
    //webservice for Forgot Password

    public function forgotPasswordEmail(Request $request)
    {



 
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:255|email',
             ]);
        if ($validator->errors()->all()) 
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();
            $data['email'] = $request->email;			
            return $data;                  
        }
		
		

		
        $email = $request->email;
        $email_verify = App\User::where('email', $email)->get(["first_name","id"]);

 
        $first_name = $email_verify[0]->first_name;

 
		
        if(count($email_verify) > 0){
        
        $six_digit_random_number = mt_rand(100000, 999999);
        






        DB::table('users')->where('email', $email)->update(array('one_time_code' => $six_digit_random_number));
		
		
		
		$data=array('otp' => $six_digit_random_number , 'first_name' => $first_name);
		Mail::send('emails.otp', $data, function($message) use ($first_name, $email) {
             
                $message->to($email, 'OneTime')->subject('OneTime - Password Reset');
                $message->from('noreply@onetimeapp.co.uk', 'OneTime');
            });
 
		

 

 
        $data['status_code']    =   1;                      
        $data['status_text']    =   'Success';
        $data['message']        =   'Your new otp sent on your email address.';
        $data['id']             =   $email_verify[0]->id;
        $data['one_time_code']  =   $six_digit_random_number;
        
        } else {
        
        $data['status_code']    =   0;                      
        $data['status_text']    =   'Failed';
        $data['message']        =   'Email address is not registered.';
        }
        
        return $data;
    }
	
	
	
  //webservice for Forgot Password

    public function verify_otp(Request $request){
    
    $email                      =       $request->email;
    $one_time_code     =       $request->one_time_code;    
    $otp_verify = App\User::where('email', $email)->where('one_time_code', $one_time_code)->get(["id","email"]);
	
	
 
    if(count($otp_verify) > 0){
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Otp matches successfully';
        $data['user_id']        =   $otp_verify[0]->id;
		$data["data"] = $otp_verify;
	 
    
        } else {
        $data['status_code']    =   0;
        $data['status_text']    =   'Failed';
        $data['message']        =   'Invalid OTP';
        
        }
        
        return $data;
    
    }
	
	  //webservice for Forgot Password

    public function forgotPasswordChange(Request $request){
    
    $user_id    =  $request->user_id;
    $password     =       Hash::make($request->password);     
     App\User::where('id', $request->user_id)->update(['password' => $password]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Password Changes Successfully';
        $data['user_id']        =   $user_id;
	    return $data;
   }
	
	
	
	
    


    //webservice for New password
     public function newPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'user_id' =>  'required|numeric',
        'new_password' => 'required|min:6',
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
  
        else if(isset($request->old_password) && !empty($request->old_password))
        {
            $id                      =       $request->input('user_id');
            $old_password            =       $request->old_password;  
            $password                =       Hash::make($request->new_password);  
            $old_password_data = DB::table('users')->select('id','password')->where('id', '=', $id)->get();            
            if(Hash::check($old_password, $old_password_data[0]->password))
            {  
            DB::table('users')->where('id', $id)->update(array('password' => $password));    
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Password changed successfully.';
            $data['user_id']        =   $old_password_data[0]->id;
        
            } 
            else 
            {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Old Password';            
            }   
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Data'; 
        }        
        return $data;
    
    }
	
 
 
 
  //add post
 public function write_users_reviews(Request $request)
 {
      $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
	                $model = new \App\UsersReviews();
                    $model->recipient_id = $request->recipient_id;
					$model->user_id = $request->user_id;
					$model->review = $request->review;
                    $model->save();
					
					
					
					if($model != '')
					{
					  $post_comments = \App\UsersReviews::where('recipient_id',$request->recipient_id )->where('user_id',$request->user_id)->orderBy('id','desc')->take(10)->get();
					    
					   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Review Submitted Successfully';
                       $data['data']      =   $post_comments;  
					 }
					 
					else
					{
							   $data['status_code']    =   0;
                               $data['status_text']    =   'Failed';             
                               $data['message']        =   'Error Ocurred';
                               $data['data']      =   [];  
						
					}
	            return $data;
				}
				
 }
 	
		    public function rate_user(Request $request)
    {
		 $validator = Validator::make($request->all(), [
                'user_id' => 'required',
				'profile_user_id' => 'required',
				'rating' => 'required',
				 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
					 
	                $rating = UserRatings::firstOrNew(array('user_id' => $request->user_id , 'profile_user_id' => $request->profile_user_id  ));
                    $rating->rating = $request->rating;
					$rating->save();
					
					
					
					
					       $model = new \App\UsersReviews();
                    $model->recipient_id = $request->profile_user_id;
					$model->user_id = $request->user_id;
					$model->review = $request->review;
                    $model->save();
					
					
					
					
					
				   if($rating != '')
                    {
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =    'Rating Submitted Successfully';
                    $data['data'] = $rating;


					
					
					//sending notifications
					
					@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['username'])->username;
	                @$token = @\App\UsersSessions::where('id',$request->profile_user_id)->pluck('notification_token');
	                @$notify_following = @\App\User::where('id',$request->profile_user_id)->first(['notify_following'])->notify_following; 
					
					
					$inc = \App\User::where("id",$request->profile_user_id)->increment('badge_count');
					$details1 = array();
					$details['id'] = $request->user_id;
					$df='';
				    $this->create_notification('user', $follower_first_name.' rated you with '.$request->rating.' points',$request->profile_user_id,$request->user_id,$details1 , $df);
						  
					
              
                    $details = array();
		            $details['id'] = @$request->user_id;
		            $details['notification_type'] = 'push';
		            if($notify_following == 1 or $notify_following == '1')
		            {
	                     $this->notification_to_single_identifier($token , 'Rated' , $follower_first_name.' review & rated you with '.$request->rating.' points' , $details ,'user');
		            }
        //sending notification ends
		
		
		
		
		
                    }                    
                    else
                    {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Some Error Occurred';
					$data['data']        =   [];
                    }
                }
        return $data;
    }
	
	
	
	
	
	    //Searching for a user
    public function search_store(Request $request)
    {
         $validator = Validator::make($request->all(), [
                'search_text'=>'required',
                'user_id'=>'required'
                 ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
		
	 
               
                        $d = \App\Admin::where('user_type','brand')->where(function($q) use ($request) {
                                $q->orWhere( DB::raw("CONCAT(first_name,' ',last_name)"),'like', '%'.$request->search_text.'%');
                            }) ->orderBy('id','desc')->get()->toArray();
                     
					 $stores = array();
                 
					for($l=0;$l<sizeof($d);$l++)
					{
						
						$brand_id = $d[$l]['id'];
						
						
								  
								  $lat = $request->latitude;
                                  $lon = $request->longitude;
								  
@$nearest_stores = @DB::table("admin") 
	->select("admin.*"
		,DB::raw("6371 * acos(cos(radians(".$lat.")) 
		* cos(radians(admin.latitude)) 
		* cos(radians(admin.longitude) - radians(".$lon.")) 
		+ sin(radians(".$lat.")) 
		* sin(radians(admin.latitude))) AS distance "))->where('store_brand_id',$brand_id)
		->groupBy("admin.id")
		->take(1)->orderBy('distance','ASC')->get(['long_address']);
		
		
		if(sizeof($nearest_stores) > 0 )
		{
		//return  $d[$l]['first_name'];
		            @$store_id = @$nearest_stores[0]->id;
					@$following = @\App\StoresRelations::where('store_id',$store_id)->where('user_id',$request->user_id)->count();
				    @$d[$l]["nearest_store"] =  @$nearest_stores[0];	
					@$d[$l]["nearest_store"]->following =  @$following;	
					@$d[$l]['brand_first_name'] = $d[$l]['first_name'];
					@$d[$l]['brand_last_name'] = $d[$l]['last_name'];
		            $stores[] = @$d[$l];
		}
					//	$stores = \App\Admin::where('user_type','store')->where()
						
						 
					}
        
                    if($request->search_text == '%')
                    {
                        $stores = [];
                    }
                    $data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Users Fetched sucessfully';
                    $data['users'] = $stores;
                 
             
            }
            return $data;
    }
	
	
	
     public function create_notification($notification_type,$notification_message,$user_id,$notification_by_user_id, $details , $df='')
    {               
                    $for_user_notification = User::select('id','notification_token')->where('id',$user_id)->first();    
                    App::setLocale($for_user_notification['language']);
					
				 
			        $sub_title = @\App\NotificationTriggers::where('notification_type','app')->where('trigger_type',$notification_type)->first(['sub_title'])->sub_title;
			        $sub_title = $this->get_notification_string(@$sub_title , @$df);
			  
                    $input = array();                
                    $input['notification_type'] = @$notification_type;        
                    $input['notification_message'] = @$sub_title;
					if($sub_title == null or $sub_title == '')
					{
						$input['notification_message'] = @$notification_message;
					}
					
                    $input['user_id'] = @$user_id;
					$input['linked_id'] =@$details['id'];
					 
                    $input['is_read'] = 0;
                   
                    $input['status'] = 1;
                    $notification = \App\Notification::create($input);
                    $badge_count = $for_user_notification['badge_count']+1;
                    @\App\User::where('id',$for_user_notification['id'])->update(['badge_count'=>$badge_count]);
    }
	
	
	
	
	
	
	
	
	
	
	
 
		
	//services on 21 april 2018
	
	
	// service 1
	 
	public function check_pgp_key_exist(Request $request)
	{		
 	    $user_id = $request->user_id;
	    $validator = Validator::make($request->all(), [
            'user_id' => 'required',
		    ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
             $pgp_key = \App\User::where('id',$request->user_id)->first(['pgp_key'])->pgp_key;
			 if($pgp_key == null or $pgp_key =='')
			 {
				             $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';
                             $data['message']        =   'you do not have pgp key';
							 return $data;
			 }
			 else
			 {
				             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';
                             $data['message']        =   'Pgp key found';
							 return $data;
				 
			 }
        }
    }
	 
	
	// service 2
	public function get_2fa_otp_message(Request $request)
	{
		
 	    $user_id = $request->user_id;
	    $validator = Validator::make($request->all(), [
            'user_id' => 'required',
		 
 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
			 
			
        
		 $otp = str_random(6);
		 
		 $users_pgp_key = \App\User::where("id",$request->user_id)->first(['pgp_key'])->pgp_key;
		 $pgp_encrypted_message =  $this->gpg_encryption_decryption($request,$users_pgp_key,$otp);
		
		 
		 $o = \App\Otps::firstOrNew(array('user_id' => $user_id));
         $o->otp = $otp;
		 $o->pgp_encrypted_message = $pgp_encrypted_message;
	 
         $o->save();
		 
		 

             if($o != '')
			 {
				             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';
                             $data['message']        =   'Successfull';
							 $data['data'] = $o;
							 return $data;
				 
			 }
			 else
			 {
				             $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';
                             $data['message']        =   'you do not have pgp key';
							 $data['data'] = [];
							 return $data;
				 
			 }
 return $data;
        }
         
 	 return $data;
	}
	
	
	
	
	public function update_two_fa_status(Request $request)
	{
		$user_id = $request->user_id;
		$status = $request->status;
		
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
		 
 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {

                        App\User::where('id',$user_id)->update(['two_fa_status' => $status]);

			 $decrypted_message = $request->decrypted_message;
        
             $encrypted_message_count = \App\Otps::where('user_id',$request->user_id)->where('otp',$request->decrypted_message)->count();
			  
			 if($encrypted_message_count > 0)
			 {
				 App\User::where('id',$user_id)->update(['two_fa_status' => $status]);
				             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';
                             $data['message']        =   'Status Updated';
							 return $data;
				 
			 }
			 else
			 {
					 		 $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';
                             $data['message']        =   'you entered invalid Encryption Message';
							 return $data;
			 }

        }
        return $data;
 	
	}
	
	
	
	
	
		public function two_fa_login(Request $request)
	{
		$user_id = $request->user_id;
		$status = $request->status;
		
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
		 
 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
			 $decrypted_message = $request->decrypted_message;
        
             $encrypted_message_count = \App\Otps::where('user_id',$request->user_id)->where('otp',$request->decrypted_message)->count();
			  
			 if($encrypted_message_count > 0)
			 {
				 
				             $da = DB::table('users')->where('id', '=', $request->user_id)->get();
							 
							 
							                     $da[0]->profile_image = \App\User::where('id',$request->user_id)->first(["profile_image"])->profile_image;
					
				    $da[0]->followers_count = \App\UsersRelations::where('recipient_id', $request->user_id)->where('status','1')->count();
					$da[0]->followings_count = \App\UsersRelations::where('user_id', $request->user_id)->where('status','1')->count();
					$da[0]->posts_count  = \App\Posts::where('user_id', $request->user_id)->count();
					
					
				             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';
                             $data['message']        =   'Status Updated';
							 $data['user_data'] =  $da;
							 return $data;
				 
			 }
			 else
			 {
					 		 $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';
                             $data['message']        =   'you entered invalid Decrypted Message';
							 return $data;
			 }

        }
        return $data;
 	
	}
	
	
	
	
	
	
	
	
	
		public function gpg_encryption_decryption(Request $request,$public_key,$plain_string)
	{
			 
	 //require public_path().'/gpg/vendor/ccc.txt';
     require  public_path().'/gpg/libs/GPG.php';
     $gpg = new \GPG();
 
 
 
$public_key_ascii = $public_key;

 
// create an instance of a GPG public key object based on ASCII key
$pub_key = new \GPG_Public_Key($public_key_ascii);

$plain_text_string = $plain_string;
// using the key, encrypt your plain text using the public key
$encrypted = $gpg->encrypt($pub_key,$plain_text_string);

$http_status = app('Illuminate\Http\Response')->status();


$d_arr = array();
$d['encrypted_message'] = $encrypted;
$d['plain_text_string'] = $plain_text_string;
$d_arr[] = $d;
if($http_status == '200')
{
	return 	$encrypted;			$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Encrypted Message Fetched';
                    $data['data']      =  $d_arr;
}
else
   {
					$data['status_code']    =   0;                      
                    $data['status_text']    =   'Failed';
                    $data['message']        =   'Some Error Occurred';
                    $data['data']      =  [];
	}
 
 return $data;

 
	}
	
	
	
	
	
	
	
	
	// new services added ===================================
	public function update_private_status(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'private_status' => 'required',
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   


                           @$d = @\App\UsersRelations::where('recipient_id', $request->user_id)->where('status', '0')->count();
			   if($d  > 0)
                            {
			     $data['status_code']    =   0;
                             $data['status_text']    =   'Failed';             
                             $data['message']        =   'You have pending follow requests.';
                             $data['user_data']      =   [];      
                             return $data; 
                            } 






	            $result = User::where('id',$request->user_id)->update(['private_status'=>$request->private_status]);
				 $private_status = @\App\User::where('id',$request->user_id)->first(['private_status'])->private_status;
							 $d=array();
							 $d1['private_status'] = $private_status;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Submitted successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
		public function get_private_status(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	          
				            $private_status = @\App\User::where('id',$request->user_id)->first(['private_status'])->private_status;
							 $d=array();
							 $d1['private_status'] = $private_status;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Fetched successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
	
	public function get_2fa_status(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	          
				            $two_fa_status = @\App\User::where('id',$request->user_id)->first(['two_fa_status'])->two_fa_status;
							 $d=array();
							 $d1['two_fa_status'] = $two_fa_status;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Fetched successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
		public function get_pgp_key(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	          
				            $pgp_key = @\App\User::where('id',$request->user_id)->first(['pgp_key'])->pgp_key;
							 $d=array();
							 $d1['pgp_key'] = $pgp_key;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Fetched successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
 
	

	
	
	
	
	
	
	
	
		public function update_preferred_currency(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'preferred_currency' => 'required',
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	            $result = User::where('id',$request->user_id)->update(['preferred_currency'=>$request->preferred_currency]);
				$preferred_currency = @\App\User::where('id',$request->user_id)->first(['preferred_currency'])->preferred_currency;
							 $d=array();
							 $d1['preferred_currency'] = $preferred_currency;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Preferred Currency Successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
	
		
		public function get_preferred_currency(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        
        'user_id' => 'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	          
				            $preferred_currency = @\App\User::where('id',$request->user_id)->first(['preferred_currency'])->preferred_currency;
							 $d=array();
							 $d1['preferred_currency'] = $preferred_currency;
							 $d[]=$d1;
			    $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Fetched successfully.';
                $data['user_data']      =   $d;       
		}
       return $data;
    }
	
	
	
	
	public function accept_reject_follow_request(Request $request)
    {   
        $validator = Validator::make($request->all(), [
        'request_user_id'=> 'required',
        'user_id' => 'required',
		'action'=>'required',
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
 
	
	                      if($request->action == 'accept')
						  {
							   $result = \App\UsersRelations::where('recipient_id',$request->user_id)->where('user_id',$request->request_user_id)->update(['status'=>'1']);
							   
							   
							   					//sending notifications
					
					            $inc = \App\User::where("id",$request->request_user_id)->increment('badge_count');
						    
							$details1 = array();
							$details1['id'] = @$request->user_id;
							$details1['notification_type'] = 'push';
						    $this->create_notification('user',' accepted your follow request',$request->request_user_id,$request->user_id,$details1);
						  
						  
					
						  
						  
						  		//sending notifications
							@$follower_first_name = @\App\User::where('id',$request->user_id)->first(['first_name'])->first_name;
							@$token = @\App\UsersSessions::where('id',$request->request_user_id)->pluck('notification_token');
					        @$notify_following = @\App\User::where('id',$request->request_user_id)->first(['notify_following'])->notify_following; 
		                    $details = array();
							$details['id'] = @$request->user_id;
							$details['notification_type'] = 'push';
							if($notify_following == 1 or $notify_following == '1')
								{
									$this->notification_to_single_identifier($token , 'Accepted' , $follower_first_name.' accepted your follow request' , $details ,'user');
								}
                                                 //sending notification ends
		 
						  }
						  
						  
						  if($request->action == 'reject')
						  {
							   $result = \App\UsersRelations::where('recipient_id',$request->user_id)->where('user_id',$request->request_user_id)->delete();
						  }
	
	
	
		             @$d = @\App\UsersRelations::where('recipient_id', $request->user_id)->where('status', '0')->pluck('user_id');
				    $followers_array = json_decode($d);
					$users_array = array();
					for( $x=0; $x<sizeof($followers_array); $x++ )
					{
					    $user = \App\User::where('id',$followers_array[$x])->first(['id','first_name','last_name','profile_image','profile_image_thumb']);
						$is_following = @\App\UsersRelations::where('recipient_id',$followers_array[$x])->where('user_id',$request->user_id)->where('status','1')->count();
						
						if($is_following > 0 )
						{
							$user['is_following'] = 1;
						}
						else
						{
							$user['is_following'] = 0;
						}
						 $users_array[] = $user;
					}
					
			  
	                      $data['status_code']    =   1;
						  $data['status_text']    =   'Success';             
						  $data['message']        =   'Operation successfull.';
						  
						   
						  $data['data'] = $users_array;
					   
		}
       return $data;
    }
	
	
	
	public function report_user(Request $request)
	{
		 $validator = Validator::make($request->all(), [
        'reported_user_id'=> 'required',
        'user_id' => 'required',
	 
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	
	                      $reported = \App\ReportedUsers::where('reported_user_id',$request->reported_user_id)->where('user_id',$request->user_id)->count();
						  
						  if($reported > 0)
						  {
						   $data['status_code']    =   0;
						   $data['status_text']    =   'Failed';             
						   $data['message']        =   'you already reported this user.';
						   return $data;
						  }
	
	                      $flight = new \App\ReportedUsers;
                          $flight->reported_user_id = $request->reported_user_id;
						  $flight->user_id = $request->user_id;
						  $flight->status = 0;
                          $flight->save();
	
	                      $data['status_code']    =   1;
						  $data['status_text']    =   'Success';             
						  $data['message']        =   'Operation successfull.';
					   
		}
       return $data;
	}
	
	 
	 
	 	public function report_post(Request $request)
	{
		 $validator = Validator::make($request->all(), [
        'user_id'=> 'required',
        'post_id' => 'required',
	 
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	
	                      $reported = \App\ReportedPosts::where('user_id',$request->user_id)->where('post_id',$request->post_id)->count();
						  
						  if($reported > 0)
						  {
						   $data['status_code']    =   0;
						   $data['status_text']    =   'Failed';             
						   $data['message']        =   'you already reported this user.';
						   return $data;
						  }
	
	                      $flight = new \App\ReportedPosts;
                          $flight->user_id = $request->user_id;
						  $flight->post_id = $request->post_id;
						  $flight->status = 0;
                          $flight->save();
	
	                      $data['status_code']    =   1;
						  $data['status_text']    =   'Success';             
						  $data['message']        =   'Operation successfull.';
					   
		}
       return $data;
	}
	
	
	
	
	//gpg_encryption_decryption(Request $request,$public_key,$plain_string)
	
	public function send_pgp_message(Request $request)
	{
		
				 $validator = Validator::make($request->all(), [
        'sender_id'=> 'required',
        'receiver_id' => 'required',
		'message' => 'required',
	 
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	
	
	                      $pgp_key = @\App\User::where('id',$request->receiver_id)->first(['pgp_key'])->pgp_key;
	                     
		                  $encrypted_message = $this->gpg_encryption_decryption($request,$pgp_key,$request->message);
						  
						  
						  
						  	  
						  	//sending notifications
							@$sender_name = @\App\User::where('id',$request->sender_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$request->sender_id)->first(['last_name'])->last_name;
							@$token = @\App\UsersSessions::where('id',$request->receiver_id)->pluck('notification_token');
					 
		                    $details = array();
							$details['id'] = @$request->sender_id;
							$details['notification_type'] = 'message';
							
							 
					        $this->notification_to_single_identifier($token , 'PGP Message' , $sender_name.' just sent you a encrypted message' , $details , 'chat');
							 
								
								
								
		   
	
	                      $flight = new \App\PgpMessages;
                          $flight->sender_id = $request->sender_id;
						  $flight->receiver_id = $request->receiver_id;
						  $flight->message = $encrypted_message;
						  $flight->read_status = '0';
						
                          $flight->save();
	
	                      $data['status_code']    =   1;
						  $data['status_text']    =   'Success';             
						  $data['message']        =   'Message Sent successfully.';
					   
		}
       return $data;
	   
	   
		
	}
	
	
	
	
	
		public function get_pgp_message(Request $request)
	{ 
				 $validator = Validator::make($request->all(), [
        'user_id'=> 'required',
   
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	                     $messages = @\App\PgpMessages::where('receiver_id',$request->user_id)->orderBy('id','DESC')->paginate(10);
						 
						 foreach($messages as $m)
						 {
							 $m->sender_name = @\App\User::where('id',$m->sender_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$m->sender_id)->first(['last_name'])->last_name;
							 $m->sender_photo = @\App\User::where('id',$m->sender_id)->first(['profile_image'])->profile_image;
						 }
	 
                      $flight = App\PgpMessages::where('receiver_id',$request->user_id)->update(['read_status' => 1]);
                  



	                      if(count($messages) > 0)
						  {
							        $data['status_code']    =   1;
									$data['status_text']    =   'Success';             
									$data['message']        =   count($messages).' messages found';
									$data['data']        =   $messages;
									return $data;
						  }
						  else
						  {
							  		$data['status_code']    =   0;
									$data['status_text']    =   'Failed';             
									$data['message']        =   'No Messages Found.';
									return $data;
						  }
	

					   
		}
       return $data;
	   
	   
		
	}
	
	
	
	
	
	
	
		
		public function delete_pgp_message(Request $request)
	{ 
				 $validator = Validator::make($request->all(), [
        'pgp_message_id'=> 'required',
   
        ]);
 
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
	                           $deleted = @\App\PgpMessages::where('id',$request->pgp_message_id)->delete();
							   
							     $messages = @\App\PgpMessages::where('receiver_id',$request->user_id)->orderBy('id','DESC')->paginate(10);
						 
						 foreach($messages as $m)
						 {
							 $m->sender_name = @\App\User::where('id',$m->sender_id)->first(['first_name'])->first_name." ".@\App\User::where('id',$m->sender_id)->first(['last_name'])->last_name;
							 $m->sender_photo = @\App\User::where('id',$m->sender_id)->first(['profile_image'])->profile_image;
						 }
	 
                      $flight = App\PgpMessages::where('receiver_id',$request->user_id)->update(['read_status' => 1]);
                  
				  
				  
				 
							        $data['status_code']    =   1;
									$data['status_text']    =   'Success';             
									$data['message']        =   'Message Deleted Successfully';
									$data['data']        =   $messages;
									return $data;
					 

					   
		}
       return $data;
	   
	   
		
	}
	
	
	
	
	
	
    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
	
	
	 $source_image = imagecreatefrompng($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}