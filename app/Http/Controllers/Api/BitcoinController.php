<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Controllers\Controller; // using controller class
use App\Country;
use Illuminate\Http\Request;

class BitcoinController extends Controller 
{

    public function currency_bitcoin_price(Request $request)
    {        
	 
	               // $countries = Country::get(["country_name","nicename"]);
					$allarray = array();
			 
			        $data_array = array();
					$d["bitcoin_price"] = 1000;
					$data_array[] = $d;
			         
				 	$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Bitcoin Price Fetched';
                    $data['data']      =   $data_array;
		   return $data;
	}
	
	
	
	public function currency_amount_bitcoin_price($currency_code='')
    {        
	 
	                //$countries = Country::get(["country_name","nicename"]);
					$allarray = array();
			 
			        $data_array = array();
					$d["bitcoin_price"] = 1000;
					$data_array[] = $d;
			         
				 	$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Bitcoin Price Fetched';
                    $data['data']      =   $data_array;
		   return $data;
	}
	
	
	
public function gpg_encryption_decryption(Request $request)
{
 //require public_path().'/gpg/vendor/ccc.txt';
require  public_path().'/gpg/libs/GPG.php';
$gpg = new \GPG();
$public_key_ascii = $request->public_key;

 // create an instance of a GPG public key object based on ASCII key
$pub_key = new \GPG_Public_Key($public_key_ascii);

$plain_text_string = $request->plain_string;
// using the key, encrypt your plain text using the public key
$encrypted = $gpg->encrypt($pub_key,$plain_text_string);

$http_status = app('Illuminate\Http\Response')->status();

$d_arr = array();
$d['encrypted_message'] = $encrypted;
$d['plain_text_string'] = $plain_text_string;
$d_arr[] = $d;
if($http_status == '200')
{
	return 	$encrypted;			$data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Encrypted Message Fetched';
                    $data['data']      =  $d_arr;
}
else
   {
					$data['status_code']    =   0;                      
                    $data['status_text']    =   'Failed';
                    $data['message']        =   'Some Error Occurred';
                    $data['data']      =  [];
	}
 
 return $data;

 }
}