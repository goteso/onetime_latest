<?php

namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use App\User;
use Session;
use DB;
use Validator;
use App\TermsConditions;
use Carbon\Carbon;
use Hash;
use Mail;
use File;
use App\Traits\one_signal; // <-- you'll need this line...


class TermsConditionsController extends Controller 
{
   public function get_terms_conditions(Request $request)
    {
	                $TermsConditions = TermsConditions::first(["terms_conditions"])->terms_conditions;
	                $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Fetched Successfully';
                    $data['data'] = $TermsConditions; 
                    return $data;
    }
}