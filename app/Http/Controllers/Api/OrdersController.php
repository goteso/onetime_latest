<?php
namespace App\Http\Controllers\Admin;
use Intervention\Image\Facades\Image as Image;
use App\User;
use App\Role;
use App\Products;
use App\ProductMetaTypes;
use App\ProductMetaValues;
use App\ProductVariants;
use App\ProductVariantTypes;
 
use Session;
use DNS2D;
 
use Validator;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Excel;
use App\Traits\feature; // <-- you'll need this line...
use Illuminate\Auth\Events\Registered;
 
 
 
 
 
 
 
 use App\Traits\notification; // <-- you'll need this line...
 
 
 
 
 
 
 
use App\Jobs\SendVerificationEmail;
class OrdersController extends Controller
{
   use feature;   
 
 
 use notification; // <-- ...and also this line.
public function traits(){
	 return $this->notification();
 }
 
 
 
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    
	
	
	public function barcode()
	{
	 $base_string=  \DNS1D::getBarcodePNG('11', 'C39');
     $image = base64_decode($base_string);
     $image_name= time().'.png';
     $path = public_path() . "/orders/" . $image_name;
     file_put_contents($path, $image);
    } 
	
	
 
 
	 
 
 public function orders_list_detail()
{
		  return view('orders.orders');
}
 	public function order_add()
{ 
	return view('orders.add-order');
}
  public function order_detail()
{ 
	return view('orders.order-detail');
}


	// 1 ==================================================== show Order Form =====================================================================
	
	public function get_category_products($category_id,$path,$parent_category_title,$category_title)
	{
	       $sub_categories = \App\Categories::where('parent_id',$category_id)->pluck('id');
		
		   for($j=0;$j<sizeof($sub_categories);$j++)
		   {
			   $category_title = @\App\Categories::where('id',$sub_categories[$j])->first(['title'])->title; 
			   $s_count = \App\Categories::where('parent_id',$sub_categories[$j])->count();
			   
			      $c["products"] = \App\Products::whereRaw("FIND_IN_SET(".$category_id.",products.category_ids)")->get();
			   
			   
			   
			   if(intval($s_count) < 1)
			   {
				   $path .= "/".$category_title;
				   $s['category_id'] = $sub_categories[$j];
				   $s['category_title'] = $category_title;
				   $s['path'] = $path;
			       $s['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
				   $sub_categories_array[]= $s;
		 
			   }
			   else
			   {
				     $path .= "/".$category_title;
				     $s2['sub'] =$this->get_category_products($sub_categories[$j],$path,$parent_category_title,$category_title);
					 $s2['category_id'] = $sub_categories[$j];
					 $s2['category_title'] = $category_title;
                     $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
					 
			         $sub_categories_array[]= $s2;
				    
			   }
		   }
		   
		   return $sub_categories_array;
	}
	
	
	public function add_order_form(Request $request)
	{
		
		
	 
		 
		$main = array();
		$categories = array();
		$root_categories = \App\Categories::where('parent_id','')->orderBy('sort_index','ASC')->pluck('id');
		
 
 
		
	 
		for($i=0;$i<sizeof($root_categories);$i++)
		{
			$path ='';
			$path2 ='';
		  
		   $parent_category_title = @\App\Categories::where('id',$root_categories[$i])->first(['title'])->title;
		   $path = $parent_category_title;
		   $path2 = $parent_category_title;
           $sub_categories = \App\Categories::where('parent_id',$root_categories[$i])->pluck('id');
		   
		   
				   $c["products"] = \App\Products::whereRaw("FIND_IN_SET(".$root_categories[$i].",products.category_ids)")->get();
				   
			   
		  $sub_categories_array = array();
		   	for($j=0;$j<sizeof($sub_categories);$j++)
		   {
               
			 
			   $category_title = @\App\Categories::where('id',$sub_categories[$j])->first(['title'])->title;
			   
			   $s_count = \App\Categories::where('parent_id',$sub_categories[$j])->count();
			   
			
			   if(intval($s_count) < 1)
			   {
				   $path2 .= "/".$category_title;
				   $s['category_id'] = $sub_categories[$j];
				   $s['category_title'] = $category_title;
				   $s['path'] = $path2;
			 
			       $s['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
				   $sub_categories_array[]= $s;
		 
			   }
			   else
			   {
				     $path .= "/".$category_title;
				     $s2['sub_categories'] =$this->get_category_products($sub_categories[$j],$path,$parent_category_title,$category_title);
					 $s2['category_id'] = $sub_categories[$j];
					 $s2['category_title'] = $category_title;
					 $s2['path'] = $path;
					 
                     $s2['products'] = \App\Products::whereRaw("FIND_IN_SET(".$sub_categories[$j].",products.category_ids)")->get();
					 $sub_categories_array[]= $s2;
					
			   }
		   }
		   
		   $c['sub_categories'] = $sub_categories_array; 
		   $c['category_id'] = $root_categories[$i]; 
		   $c['category_title'] = $parent_category_title; 
		   $categories[] = $c;
		}
		
	 
		 $data['categories_products'] = $categories;
		$data['order_meta_fields'] = $this->order_meta_form();
		$main = $data;
		return $main;
	}
	
	
	public function order_meta_form( $order_id = '')
{
	
	 
	  $orders_meta_types = \App\OrdersMetaTypes::where('status','1')->where('type','<>','quick_link')->where('type','<>','hidden')->get(['id','title','identifier','type','field_options','field_options_model','field_options_model_columns','parent_identifier']);
	  $fields_array2 = array();
	  foreach($orders_meta_types as $umt)
	  {		  
		  if($umt->field_options_model != '' && $umt->type != 'api')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             //$d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			$user_meta_type_id = $umt->id;  
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
		  
		  
		  		  	    if(isset($_GET["request_type"]) && $_GET["request_type"] == 'api' && $umt->identifier == 'customer_id' )
		  {
			  
			  
			  $umt->value = $_GET["user_id"];
			  $umt->field_options =[];
			  
		  }
		  
		  
	  }
	  
	  
	  $parent_array = array();
	  
	  
	  foreach( $orders_meta_types as $mt)
	  {
		  
		  if($mt->parent_identifier == '' or $mt->parent_identifier == ' ' or $mt->parent_identifier == null )
		  {
			 $parent_array[] = $mt;
		  }
		  
	  }
	  
	 
	  $final_array = array();
	  $child_array = array();
	  foreach($parent_array as $pa)
	  {
		  
		 
		  
		  $c =array();
		  $c[] = $pa;
		   foreach( $orders_meta_types as $mt)
	       {
			   
			  
			   if($pa->identifier == $mt->parent_identifier)
			   {
				  $c[] = $mt;
				  
				  
				   
			   }
		   }
		 
		  $final_array[] = $c; 
		  
	  }
	  
 
	  
	  $data_final["fields"] = $final_array;
	  
	  //return $orders_meta_types;  //old
	  return $data_final; //new
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $orders_meta_types;
	  return $data_array2;
}

public function order_meta_form2( $order_id = '')
{
	  $orders_meta_types = \App\OrdersMetaTypes::where('status','1')->get(['id','title','identifier','type','field_options','field_options_model','field_options_model_columns','parent_identifier']);
	  $fields_array2 = array();
	  foreach($orders_meta_types as $umt)
	  {		

	  

		 

         	  
		  if($umt->field_options_model != '' && $umt->type != 'api')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             //$d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			$user_meta_type_id = $umt->id;  
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
		  
		  
		  
 
		  	    if(isset($_GET["request_type"]) && $_GET["request_type"] == 'api' && $umt->identifier == 'customer_id' )
		  {
			  
			  
			  $umt->value = $_GET["user_id"];
			  
		  }
		  
	  }
	  
	  
	  $parent_array = array();
	  
	  
	  foreach( $orders_meta_types as $mt)
	  {
		  
		  if($mt->parent_identifier == '' or $mt->parent_identifier == ' ' or $mt->parent_identifier == null )
		  {
			 $parent_array[] = $mt;
		  }
		  
	  }
	  
	 
	  $final_array = array();
	  $child_array = array();
	  foreach($parent_array as $pa)
	  {
		  
		 
		  
		  $c =array();
		   foreach( $orders_meta_types as $mt)
	       {
			   
			  
			   if($pa->identifier == $mt->parent_identifier)
			   {
				  $c[] = $mt;
				  
				  
				   
			   }
		   }
		  $pa->child = $c;
		  if(count($c) > 0)
		  {
			 $final_array[] = $pa; 
		  }
		  else
		  {
			  $child_array[] = $pa;
		  }
		  
	  }
	  
 
	  
	  $data_final["group_meta_fields"] = $final_array;
	  $data_final["other_meta_fields"] = $child_array;
	  
	  
	  //return $orders_meta_types;  //old
	  return $data_final; //new
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $orders_meta_types;
	  return $data_array2;
}




	public function order_meta_form_old( $order_id = '')
{
	  $orders_meta_types = \App\OrdersMetaTypes::where('status','1')->get(['id','title','identifier','type','field_options','field_options_model','field_options_model_columns','parent_identifier']);
	  $fields_array2 = array();
	  foreach($orders_meta_types as $umt)
	  {		  
		  if($umt->field_options_model != '')
		  {
			  $modelName = $umt->field_options_model;  
              $model = new $modelName();
			  $columns_array = explode(",",$umt->field_options_model_columns);
			  $columns_data = array();
		      $columns_data[] = $columns_array[0];
			  $columns_data[] = $columns_array[1];
			  $col0 =  $columns_array[0];
			  $col1 =  $columns_array[1];
			  $data_field_options = $model::get($columns_data);
			  $t = array();
			  foreach($data_field_options as $fo)
			  {
	             $d='';
				 $d['title'] = $fo->$col0;
				 $d['value'] = $fo->$col1;
				 $t[] = $d;
			  }			  
			  $umt->field_options =$t;
			$user_meta_type_id = $umt->id;  
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
	 
			$umt->value  = $value;
			
			
		  }
	      else if($umt->field_options != '')
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->field_options = json_decode($umt->field_options);
			$umt->value  = $value;
		  }
		  else
		  {
			$user_meta_type_id = $umt->id;
			$value = @\App\OrdersMetaValues::where('order_meta_type_id',$user_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
			if($value == null or $value == '') { $value = '';}
			$umt->value  = $value;
		  }
	  }
	  $data_array2["title"] = "Additional Information";
	  $data_array2["fields"] = $orders_meta_types;
	  return $data_array2;
}





 

// 2. =============================================================== Place Order Form ========================================================================
	
	public function place_order(Request $request)
  {
	    $order_basic = $request->order;
		$orders_payments = $request->orders_payments;
		$order_products = $request->order_products;
		$order_meta_fields = $request->order_meta_fields;
	    $order_meta_fields = $order_meta_fields['fields'];
	   
	  //insert to 'orders' table
        $order = new \App\Orders;
        $order->order_status = $order_basic[0]['order_status'];
		$order->sub_total = $orders_payments[0]['sub_total'];
		$order->coupon_id = $orders_payments[0]['coupon_id'];
		$order->coupon_code = $orders_payments[0]['coupon_code'];
		$order->coupon_discount = $orders_payments[0]['coupon_discount'];
		$order->total = $orders_payments[0]['total'];
		$order->shipping = '0';
		
		

		
		$order->tax = '';
		
		
	    $order->save();
		$order_id = $order->id;
		 
	 
	 
	 	$taxes = \App\Tax::get();
		
		$tax_amount = 0;
		$total_tax = 0;
		foreach($taxes as $tax)
		{
		   $sub_total = floatval($orders_payments[0]['sub_total']);
		   $tax_percentage = floatval($tax->percentage);
		   $tax_amount  =   $sub_total * $tax_percentage/100;
		   $total_tax = $total_tax + floatval($tax_amount); 
		   $order_tax_transactions = new \App\OrderTaxTransactions;
           $order_tax_transactions->order_id = $order_id;
		   $order_tax_transactions->amount = $tax_amount;
		   $order_tax_transactions->tax_id = $tax->id;
		   $order_tax_transactions->save();
        }
		
		$final_sub_total = floatval($orders_payments[0]['sub_total']) + $total_tax;
		\App\Orders::where('id', $order_id)->update(['tax' => $total_tax , 'total'=>$final_sub_total]);
		
		
		
		if(!isset($order_id) or $order_id == '' or $order_id == null)
		{
			                              $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Order Cannot be Placed';  
				                          return $data;
		}
		
		//insert order payments
 
		$order_payments = new \App\OrdersPayments;
        $order_payments->sub_total = $orders_payments[0]['sub_total'];
		$order_payments->coupon_id = $orders_payments[0]['coupon_id'];
		$order_payments->coupon_code = $orders_payments[0]['coupon_code'];
		$order_payments->coupon_discount = $orders_payments[0]['coupon_discount'];
		$order_payments->total = $orders_payments[0]['total'];
		$order_payments->order_id = $order_id;
        $order_payments->save();
		$order_payments_id = $order_payments->id;
		
		//insert products and variants
	 
		if( sizeof($order_products) > 0)
		{
			for($t=0;$t<sizeof($order_products);$t++)
			{
				
		 
		        $products = new \App\OrderProducts;
				$products->order_id = @$order_id;
				$products->product_id = @$order_products[$t]['id'];
				$products->title =@$order_products[$t]['title'];
				$products->category_id =@$order_products[$t]['category_ids'];
				$products->discount =@$order_products[$t]['base_discount'];
				$products->discounted_price =@$order_products[$t]['discounted_price'];
				$products->unit = @$order_products[$t]['units']."";
				$products->quantity = @$order_products[$t]['quantity'];
				$products->save();
				 
				$order_products_variants = @$order_products[$t]['variants'];
				if( sizeof($order_products_variants) > 0)
						{
							
					 
							for($y=0;$y<sizeof($order_products_variants);$y++)
								{
								 
									    $product_variant_type_id = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['product_variant_type_id'])->product_variant_type_id;
									    $product_variant_type_title = @\App\ProductVariantTypes::where('id',$product_variant_type_id)->first(['title'])->title;
										
										
									 
										$product_variant_value = @\App\ProductVariants::where('product_variant_type_id',$product_variant_type_id)->where('product_id',$order_products[$t]['id'])->where('id',$order_products_variants[$y]['id'])->first(['title'])->title;
					 
				 
										$variants = new \App\OrderProductsVariants;
										$variants->order_product_id = $products->id;
										$variants->title =$product_variant_type_title;
										$variants->value =$product_variant_value;
										$variants->product_variant_id = $order_products_variants[$y]['id'];
                                        $variants->save();
									$id =  $order_products_variants[$y]['id'];
								}
						}
						
					 
			}
			
		}
		 //insert to 'orders_meta_value' table
		  
		  
		 
		 if(sizeof($order_meta_fields) > 0 )
		 {
		    for($i=0;$i<sizeof($order_meta_fields);$i++)
			{
				
				$inner_array = $order_meta_fields[$i];
				
				
			 
			 for($j=0;$j<sizeof($inner_array);$j++)
			{
		 
				
				if($inner_array[$j]["type"] == 'api')
				{
					
				 
					$da = $inner_array[$j]['value'];
					
					 
					  if(getType($da) != 'array')
					  {
					  $da =json_decode($da);
					  }
					$field_options_model = $inner_array[$j]['field_options_model'];
					
 
					 $order_meta1 =   new $field_options_model();
					 
					 
					 
					 
				   foreach($da as $key => $value)
				    {
						 
					 
					if($key != 'id' && $key != '$$hashKey')
					{
					$order_meta1->$key = $value;
					}
				 
					
					
				 
				    }
					$order_meta1->save();
				 
				 
		 
					 
					$identifier = $inner_array[$j]['identifier'];
					$order_meta = new \App\OrdersMetaValues;
					$order_meta->order_id = $order_id;
					$order_meta->order_meta_type_id = $inner_array[$j]['id'];
					$order_meta->value = $order_meta1->id;
					$order_meta->save();
				 
					
					
				}
				else
				{
					$identifier = $inner_array[$j]['identifier'];
					$order_meta = new \App\OrdersMetaValues;
					$order_meta->order_id = $order_id;
					$order_meta->order_meta_type_id = $inner_array[$j]['id'];
					$order_meta->value = $inner_array[$j]['value'];
					$order_meta->save();
				}
				
			}
		    }		
		 }
		 
 
		 if(isset($order_id) && $order_id != '' && $order_id != null)
		{
			
				$base_string=  \DNS1D::getBarcodePNG($order_id, 'C39');
				$image = base64_decode($base_string);
				$image_name= $order_id.'.png';
				$path = public_path() . "/orders/" . $image_name;
				file_put_contents($path, $image);
	 
	 
	 
						                  $data['status_code']    =   1;
                                          $data['status_text']    =   'Success';             
                                          $data['message']        =  'Order Placed Successfully';  
										  $data['order_id']        =  $order_id;  
				                          return $data;
			
		}
		else
		{
						                  $data['status_code']    =   0;
                                          $data['status_text']    =   'Failed';             
                                          $data['message']        =  'Order Cannot be Placed';  
				                          return $data;
			
		}
		
	
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  	public function place_order_calculate(Request $request)
  {
	 
		$order_products = $request->order_products;
	 
	 
 
	 
 
	 $s = array();
	 
	 $sub_total = 0;
	 
	 $total = 0;
		if( sizeof($order_products) > 0)
		{
			
			
			for($t=0;$t<sizeof($order_products);$t++)
			{
				
				$product_price = 0;
				
	 
				 
				$product_price =   $product_price + floatval($order_products[$t]['discounted_price']);
				
				
				 
				$order_products_variants = @$order_products[$t]['variants'];
				if( sizeof($order_products_variants) > 0)
						{
						    for($y=0;$y<sizeof($order_products_variants);$y++)
								{
								  
									    $price = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['price'])->price;
										
										if($price != '' or $price != null)
										{
											$product_price = floatval($price);
										}
									    
								}
								
								
								
							for($y=0;$y<sizeof($order_products_variants);$y++)
								{
									
								       $price_difference = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['price_difference'])->price_difference;
										 
											if($price_difference != '' or $price_difference != null)
										{
											$product_price = $product_price + floatval($price_difference);
										}
							    }
								
								
						
						}
						
					
				  $product_price = $product_price * floatval($order_products[$t]['quantity']);
				  $s[] = $product_price;
				 
				
				
			}
			
		}
		
		
		
	
	 
	$total = 0;	 
		for($f=0;$f<sizeof($s);$f++)
		{
			$total = $total + $s[$f];
		}
 
 
 
	  
	 
		$payment_components = array();
			
			$d4['title'] ='Payment Summary';
			
	        $get_attributes = array();
		 
			$get_attributes[] = 'sub_total';
			
			//Sub Total Concept
			$sub_total_component['title'] = 'Sub Total';
			$sub_total_component['value'] = $total;
			$payment_components[] = $sub_total_component;
			
			$c = @\App\FeaturesSettings::where('title','tax')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'tax';
			  
			 
			}
			
			$c = @\App\FeaturesSettings::where('title','shipping')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'shipping';
			  
			  //shipping Concept
			$shipping['title'] = 'Delivery';
			$shipping['value'] = $payment_data->first(['shipping'])->shipping;
			$payment_components[] = $shipping;
			}
             
			 
			 $t = json_encode($get_attributes);
	         
	 
			  
			  
			  
			  $order_tax_transactions = @\App\Tax::get();
			 
			  $tax_string = '';
			  
			   
			  foreach($order_tax_transactions as $ot)
			  {
			    //shipping Concept
		         $tax['title'] = @\App\Tax::where('id',$ot->id)->first(['title'])->title;
			     $tax['value'] =  $ot->percentage * $total / 100;
			     $payment_components[] = $tax;
			   
			 
			 
			   $total = $total + floatval($tax['value']);
			  }
			  
			  
			 
			    $d4["data"]["0"]["tax"] = $tax_string;
			  
			  $d4['data'] = $payment_components;
			   
			 $order_tax_transactions = \App\OrderTaxTransactions::where('order_id',@$request->order_id)->get();
		 
			 $payment_details['type'] = 'payment_details';	
             $payment_details['data'] = $d4;
			 $payment_details['order_total'] = $total;
             $block_array[] = $payment_details;	
			
			
			return $block_array;
			
			
	
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

 public function edit_order(Request $request)
 {
        $order_id = $request->order_id;
         $order_status = $request->order_status;
         if(!$request->order_id)
         {
                                          $data['status_code']    =   0;
                                         $data['status_text']    =   'Failed';            
                                         $data['message']        =  'Order Id Required';  
                                          return $data;
         }
        
         // update order status
         if($request->order_status)
         {
            \App\Orders::where('id',$request->order_id)->update(['order_status' => $request->order_status]); 
         }
          // update order status
         if($request->order_products)
         {
             $products_array = $request->order_products;
            
             for($i=0;$i<sizeof($products_array);$i++)
             {
                  
                 $id = @$products_array[$i]['id'];
                
                 $product_id = $products_array[$i]['product_id'];
                 $quantity = $products_array[$i]['quantity'];
                 $title = $products_array[$i]['title'];
                 $category_id = $products_array[$i]['category_id'];
                 $discount = $products_array[$i]['discount'];
                 $discounted_price = $products_array[$i]['discounted_price'];
                 $unit = $products_array[$i]['unit'];
                
                 if($id !='' && $id != null)
                 {
                     //updates
                     $flight1 = OrderProducts::find($id);
                    $flight1->product_id = $product_id;
                     $flight1->quantity = $quantity;
                     $flight1->title = $title;
                     $flight1->category_id = $category_id;
                     $flight1->discount = $discount;
                     $flight1->discounted_price = $discounted_price;
                     $flight1->unit = $unit;
                     $flight1->order_id = $order_id;
                    $flight1->save();
                    
                 }
                 else
                 {
                     //inserts
                     $flight2 = new OrderProducts;
                     $flight2->product_id = $product_id;
                     $flight2->quantity = $quantity;
                     $flight2->title = $title;
                     $flight2->category_id = $category_id;
                     $flight2->discount = $discount;
                     $flight2->discounted_price = $discounted_price;
                     $flight2->unit = $unit;
                     $flight2->order_id = $order_id;
                    $flight2->save();
                  }
             }
           }
          // order meta starts=============================
          $order_meta_types = \App\OrdersMetaTypes::where('status','1')->get(['id','identifier']);
          foreach($order_meta_types as $omt)
          {
              $omt_id = $omt->id;
              $omt_identifier = $omt->identifier;
              if($request->$omt_identifier)
              {
                 $value = $request->$omt_identifier;
                 if(!isset($value) or $value == null or $value == '') { $value = '';}
                 \App\OrdersMetaValues::where('order_id', $order_id)->where('order_meta_type_id', $omt_id)->update(['value' => $value]);
             }
          }
                                          $data2['status_code']    =   1;
                                         $data2['status_text']    =   'Success';            
                                         $data2['message']        =  'Order Updated Successfully';  
                                          return $data2;
}
 
 //4 order list =======================================================================
 
  	public function orders_list(Request $request)
     {
        
		
	
		
 //if(Auth::user()->can('view_roles')){ //return 'yes';} 
 //else
// {
	// return 'no';
 //}
		//  $orders2 = \App\Orders::orderBy("id","desc")->paginate(1000);
		 
		$login_id = @Auth::id();
	 	$user_type = @Auth::user()->user_type;
		 
		$price_filter = @$_GET['price_filter'];
		$date_filter = @$_GET['date_filter'];
		$order_status = @$_GET['order_status'];
	 
		$data_array = new  \App\Orders;
		 
	    if($order_status != '')
	    {
		   $data_array = $data_array::where('order_status',$_GET["order_status"]);
	    }
	    else  
	    {
		   $data_array = $data_array::where('order_status','<>','not_applicable');
	    }	   
 
	   
	   if($price_filter == 'lowToLow')
	   {
		   $data_array = $data_array->orderBy('total','ASC');
	   }
	   else if($price_filter == 'highToLow')
	   {
		   $data_array = $data_array->orderBy('total','DESC');
	 
	   }
	   
	   else if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC');  
	   }
	   
 
	   
 
      $role_id = @\App\Role::where('name','Vendor')->first(['id'])->id;
	   
	   if($user_type ==$role_id )
	   {
	     $orders2 = $data_array->join('orders_meta_values', 'orders_meta_values.order_id', '=', 'orders.id')->where('orders_meta_values.order_meta_type_id',8)->where('orders_meta_values.value',$login_id)->orderBy('orders.id','DESC')->paginate(1000);
	   }
	   else
	   {
		     $orders2 = $data_array->orderBy('id','DESC')->paginate(1000);
	   }
	 
		 $orders = array();
		 
		 foreach($orders2 as $or)
		 { 
			 
			 $or->created_at_formatted = $or->created_at->diffForHumans();
		 
			 $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		     $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id)->where('order_id',@$or->id)->first(['value'])->value;
			 
			 
		 
			 $order= array();
			 
			// $or->order_status = @\App\OrdersStatus::where('id',$or->order_status)->first(['title'])->title;
			// $or->order_payments_details = @\App\OrdersPayments::where('order_id',$or->id)->get();
			  $or->customer_details = @\App\User::where('id',$customer_id)->get();
			 
			 $or->label_color = @\App\OrdersStatus::where('identifier',$or->order_status)->first(['label_colors'])->label_colors;
		 
			 /**
			 $order_products= \App\OrderProducts::where('order_id',$or->id)->get();
			 foreach($order_products as $op)
			 {
			   $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			 }
			 $order['order_products'] = $order_products;
			 
			  
			 $order['order_payments'] = \App\OrdersPayments::where('order_id',$or->id)->get();
			 **/
			 //meta values
			 
			 /**
			  $order_meta_values = \App\OrdersMetaValues::where('order_id',$or->id)->get();
			  foreach($order_meta_values as $om)
			  {
				    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			  }
			  $order['order_meta_values'] = $order_meta_values;
			  **/
			  
			  
			  
			 
		 }
 
		 
		 $order_status = \App\OrdersStatus::get();
	 
		 
		 foreach($order_status as $s)
		 {
	 
	 
	         $s->orders_count = @\App\Orders::where('order_status',$s->identifier)->count();
			 $s->orders_totals = @\App\Orders::where('order_status',$s->identifier)->sum('total');
		 }
		 
		 
		  
  
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['user_data']      =     $orders2 ;
					   $data['orders_count_data']      =     $order_status ;
					    
	     $result = $data;
		 
 return $result;
	     return view('orders.orders', compact('result'));
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	   	public function drivers_orders_list(Request $request)
     {
		 
		 $order_status = @$_GET['order_status'];
        /**
		$price_filter = @$_GET['price_filter'];
		$date_filter = @$_GET['date_filter'];
		
	 
		$data_array = new  \App\Orders;
		 
	    if($order_status != '')
	    {
		   $data_array = $data_array::where('order_status',$_GET["order_status"]);
	    }
	    else  
	    {
		   $data_array = $data_array::where('order_status','<>','not_applicable');
	    }	   
    
	   if($price_filter == 'lowToLow')
	   {
		   $data_array = $data_array->orderBy('total','ASC');
	   }
	   else if($price_filter == 'highToLow')
	   {
		   $data_array = $data_array->orderBy('total','DESC');
	 
	   }
	   
	   else if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC');  
	   }
	   **/
	    
	   if($request->order_status =='')
	   {
		   $orders2 = \App\Orders::where('order_status','pending')->paginate(1000);
	   }
	   else
	   {
		  
		   $orders2 = \App\Orders::where('order_status',$request->order_status)->paginate(1000);
		   
		   
	   }
   
	    $order_actions_auth_settings = \App\OrderActionsAuthSettings::where('user_type','driver')->get();
	    $assigned_status = $order_actions_auth_settings[0]["assigned_status"];
	    $assigned_status_array = explode(',', $assigned_status);
	    $a_array = array();
		
	    foreach($assigned_status_array as $as)
	    {
		   $a_array[] = @\App\OrdersStatus::where('identifier',$as)->first();
	    }
		$status_data = $a_array;
		 
		  
		 $orders = array();
		 
		 foreach($orders2 as $or)
		 { 
		     $order_meta_type_id_v = @\App\OrdersMetaTypes::where("identifier","driver_id")->first(['id'])->id;
		     $driver_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id_v)->where('order_id',@$or->id)->first(['value'])->value;
			 
			 
             $or->created_at_formatted = $or->created_at->diffForHumans();
		     $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		     $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id)->where('order_id',@$or->id)->first(['value'])->value;
			 $order= array();
			 $or->customer_details = @\App\User::where('id',$customer_id)->get();
			  
			  
			  
			 if($driver_id == $request->user_id)
			 {
				 
				 $orders[] = $or;
			 }
	     }
 
     
		 if(count($orders) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['order_data']      =     $orders ;
					   $data['status_data']      =     $status_data ;
				 
					    
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'No Order Found';
                       $data['order_data']      =   [];
					   $data['status_data']      =  $status_data ;
					 
		 }
		 return $data;
 	 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
 	public function customers_orders_list(Request $request)
     {
		 
		 $order_status = @$_GET['order_status'];
        /**
		$price_filter = @$_GET['price_filter'];
		$date_filter = @$_GET['date_filter'];
		
	 
		$data_array = new  \App\Orders;
		 
	    if($order_status != '')
	    {
		   $data_array = $data_array::where('order_status',$_GET["order_status"]);
	    }
	    else  
	    {
		   $data_array = $data_array::where('order_status','<>','not_applicable');
	    }	   
    
	   if($price_filter == 'lowToLow')
	   {
		   $data_array = $data_array->orderBy('total','ASC');
	   }
	   else if($price_filter == 'highToLow')
	   {
		   $data_array = $data_array->orderBy('total','DESC');
	   }
	   else if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC');  
	   }
	   **/
	    
	   if($request->order_status =='')
	   {
		   $orders2 = \App\Orders::where('order_status','pending')->paginate(1000);
	   }
	   else
	   {
		   $orders2 = \App\Orders::where('order_status',$request->order_status)->paginate(1000);
	   }
       
	    $order_actions_auth_settings = \App\OrderActionsAuthSettings::where('user_type','driver')->get();
	    $assigned_status = $order_actions_auth_settings[0]["assigned_status"];
	    $assigned_status_array = explode(',', $assigned_status);
	    $a_array = array();
		
	    foreach($assigned_status_array as $as)
	    {
		   $a_array[] = @\App\OrdersStatus::where('identifier',$as)->first();
	    }
		$status_data = $a_array;
		 
		  
		 $orders = array();
		 
		 foreach($orders2 as $or)
		 { 
		     $order_meta_type_id_v = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
			 
		 
			  
		     $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id_v)->where('order_id',@$or->id)->first(['value'])->value;
             $or->created_at_formatted = $or->created_at->diffForHumans();
 
		 
 
			
			 $order= array();
			 $or->customer_details = @\App\User::where('id',$customer_id)->get();
			  
			 if($customer_id == $request->user_id)
			 {
				 
				 
				 $orders[] = $or;
			 }
	     }
 
    
 
		 if(count($orders) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['order_data']      =     $orders ;
					   $data['status_data']      =     $status_data ;
				 
					    
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'No Order Found';
                       $data['order_data']      =   [];
					   $data['status_data']      =  $status_data ;
					 
		 }
		 return $data;
 	 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	   	public function orders_list_filtered(Request $request)
     {
        
		//  $orders2 = \App\Orders::orderBy("id","desc")->paginate(1000);
		 
		 
		$price_filter = @$_GET['price_filter'];
		$date_filter = @$_GET['date_filter'];
		$order_status = @$_GET['order_status'];
	 
		 
			 
		 
		        $data_array = new  \App\Orders;
				
	 
	 
	   if($order_status != '')
	   {
		   $data_array = $data_array::where('order_status',$_GET["order_status"]);
	   }
	   else  
	   {
		   $data_array = $data_array::where('order_status','<>','not_applicable');
	 
	   }	
	   
 
	   
	   if($price_filter == 'lowToLow')
	   {
		   $data_array = $data_array->orderBy('total','ASC');
	   }
	   else if($price_filter == 'highToLow')
	   {
		   $data_array = $data_array->orderBy('total','DESC');
	 
	   }
	   
	   else if($date_filter == 'lastAdded')
	   {
		   $data_array = $data_array->orderBy('created_at','DESC');
	   }
	   else if($date_filter == 'firstAdded')
	   {
		  	   $data_array = $data_array->orderBy('created_at','ASC');  
	   }
	   
 
	   
    /**
	   if(isset($_GET['category']) && $_GET['category'] != '' && $_GET['category'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$category."',products.category_ids)");
	   }
	   
	   	   if(isset($_GET['brand']) && $_GET['brand'] != '' && $_GET['brand'] != null)
	   {
	     $data_array = $data_array->whereRaw("FIND_IN_SET('".$brand."',products.brand_ids)");
	   }
	   
	  
	   
	      if(isset($_GET['search_text']) && $_GET['search_text'] != '' && $_GET['search_text'] != null)
	   {
		   
		 $d33 = $_GET['search_text'];
		 $data_array = $data_array->where(function($q) use ($d33) {$q->where( DB::raw("title"),'like', '%'.$d33.'%');});
	   }
	  **/  
	   
	   
	   
	   
	   	   $orders2 = $data_array->paginate(1000);
	   
		 
		 $orders = array();
		 
		 foreach($orders2 as $or)
		 { 
			 
			 $or->created_at_formatted = $or->created_at->diffForHumans();
		 
			 $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		     $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",@$order_meta_type_id)->where('order_id',@$or->id)->first(['value'])->value;
			 
			 $order= array();
			 
			// $or->order_status = @\App\OrdersStatus::where('id',$or->order_status)->first(['title'])->title;
			// $or->order_payments_details = @\App\OrdersPayments::where('order_id',$or->id)->get();
			// $or->customer_details = @\App\User::where('id',$customer_id)->get();
			 
			 /**
			 $order_products= \App\OrderProducts::where('order_id',$or->id)->get();
			 foreach($order_products as $op)
			 {
			   $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			 }
			 $order['order_products'] = $order_products;
			 
			  
			 $order['order_payments'] = \App\OrdersPayments::where('order_id',$or->id)->get();
			 **/
			 //meta values
			 
			 /**
			  $order_meta_values = \App\OrdersMetaValues::where('order_id',$or->id)->get();
			  foreach($order_meta_values as $om)
			  {
				    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			  }
			  $order['order_meta_values'] = $order_meta_values;
			  **/
			  
			  
			  
			 
		 }
		 
		 $order_status = \App\OrdersStatus::get();
	 
		 
		 foreach($order_status as $s)
		 {
	 
			 $s->orders_count = @\App\Orders::where('order_status',$s->id)->count();
			 
			 
			 $s->orders_totals = @\App\Orders::where('order_status',$s->identifier)->sum('total');
		 }
		 
 
		 if(count($orders2) > 0)
		 {
			 		   $data['status_code']    =   1;
                       $data['status_text']    =   'Success';             
                       $data['message']        =   'Orders Fetched Successfully';
                       $data['user_data']      =     $orders2 ;
					   $data['orders_count_data']      =     $order_status ;
					    
		 }
		 else
		 {
			 		   $data['status_code']    =   0;
                       $data['status_text']    =   'Failed';             
                       $data['message']        =   'No Order Not Found';
                       $data['user_data']      =   [];
					 
		 }
		 
		 $result = $data;
		 
		 
		 //return $result;
	 
	 
	 if(isset($_GET["request_type"]) && $_GET["request_type"] == 'api')
	 {
		return $result; 
	 }
 
		 return view('orders.orders', compact('result'));
	 }
	 
	 
	 
	 
	 
 // 3. ======================================= order_details ===================================================
 
  public function order_details(Request $request)
  {
	    
	 
	 
	 
	 
	    $order= array(); 
	  
	        $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		    $customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$request->order_id)->first(['value'])->value;
		 
		 
	 
	  
			$d_array = array(); 
			
			$block_array = array(); 
		    //products
			$order_products= @\App\OrderProducts::where('order_id',$request->order_id)->get();
			$sub_total = 0;
			foreach($order_products as $op)
			{
				$sub_total = $sub_total + floatval($op['discounted_price']*$op['quantity']);
				
				$variants = @\App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
				
				$var_desc = '';
				foreach($variants as $va)
				{
					$var_desc .= $va->title.' : '.$va->value."\n";				
				}
				 
				$op['variants_desc'] = $var_desc;
			   // $op['variants'] =  @\App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			
			
			
			
			
			
			
			
			
			
			
			$d5['title'] = count($order_products)." Items";
		    $d5['total'] = $sub_total;
	        $d5['data'] = @$order_products;
			$order['items'] = $d5;
			
			//$order['items'] = $d2;
			  
			$items['type'] = 'items';
 		
            $items['data'] = $d5;
            $block_array[] = $items;
 
 
 
 
					
					
		    //customer_details starts 
			$customer_details = @\App\User::where('id',$customer_id)->get();
	 
			@$orders_count = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
			@$order_ids =@\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->pluck('order_id');
			 
			$total_revenues = 0;
			for($y=0;$y<sizeof($order_ids);$y++)
			{
				$total_revenues += @\App\OrdersPayments::where('order_id',@$order_ids[$y])->sum('total');
			}
			$total_revenues = $total_revenues;
			
			$customer_details[0]['title'] = @$customer_details[0]['first_name']." ".@$customer_details[0]['last_name'];
			
			$joined = @$customer_details[0]['created_at'];
			
			$currency_symbol = env('CURRENCY_SYMBOL');
			
		    $customer_data['description'] = $customer_details[0]['email']."\n"."Joined : ".$joined."\nTotal Order : ".$orders_count."\nTotal Orders Amount : ".$currency_symbol.$total_revenues;
			 
			 
			$d1['type'] = 'user';
			
			$customer_data["name"] = @$customer_details[0]["first_name"]." ".@$customer_details[0]["last_name"];	
            $customer_data["id"] = @$customer_details[0]["id"];	
            $customer_data["title"] = "Customer Details";
            $customer_data_array = array();
         	$customer_data_array[] = $customer_data;
			$d1['data'] = $customer_data_array;
			 
			$block_array[] = $d1;
		 
	  
			// delivery_address	
		    $delivery_address_meta_type_id = @\App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
			$delivery_address_meta_type_columns = @\App\OrdersMetaTypes::where("identifier","delivery_address")->first(['columns'])->columns;
			$delivery_address_meta_type_type = @\App\OrdersMetaTypes::where("identifier","delivery_address")->first(['type'])->type;
			$delivery_address_meta_type_model = @\App\OrdersMetaTypes::where("identifier","delivery_address")->first(['field_options_model'])->field_options_model;
		    $delivery_address_id =@\App\OrdersMetaValues::where("order_meta_type_id",$delivery_address_meta_type_id)->where('order_id',$request->order_id)->first(['value'])->value;
			
			
			 
	        $delivery_address_columns =explode(",", $delivery_address_meta_type_columns);
					 
					 
			
			$delivery_address =$delivery_address_meta_type_model::where('id',$delivery_address_id)->get($delivery_address_columns);
	        $d2['title'] = 'Delivery Address';
		    $d2['data'] = $delivery_address;
		    $order['delivery_address'] = $d2;
			$delivery_address2['type'] = 'delivery_address';	
            $delivery_address2['data'] = $d2;
            $block_array[] = $delivery_address2	;			  
		    
			
			//payment_details
		 
		 
		 
		    
		   

 

		    $d4['title'] ='Payment Summary';
			
		     $payment_data = @\App\Orders::where('id',@$request->order_id);
	  $d4['order_total'] = $payment_data->first(['total'])->total;
	        $get_attributes = array();
		 
			$get_attributes[] = 'sub_total';
			$c = @\App\FeaturesSettings::where('title','tax')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'tax';
			}
			
			$c = @\App\FeaturesSettings::where('title','shipping')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'shipping';
			}
			
		 
			 
	 $t = json_encode($get_attributes);
	         
			 $d4['data'] = $payment_data->get($get_attributes);
		     $order['payment_details'] = $d4;
			 
			 
			 
			
			 
			  $order_transactions = \App\OrdersTransactions::where('order_id',@$request->order_id)->get();
			  $paid_sum = $order_transactions->sum('amount');
			  $d4["data"]["0"]['total_paid'] = $paid_sum;
			  $d4["data"]["0"]['tax'] = @\App\Orders::where('id',@$request->order_id)->first(['tax'])->tax;
			  //$d4["data"]["0"]['order_transactions'] = $order_transactions ;
			  //$d4["data"]["0"]['order_total_to_pay'] = floatval($d4["data"]["0"]['order_total']) -  floatval($paid_sum);
			 			 $order_tax_transactions = \App\OrderTaxTransactions::where('order_id',@$request->order_id)->get();
			 //$d4["data"]["0"]['order_tax_transactions'] = $order_tax_transactions ;
			 
			 $payment_details['type'] = 'payment_details';	
             $payment_details['data'] = $d4;
             $block_array[] = $payment_details;			
		 
		    
			 
			//=========== meta values ==========
			$order_meta_values = @\App\OrdersMetaValues::where('order_id',@$request->order_id)->get();
			foreach($order_meta_values as $om)
			 {
			  //  $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			 }
			
            $d6['title'] = 'Order Summary';
			$order_meta_values_array = array();  			
			$order_meta_types = \App\OrdersMetaTypes::where('important','0')->where('type','<>','quick_link')->get();
			
			foreach($order_meta_types as $omt)
			{
				$order_meta_type_id = $omt->id;
				
				//$v['order_meta_type_id'] = $omt->id;
				//$v['title'] = $omt->title;
				$v['display_title'] = $omt->display_title;
				//$v['type'] = $omt->type;
				$value = @\App\OrdersMetaValues::where('order_id',@$request->order_id)->where('order_meta_type_id',@$omt->id)->first(['value'])->value;
				if( $omt->type =='api' )
				{
					$model_name = $omt->field_options_model;
					 $columns =explode(",", $omt->columns);
					$v['value'] = $model_name::where('id',$value)->get($columns);
				}
				else if($omt->type =='dateTimePicker' )
				{
					//diffForHumans();
					$d = \App\OrdersMetaValues::where('order_id',@$request->order_id)->where('order_meta_type_id',@$omt->id)->first(['value'])->value;
					$date = \Carbon\Carbon::parse($d);
					$v['value'] =$date->format('d M Y | h:i A');
				}else
					{
				$v['value'] = @\App\OrdersMetaValues::where('order_id',@$request->order_id)->where('order_meta_type_id',@$omt->id)->first(['value'])->value;
					}
				$order_meta_values_array[] = $v;
				
			}
			
			     $meta_data['type'] = 'order_summary';
				 
				 
				 $meta_data['data']['title'] = 'Order Summary';
				 $meta_data['data']['data'] = $order_meta_values_array;
			    
				
				$block_array[] = $meta_data;	
	
	
	  
		
		
		 
			 
			      $quick_l['type'] = 'quickActions_links';
				 
				 
				 $quick_l['data']['title'] = 'Quick Links';
				 $quick_l['data']['data'] = $this->order_quick_actions( $request , 'links');
				 
				 
			 
			 $block_array[] =$quick_l;
			 
			 
			 
	
	 
	 
 
			 $qa["type"] = 'quickActions_misc';
			 $qa["data"] = $this->order_quick_actions( $request , 'misc');
			 $d_array[] = $qa;
 
		 
			 
			 
			 	      $quick_a['type'] = 'quickActions_misc';
				 
				 
				 $quick_a['data']['title'] = 'Quick Actions';
				 $quick_a['data']['data'] = $this->order_quick_actions( $request , 'misc');
				 
				 
			 
			 $block_array[] =$quick_a;
			 
			 
			 
			 		 
	        $d_blocks["order_number"] = $request->order_id;
			 $d_blocks["order_date"] = @\App\Orders::where('id',$request->order_id)->first(['created_at'])->created_at->format('d M , Y h:i A');
             $d_blocks["blocks"] = $block_array;
			 
			 
			 return $d_blocks	;  
  }
  
  
  
    public function order_invoice_data_thermal(Request $request , $order_id)
  {
	  
       $d["order_id"] = $order_id;
	   
	   
	  
	    $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		$customer_id =@\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
		$customer_details = @\App\User::where('id',$customer_id)->get(['first_name','last_name','mobile','email']);
		$d["customer_details"] = @$customer_details[0]->first_name." ".@$customer_details[0]->last_name."\n".@$customer_details[0]->mobile."\n".@$customer_details[0]->email;
	  
	    $app_name = @\App\Settings::where('key_title','app_name')->first(['key_value'])->key_value;
		$currency_symbol = @\App\Settings::where('key_title','currency_symbol')->first(['key_value'])->key_value;
		$app_email = @\App\Settings::where('key_title','app_email')->first(['key_value'])->key_value;
		$app_phone = @\App\Settings::where('key_title','app_phone')->first(['key_value'])->key_value;
		$app_logo = @\App\Settings::where('key_title','app_logo')->first(['key_value'])->key_value;
        $d["vendor_details"] = $app_name."\n".$app_email."\n".$app_phone;
		
		
	 
		$date_now = \Carbon\Carbon::now();
        $d["date"] = $date_now->format('d/m/Y')."";
         
		 $d["app_logo"] = $app_logo;
		 $d["currency_symbol"] = $currency_symbol;
	 
			 
		    //products
			$order_products = \App\OrderProducts::where('order_id',$order_id);
		 
			
	 
			$order_products_id = $order_products->first(['id'])->id;
			$order_products_data = \App\OrderProducts::where('order_id',$order_id)->get(['title','quantity','discounted_price','unit']);
			
			$order_products = $order_products->get();
			
	       /**
			foreach($order_products as $op)
			{
			 // $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			**/
			
			$col_arr = array();
			$col_arr[] = 'Description';
			$col_arr[] = 'Unit/Price';
			$col_arr[] = 'Quantity';
			$col_arr[] = 'Price';
			$d5['columns'] = $col_arr;
			$d5['data'] = $order_products_data;
			$d['items'] = $d5;
			 
			 
			 
			$order_details = \App\Orders::where('id',$order_id); 
			
			//////////////////////////////////////////////////////
			$payment_components = array();
			
			$d4['title'] ='Payment Summary';
			 $d['order_total'] = $order_details->first(['total'])->total;
	        $get_attributes = array();
		 
			$get_attributes[] = 'sub_total';
			
			//Sub Total Concept
			$sub_total_component['title'] = 'Sub Total';
			$sub_total_component['value'] = $order_details->first(['sub_total'])->sub_total;
			$payment_components[] = $sub_total_component;
			
			$c = @\App\FeaturesSettings::where('title','tax')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'tax';
			  
			  //Tax Concept
			//$tax['title'] = 'tax';
			//$tax['value'] = $order_details->first(['tax'])->tax;
			//$payment_components[] = $tax;
			}
			
			$c = @\App\FeaturesSettings::where('title','shipping')->where('status','1')->count();
			if($c > 0)
			{ 
		      $get_attributes[] = 'shipping';
			  
			  //shipping Concept
			$shipping['title'] = 'Delivery';
			$shipping['value'] = $order_details->first(['shipping'])->shipping;
			$payment_components[] = $shipping;
			}
              
			 $t = json_encode($get_attributes);
	         
			 $d4['data'] = $order_details->get($get_attributes);
		    // $order['payment_details'] = $d4;
			  
			  $order_transactions = \App\OrdersTransactions::where('order_id',@$request->order_id)->get();
			  $paid_sum = $order_transactions->sum('amount');
			  
			  $d4['total_paid'] = $paid_sum;
			   
			  $order_tax_transactions = @\App\OrderTaxTransactions::where('order_id',@$request->order_id)->get();
			  
			   
			  $tax_string = '';
			  foreach($order_tax_transactions as $ot)
			  {
				$tax_string .= @\App\Tax::where('id',$ot->tax_id)->first(['title'])->title." : ".$ot->amount."\n";
				
				//shipping Concept
		
			$tax['title'] = @\App\Tax::where('id',$ot->tax_id)->first(['title'])->title;
			$tax['value'] = $ot->amount;
			$payment_components[] = $tax;
				
			  }
			   
			  $tax_title = '';
			  
			  $d4["data"]["0"]["tax"] = $tax_string;
			  
			  
			
			
			
			
			
			 
			
			$d['data'] = $payment_components;
			  $st = \App\FeaturesSettings::where("title",'barcodes')->first(['status'])->status;
             if($st =='1' or $st ==1)
			 { $d["order_barcode"] = 'orders/'.$order_id.".png"; }
		 else {  $d["order_barcode"]  = '';}
			
				$orders_transactions = \App\OrdersTransactions::get();
				/*foreach($orders_transactions as $ot)
				{
					$ot->created_at_formatted = $ot->created_at->format('d-m-Y');
					$ot->transaction_id = \App\OrdersTransactions::where('order_id',$order_id)->first(['transaction_id'])->transaction_id;
				}
	 
	       /**
			foreach($order_products as $op)
			{
			 // $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			**/
			
			$col_arr = array();
			$col_arr[] = 'Transaction Date';
			$col_arr[] = 'Gateway';
			$col_arr[] = 'Transaction Id';
			$col_arr[] = 'Amount';
			$d6['columns'] = $col_arr;
			$d6['data'] = $orders_transactions;
			$d['order_transactions'] = $d6;
			
			
			$d['footer'] = '<h4>Tool by | <a href="https://www.goteso.com" target="_blank">Goteso</a> </h4>';
			
		    return view('orders.invoice_thermal', compact('d'));


 
		   //customer_details starts 
			$customer_details = @\App\User::where('id',$customer_id)->get();
			$customer_details[0]['orders_count'] =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
			$order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->pluck('order_id');
			$total_revenues = 0;
			for($y=0;$y<sizeof($order_ids);$y++)
			{
				$total_revenues += \App\OrdersPayments::where('order_id',$order_ids[$y])->sum('total');
			}
			$customer_details[0]['total_revenues']= $total_revenues;
		 
			$d1['title'] = 'Customer Details';
			$d1['data'] = $customer_details;
			$order['customer_details'] = $d1;
		 
		 
			 
			// delivery_address	
		    $delivery_address_meta_type_id = \App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
		    $delivery_address =\App\OrdersMetaValues::where("order_meta_type_id",$delivery_address_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
	        $d2['title'] = 'Delivery Address';
		    $d2['data'] = $delivery_address;
		    $order['delivery_address'] = $d2;
			 
			 
		    //payment_details
		    $d4['title'] ='Payment Summary';
		    $d4['data'] = \App\Orders::where('id',$order_id)->get();
		    $order['payment_details'] = $d4;
		 
			 
			//meta values
			$order_meta_values = \App\OrdersMetaValues::where('order_id',$order_id)->get();
			foreach($order_meta_values as $om)
			 {
			    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			 }
			$d6['title'] = 'Order Summary';
			$d6['data'] = $order_meta_values;
			 
			$order['order_meta_values'] = $d6;
			return $order;
  }
  
  
  
  public function order_invoice_data(Request $request , $order_id)
  {
	  
       $d["order_id"] = $order_id;
	   
	   
	  
	    $order_meta_type_id = @\App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
		$customer_id = @\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
		$customer_details = @\App\User::where('id',$customer_id)->get(['first_name','last_name','mobile','email']);
		$d["customer_details"] = @$customer_details[0]->first_name." ".@$customer_details[0]->last_name."\n".@$customer_details[0]->mobile."\n".@$customer_details[0]->email;
	  
	    $app_name = \App\Settings::where('key_title','app_name')->first(['key_value'])->key_value;
		$currency_symbol = \App\Settings::where('key_title','currency_symbol')->first(['key_value'])->key_value;
		$app_email = \App\Settings::where('key_title','app_email')->first(['key_value'])->key_value;
		$app_phone = \App\Settings::where('key_title','app_phone')->first(['key_value'])->key_value;
		$app_logo = \App\Settings::where('key_title','app_logo')->first(['key_value'])->key_value;
        $d["vendor_details"] = $app_name."\n".$app_email."\n".$app_phone;
		
		
	 
		$date_now = \Carbon\Carbon::now();
        $d["date"] = $date_now->format('d/m/Y')."";
         
		 $d["app_logo"] = $app_logo;
		 $d["currency_symbol"] = $currency_symbol;
	 
			 
		    //products
			$order_products = \App\OrderProducts::where('order_id',$order_id);
		 
			
	 
			$order_products_id = $order_products->first(['id'])->id;
			$order_products_data = \App\OrderProducts::where('order_id',$order_id)->get(['title','quantity','discounted_price','unit']);
			
			$order_products = $order_products->get();
			
	       /**
			foreach($order_products as $op)
			{
			 // $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			**/
			
			$col_arr = array();
			$col_arr[] = 'Description';
			$col_arr[] = 'Unit/Price';
			$col_arr[] = 'Quantity';
			$col_arr[] = 'Price';
			$d5['columns'] = $col_arr;
			$d5['data'] = $order_products_data;
			$d['items'] = $d5;
			 
			 
			 
			$order_details = \App\Orders::where('id',$order_id)->get(); 
			
			$d["sub_total"] = $order_details[0]->sub_total;
			$d["coupon_discount"] = $order_details[0]->coupon_discount;
			$d["shipping"] = $order_details[0]->shipping;
			$d["tax"] = $order_details[0]->tax;
			$d["total"] = $order_details[0]->total;
			
			  $st = \App\FeaturesSettings::where("title",'barcodes')->first(['status'])->status;
             if($st =='1' or $st ==1)
			 { $d["order_barcode"] = 'orders/'.$order_id.".png"; }
		 else {  $d["order_barcode"]  = '';}
			
				$orders_transactions = \App\OrdersTransactions::get();
				/*foreach($orders_transactions as $ot)
				{
					$ot->created_at_formatted = $ot->created_at->format('d-m-Y');
					$ot->transaction_id = \App\OrdersTransactions::where('order_id',$order_id)->first(['transaction_id'])->transaction_id;
				}
	 
	       /**
			foreach($order_products as $op)
			{
			 // $op['variants'] =  \App\OrderProductsVariants::where('order_product_id',$op['id'])->get();
			}
			**/
			
			$col_arr = array();
			$col_arr[] = 'Transaction Date';
			$col_arr[] = 'Gateway';
			$col_arr[] = 'Transaction Id';
			$col_arr[] = 'Amount';
			$d6['columns'] = $col_arr;
			$d6['data'] = $orders_transactions;
			$d['order_transactions'] = $d6;
			
			
			$d['footer'] = '<h4>Tool by | <a href="https://www.goteso.com" target="_blank">Goteso</a> </h4>';
			
		    return view('orders.invoice', compact('d'));


 
		   //customer_details starts 
			$customer_details = @\App\User::where('id',$customer_id)->get();
			$customer_details[0]['orders_count'] =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
			$order_ids =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->pluck('order_id');
			$total_revenues = 0;
			for($y=0;$y<sizeof($order_ids);$y++)
			{
				$total_revenues += \App\OrdersPayments::where('order_id',$order_ids[$y])->sum('total');
			}
			$customer_details[0]['total_revenues']= $total_revenues;
		 
			$d1['title'] = 'Customer Details';
			$d1['data'] = $customer_details;
			$order['customer_details'] = $d1;
		 
		 
			 
			// delivery_address	
		    $delivery_address_meta_type_id = \App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
		    $delivery_address =\App\OrdersMetaValues::where("order_meta_type_id",$delivery_address_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
	        $d2['title'] = 'Delivery Address';
		    $d2['data'] = $delivery_address;
		    $order['delivery_address'] = $d2;
			 
			 
		    //payment_details
		    $d4['title'] ='Payment Summary';
		    $d4['data'] = \App\Orders::where('id',$order_id)->get();
		    $order['payment_details'] = $d4;
		 
			 
			//meta values
			$order_meta_values = \App\OrdersMetaValues::where('order_id',$order_id)->get();
			foreach($order_meta_values as $om)
			 {
			    $om->order_meta_title = \App\OrdersMetaTypes::where('id',$om->order_meta_type_id)->first(['title'])->title;
			 }
			$d6['title'] = 'Order Summary';
			$d6['data'] = $order_meta_values;
			 
			$order['order_meta_values'] = $d6;
			return $order;
  }
 
  
// order_quick_actions ===================================================================
  
  public function order_quick_actions(Request $request)
  {
	   $order_id = $request->order_id;
	  
	   $d1["orderNumber"] = $order_id;
	   $created_at = \App\Orders::where('id',$order_id)->first(['created_at'])->created_at->format('d M, Y h:i A')."";
	   $d1["orderDate"] = $created_at;
	  
	   $blocks_array = array();
	  
	   $b1["type"] = "items";
	   $b1["total"] = "";
 	  	  
	   //products
	   $order_products= \App\OrderProducts::where('order_id',$order_id)->get();
	   $products_data_array = array();
	   foreach($order_products as $op)
		{
			
		  $product_id = $op->product_id;
		  
		  $b1_d1["img"] = @\App\Products::where("id",$product_id)->first(['photo'])->photo;
		  $b1_d1["product_id"] = $product_id;
		  
		  $b1_d1["title"] = @\App\Products::where("id",$product_id)->first(['title'])->title;
		  $b1_d1["desc"] = @\App\Products::where("id",$product_id)->first(['title'])->title;
		  
		  $id3 = @\App\ProductMetaTypes::where("identifier","unit")->first(['id'])->id;
		  
	 
	      $units = @\App\ProductMetaValues::where("product_meta_type_id",$id3)->where('product_id',$product_id)->first(['value'])->value;
	  
		  $b1_d1["units"] =$units;
		  $b1_d1["total"] ="";
		  
		  $products_data_array[] = $b1_d1;
	    }
			$b2['type'] = 'items';
			$b2['total'] =  \App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
			$b2['data'] = $products_data_array;
	 
	 
	        $blocks_array[] = $b2;
	 
	  
	 //customer_details
	  $order_meta_type_id = \App\OrdersMetaTypes::where("identifier","customer_id")->first(['id'])->id;
	  $customer_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('order_id',$order_id)->first(['value'])->value;
	  $total_orders =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id)->where('value',$customer_id)->count();
	 
	  $d3["type"] = "user";
	  $d3["title"] = "Customer Information";
	  $d3["id"] = $customer_id;
	  $d3["name"] = @\App\User::where("id",$customer_id)->first(['first_name'])->first_name." ".@\App\User::where("id",$customer_id)->first(['last_name'])->last_name;
	  $email = @\App\User::where("id",$customer_id)->first(['email'])->email;
	  $joined = @\App\User::where("id",$customer_id)->first(['created_at'])->created_at;
	 
	  $d3["desc"] = $email." \n Joined: ".$joined." \n Total orders: ".$total_orders;
	 $blocks_array[] = $d3;
	 
	 
	 //address
	  $order_meta_type_id2 = \App\OrdersMetaTypes::where("identifier","delivery_address")->first(['id'])->id;
	  $address_id =\App\OrdersMetaValues::where("order_meta_type_id",$order_meta_type_id2)->where('order_id',$order_id)->first(['value'])->value;
	  
	  $address_details = \App\Addresses::where('id',$address_id)->first();
	  
	   $d4["type"] = "address";
	   $d4["id"] = $address_id;
	   $d4["desc"] = $address_details->address_title."\n ".$address_details->address_line1."\n".$address_details->address_line2."\n".$address_details->city." ".$address_details->state." ".$address_details->country." ".$address_details->pincode;
       $blocks_array[] = $d4;
 
 
     //order summary
	 
	 $d5["type"] = "orderSummary";
	 $order_summary_data_array = array();
	 
	  $id1 = \App\OrdersMetaTypes::where("identifier","pickup_time")->first(['id'])->id;
	  $pickup_time =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	  $id2 = \App\OrdersMetaTypes::where("identifier","delivery_time")->first(['id'])->id;
	  $delivery_time =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	  $id3 = \App\OrdersMetaTypes::where("identifier","notes")->first(['id'])->id;
	  $notes =\App\OrdersMetaValues::where("order_meta_type_id",$id1)->where('order_id',$order_id)->first(['value'])->value;
	 
	 $summary_d1["title"] = "Pickup Time";
	 $summary_d1["value"] = $pickup_time;
	 $order_summary_data_array[] = $summary_d1;
	 $summary_d2["title"] = "Delivery Time";
	 $summary_d2["value"] = $delivery_time;
	 $order_summary_data_array[] = $summary_d2;
	 $summary_d3["title"] = "Notes";
	 $summary_d3["value"] = $notes;
	 $order_summary_data_array[] = $summary_d3;
	 $d5["data"] = $order_summary_data_array;
	 $blocks_array[] = $d5;
	 
	 //payment summary
	 
	 $d6["title"] = "paymentSummary";
	 $d6["total"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
	 $d6_data_array = array();
	 
	 $d6_d1["title"] = "Subtotal";
	 $d6_d1["value"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['sub_total'])->sub_total;
	 $d6_data_array[]=$d6_d1;
	 
	 $d6_d2["title"] = "Tax";
	 $d6_d2["value"] = "NaN";
	 $d6_data_array[]=$d6_d2;
	 
	 $d6_d3["title"] = "Discount";
	 $d6_d3["value"] = @\App\OrdersPayments::where('order_id',$order_id)->first(['coupon_discount'])->coupon_discount;
	 $d6_data_array[]=$d6_d3;
	 
	 $d6["data"] = $d6_data_array;
	 $blocks_array[] = $d6;
	 
	 $d1["blocks"] = $blocks_array;
	 
	 $d7_array = array();
	 
	 $d7_d1["links"] = @\App\OrdersQuickActionsLinks::get();
	 
	 $misc = @\App\OrdersQuickActionsMisc::get();
	 
	 
	 foreach($misc as $m)
	 {
		 $type = $m->type;
		 if($type == 'orderStatus')
		 {
			 $m->value = @\App\Orders::where('id',$order_id)->first(["order_status"])->order_status;
		 }
		 
		  if($type == 'orderPaymentReceived')
		 {
			 $c = @\App\OrdersPayments::where('order_id',$order_id)->count();
			 $total = @\App\OrdersPayments::where('order_id',$order_id)->first(['total'])->total;
			 
	 
			 if($c > 0)
			 {
				 $m->value = "Rs. ".$total." ( Rs 0 left)";
			 }
			 else
			 {
				$m->value ="Not Received";
			 }
			 
		 }
	 }
	 $d7_d2["misc"] = $misc;
	 
	 $d7_array[] = $d7_d1;
	 $d7_array[] = $d7_d2;
	 
	 $d1["quickActions"] = $d7_array;
	 return $d1;
   
  }
  
  


public function get_order_status_list(Request $request)
 {
	  
	 if(isset($_GET["request_type"]) && $_GET["request_type"] == 'api')
	 {
		 $role_id = @\App\User::where('id',$request->user_id)->first(['user_type'])->user_type;
			 if($role_id == '3')
	 {
		  $assigned_status = \App\orderActionsAuthSettings::where('user_type','Driver')->first(['assigned_status'])->assigned_status;
		 $sa = array();
		 $assigned_status_arr = explode(",",$assigned_status);
	     $order_status = \App\OrdersStatus::get();
		  for($x=0;$x<sizeof($assigned_status_arr);$x++)
		 {
		   $sa[] = @\App\OrdersStatus::where('title',$assigned_status_arr[$x])->first();
		 }
		 return $sa;
		 
	 }
	 else
	 {
		 $s =@\App\OrdersStatus::get();
		  return $s;
	 }
	 }
	 else
	 {
	 
	 $login_id = @Auth::id();
	 $role_id = @Auth::user()->user_type;
	 
	 if($role_id == '3')
	 {
		  $assigned_status = \App\orderActionsAuthSettings::where('user_type','Driver')->first(['assigned_status'])->assigned_status;
		 $sa = array();
		 $assigned_status_arr = explode(",",$assigned_status);
	     $order_status = \App\OrdersStatus::get();
		  for($x=0;$x<sizeof($assigned_status_arr);$x++)
		 {
		   $sa[] = @\App\OrdersStatus::where('title',$assigned_status_arr[$x])->first();
		 }
		 return $sa;
		 
	 }
	 else
	 {
		 $s =@\App\OrdersStatus::get();
		  return $s;
	 }
 	
	 }
 
 }
  





  
    
public function order_status_update(Request $request)
  {
	    @\App\Orders::where('id', $request->order_id)->update(['order_status' => $request->status]);
	  	$order_meta_type_id = @\App\OrdersMetaTypes::where('identifier','customer_id')->first(['id'])->id;
		$customer_id = @\App\OrdersMetaValues::where('order_meta_type_id',$order_meta_type_id)->first(['value'])->value;
		$customer_details = @\App\User::where('id',@$customer_id)->get();
		$customer_name = @$customer_details[0]["first_name"]." ".@$customer_details[0]["last_name"];
		$customer_email = @$customer_details[0]["email"];
		 
		$this->notify($request->order_id , 'order_status_update' ,$request->user_id , $request->status);
 
	    $de = array();

		$de[] = @$this->order_details($request);
		
		
		               $data2['status_code']    =   1;
                       $data2['status_text']    =   'Success';             
                       $data2['message']        =   'Order Details Fetched';
                       $data2['user_data']      =   $de;  
					   
					   return $data2;
		
		
  }
  
  
  
 public function assign_driver(Request $request)
  {
	$order_id = $request->order_id;
	$driver_id = $request->driver_id;
	$order_meta_type_id = \App\OrdersMetaTypes::where("identifier","driver_id")->first(['id'])->id;
    $user = \App\OrdersMetaValues::firstOrNew(array('order_meta_type_id' => $order_meta_type_id ,'order_id' => $order_id ));
	$user->value = $request->driver_id;
	$user->save();
    return 1;
  }
  
   public function assign_vendor(Request $request)
  {
	$order_id = $request->order_id;
	$vendor_id = $request->vendor_id;
	$order_meta_type_id = \App\OrdersMetaTypes::where("identifier","vendor_id")->first(['id'])->id;
    $user = \App\OrdersMetaValues::firstOrNew(array('order_meta_type_id' => $order_meta_type_id ,'order_id' => $order_id ));
	$user->value = $request->vendor_id;
	$user->save();
    return 1;
  }
  
   
  public function order_product_delete(Request $request)
  {
	   $order_product_id = $request->order_product_id;
	   
	   
	   $discounted_price = @\App\OrderProducts::where('id',$order_product_id)->first(['discounted_price'])->discounted_price;

	   
	   $quantity = @\App\OrderProducts::where('id',$order_product_id)->first(['quantity'])->quantity;
	   $order_id = @\App\OrderProducts::where('id',$order_product_id)->first(['order_id'])->order_id;
	   
	   	   
		$tax_transactions = @\App\OrderTaxTransactions::where('order_id',$order_id)->get();
		$discounted_tax = $discounted_price * $quantity * 0.09;
		foreach($tax_transactions as $tax_transaction)
		{
		   @\App\OrderTaxTransactions::where('id',$tax_transaction->id)->decrement('amount',$discounted_tax);		   
        }
	   
	   $tax = @\App\Tax::get();
	   $tax_deduct = 0;
	   
	   
	   foreach($tax as $t)
	   {
		   $tax_deduct =+ $discounted_price * $quantity / 100 * $t->percentage;
	   }
	   $final_deduction_total = $tax_deduct  + $discounted_price;
	   // return $discounted_price."--".$tax_deduct."--".$final_deduction_total;
	   @\App\Orders::where('id',$order_id)->decrement('sub_total',$discounted_price*$quantity);
	   @\App\Orders::where('id',$order_id)->decrement('total',$final_deduction_total*$quantity);
	   
	   
	   
	   
	   @\App\OrderProducts::where('id',$order_product_id)->delete();
	   @\App\OrderProductsVariants::where('order_product_id',$order_product_id)->delete();
	 	 		       
	   $data['status_code']    =   1;
       $data['status_text']    =   'Success';             
       $data['message']        =   'Product Deleted Successfully';
       return $data;
  }
  
  
  public function get_order_product_variants(Request $request)
  {
	 $order_product_id = $request->order_product_id;
	 $product_id =\App\OrderProducts::where('order_id',$request->order_id)->where('id',$request->order_product_id)->first(['product_id'])->product_id;
	 
	 $product_variant_types = \App\ProductVariantTypes::get();
	 
	 $product_variant_types_array = array();
	 foreach($product_variant_types as $pvt)
	 {
		 $variant = @\App\ProductVariants::where("product_variant_type_id",$pvt->id)->where('product_id',$product_id)->get();
		 if(count($variant) > 0)
		 {
			 foreach($variant as $v)
			 {
				 $d["order_product_id"] = $order_product_id;
				 $d["title"] = $pvt->title;
				 $d["value"] = $v->title;
				 $d["product_variant_id"] = $v->id;
			     $enabled_test =\App\OrderProductsVariants::where('order_product_id',$order_product_id)->where('product_variant_id',$v->id)->count();
			
				if($enabled_test > 0)
				{
					 $d["enabled"] =1;
				 
				}
				else
				{
						 $d["enabled"] =0;
				 
				}
				$pvt->v = $variant;
			 }
			 
			 $product_variant_types_array[] = $d;
		 }
		 else
		 {
			 
		 }
     }
	 if(sizeof($product_variant_types_array) > 0)
	 {
	   $data['status_code']    =   1;
       $data['status_text']    =   'Success';             
       $data['message']        =   'Variants Fetched Successfully';
	   $data['data']    =   $product_variant_types_array;   
       
	   return $data;
	 }
	 else
	 {
	   $data['status_code']    =   0;
       $data['status_text']    =   'Failed';             
       $data['message']        =   'No Variants Found';
	   $data['data']    =   [];  
       
	   return $data;
	 }
  }
  
   
public function update_product_variants(Request $request)
  {
	  $order_product_variants = $request->order_product_variants;
	   
	  foreach($order_product_variants as $pv)
	  {
		  $enabled = $pv['enabled'];
		  if($enabled == '0' or $enabled ==0)
		  {
			  @\App\OrderProductsVariants::where('order_product_id',$pv->order_product_id)->where('product_variant_id',$pv->product_variant_id)->delete();
		  }
		  else
		  {			   
			   $user = \App\OrderProductsVariants::firstOrNew(array('order_product_id' => @$pv['order_product_id'],'product_variant_id' =>@$pv['product_variant_id'] ));
               $user->title = @$pv['title'];
			   $user->value = @$pv['value'];
               $user->save();
          }
	  }
	   
	   $data['status_code']    =   1;
       $data['status_text']    =   'Success';             
       $data['message']        =   'Variants Updated Successfully';
	   $data['data']    =   [];   
       
	   return $data;
  }
  
  
  
  
  
  
  
 public function edit_order_add_product(Request $request , $order_id)
 {
	$products =  $request->order_products;
  
	foreach($products as $p)
	{
	            $products = new \App\OrderProducts;
				$products->order_id = $order_id;
				$products->product_id = $p["id"];
				$products->title = $p["title"];
				$products->category_id = $p["category_ids"];
				$products->discount = $p["base_discount"];
				$order_products_variants = @$p["variants"];
				$discounted_price = $p["discounted_price"];
				
				
								if( sizeof($order_products_variants) > 0)
						{
						    for($y=0;$y<sizeof($order_products_variants);$y++)
								{
								  
									    $price = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['price'])->price;
										
										if($price != '' or $price != null)
										{
											$discounted_price = floatval($price);
										}
									    
								}
								
								
								
							for($y=0;$y<sizeof($order_products_variants);$y++)
								{
									
								       $price_difference = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['price_difference'])->price_difference;
										 
											if($price_difference != '' or $price_difference != null)
										{
											$discounted_price = $discounted_price + floatval($price_difference);
										}
							    }
								
								
						
						}
						
						
						
				$products->discounted_price = $discounted_price;
				$products->unit = @$p["units"]."";
				$products->quantity = @$p["quantity"];
				$products->save();
				 
				$order_products_variants = @$p["variants"];
				if( sizeof($order_products_variants) > 0)
						{
							
							 for($y=0;$y<sizeof($order_products_variants);$y++)
								{
								 
						                $product_variant_type_id = @\App\ProductVariants::where('id',$order_products_variants[$y]['id'])->first(['product_variant_type_id'])->product_variant_type_id;
									    $product_variant_type_title = @\App\ProductVariantTypes::where('id',$product_variant_type_id)->first(['title'])->title;
										
								 
										$product_variant_value = @\App\ProductVariants::where('product_variant_type_id',$product_variant_type_id)->where('product_id',$p["id"])->where('id',$order_products_variants[$y]['id'])->first(['title'])->title;
					 
					 
										$variants = new \App\OrderProductsVariants;
										$variants->order_product_id = $products->id;
										$variants->title =$product_variant_type_title;
										$variants->value =$product_variant_value;
										$variants->product_variant_id = $order_products_variants[$y]['id'];
                                        $variants->save();
								        
										$id =  $order_products_variants[$y]['id'];
								}
						}
 }				
						
				

$calculation = $this->place_order_calculate($request);		
$order_total = $calculation[0]["order_total"];	
$sub_total = $calculation[0]["data"]["data"][0]["value"];

@\App\Orders::where('id',$order_id)->increment('total',$order_total);
@\App\Orders::where('id',$order_id)->increment('sub_total',$sub_total);

$tax_transactions = @\App\OrderTaxTransactions::where('order_id',$order_id)->get();
		$discounted_tax = $sub_total * 0.09;
		foreach($tax_transactions as $tax_transaction)
		{
		   @\App\OrderTaxTransactions::where('id',$tax_transaction->id)->increment('amount',$discounted_tax);		   
        }

	
	   $data['status_code']    =   1;
       $data['status_text']    =   'Success';             
       $data['message']        =   'Product Added Successfully';
	   $data['data']    =   [];  
       return $data;	
   
						
						
	 
 }
 
 
 
  
 
 
 
 
 
 
 
 
 
public function edit_order_delete_product(Request $request , $order_id)
{
	 
	 
	 $order_product_id =  $request->order_product_id;
	 $order_product_price = @\App\OrderProducts::where('order_id',order_product_id)->first(['discounted_price'])->discounted_price;
	 $order_product_quantity = @\App\OrderProducts::where('order_id',order_product_id)->first(['quantity'])->quantity;
	  return $order_id;
	// $calculation = $this->place_order_calculate($request);		
    // $order_total = $calculation[0]["order_total"];	
    // $sub_total = $calculation[0]["data"]["data"][0]["value"];
	
	\App\OrderProducts::where('id',$request->order_product_id)->delete();
	 \App\OrderProductsVariants::where('order_product_id',$request->order_product_id)->delete();

     @\App\Orders::where('id',$order_id)->decrement('total',$order_product_price*$order_product_quantity);
     @\App\Orders::where('id',$order_id)->decrement('sub_total',$order_product_price*$order_product_quantity);
									
	   $data['status_code']    =   1;
       $data['status_text']    =   'Success2';             
       $data['message']        =   'Product Deleted Successfully';
	   $data['data']    =   [];  
       return $data;	
}
 
 
 
 
 

}
