<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Mail;
use Hash;
use App\TermsConditions;
 


class TermsConditionsController extends Controller 
{
    
   
   public function __construct()
    {
        $this->middleware('auth');
    }

 
    public function index(request $request)
    {
		
		 
        // get all the users
        $terms_conditions = TermsConditions::where('key_name','terms_conditions')->first(["terms_conditions"])->terms_conditions;
		
 		return view('admin.terms_conditions.index')->with('content', $terms_conditions);
    }
 
 
 
 
     public function terms_conditions_update(request $request)
    {
    
	  $TermsConditions = TermsConditions::firstOrNew(array('key_name' => 'terms_conditions'));
      $TermsConditions->terms_conditions = $request->terms_conditions;	 
	  $TermsConditions->save();	 
      return view('admin.terms_conditions.index')->with('content', $TermsConditions->terms_conditions);
    }
 
 
 
	

}