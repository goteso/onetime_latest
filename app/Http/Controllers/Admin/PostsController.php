<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\Admin;
use App\TreasureBox;
use App\Posts;
use Hash;
use Mail;
 
 
use App\Traits\one_signal; // <-- you'll need this line...


class PostsController extends Controller 
{
    
  use one_signal; 
    
   
 

 
    public function index(request $request)
    {
	   $posts = Posts::orderBy('id','desc')->paginate(10); 
       return view('admin.posts.index')->with('posts', $posts );
    }
	
		   
		   public function user_by_status(request $request, $status)
    {
		// get all the users
        $users = Admin::where('status',$status)->orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.users.data-ajax', compact('users'));
        }
 		return view('admin.admin.index')->with('users', $users );
    }
	 
	public function user_by_verified(request $request, $verified)
    {
		   // get all the users
        $users = Admin::where('verified',$verified)->orderBy('id','desc')->paginate(10);  
		if ($request->ajax()) {
            return view('admin.users.data-ajax', compact('users'));
        }
 		return view('admin.admin.index')->with('users', $users );
    }
	
	

 
    public function create()
    {
      $countries = DB::table('countries')->get();        
      return view('admin.posts.create')->with('countries',$countries);
    }

 
    public function show($id)
    {   
        if(!is_numeric($id))
        {
         return redirect('admins');   
        }
        // get the testimonial
        $user = Admin::where('id',$id)->first(['id','first_name','last_name','email', 'status']);
        // show the view and pass the nerd to it
        return view('admin.admin.show')
            ->with('user', $user);
    }

 
    public function edit_post($id)
    {
        if(!is_numeric($id))
        {
         return redirect('admins');   
        }
        $user = Posts::find($id);

        return view('admin.posts.edit')->with('user', $user);
    }

 
	 
 
	 
	 
	 
	 
	 
	 
    public function update_post(Request $request)
    {
        $user = Posts::find($request->post_id);
		$user->title  = $request->title;
        $user->description   = $request->description;
        $user->type  = $request->type;
        $user->save();
         // redirect
        Session::flash('message', 'Successfully Updated Offer!');
        return back();
   }

 
    public function destroy($id)
    {
        $user = Posts::find($id);
        $user->delete();
		
		$d = @\App\PostsImages::where('post_id',$id)->delete();
		@\App\PostsComments::where('post_id',$id)->delete();
		@\App\PostsLikes::where('post_id',$id)->delete();
		@\App\PostsShippingMethods::where('post_id',$id)->delete();
		@\App\ReportedPosts::where('post_id',$id)->delete();
        Session::flash('message', 'Successfully deleted the Offer!');
        return back();
    }
	
	public function destroy_post_images($id)
	{
		@\App\PostsImages::where('id',$id)->delete();
		Session::flash('message', 'Image Deleted Successfully!');
        return back();
	}
	
	
		public function destroy_post_comments($id)
	{
		 
		@\App\PostsComments::where('id',$id)->delete();
		Session::flash('message', 'Comment Deleted Successfully!');
        return back();
	}
	
	
	public function block($id)
	{
	        $user = Admin::find($id);
            $user->status  = '0';
            $user->save();
			Session::flash('message', 'Successfully Blocked the Admin!');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	        //$this->notification_to_single($notification_token, 'You are Blocked' , 'You are blocked from loyatyApp by Admin');
				
		    return back();
	}
		public function unblock($id)
	{
	        $user = Admin::find($id);
            $user->status  = '1';
            $user->save();
			Session::flash('message', 'Successfully Unblocked the Admin!');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	       // $this->notification_to_single($notification_token, 'You are Unblocked' , 'You are Unblocked from loyatyApp by Admin');
		 		 
			return back();
	}
	
	
		public function verify($id)
	{
	        $user = Admin::find($id);
            $user->verified  = '1';
            $user->save();
			Session::flash('message', 'Admin Verified Successfully');
			
			//$notification_token= Admin::where('id',$id)->first(["notification_token"])->notification_token;
	       // $this->notification_to_single($notification_token, 'You are Verified' , 'You are Successfully Verified by loyatyApp Admin');
				
		    return back();
	}
	
	
	
	
	
 
		
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//New Services ==========================================
	
	
	
	 public function post_orders(request $request)
    {
	   $orders = \App\Orders::orderBy('id','desc')->paginate(10); 
	   return view('admin.orders.index')->with('result', $orders );
    }
	
	
	
		 public function trades(request $request)
    {
	   $orders = \App\TradesPosts::orderBy('id','desc')->paginate(10); 
	   return view('admin.trades.index')->with('result', $orders );
    }
	
	
	
		 public function trades_orders(request $request)
    {
	   $orders = \App\Trades::orderBy('id','desc')->paginate(10); 
 
	   return view('admin.trades_orders.index')->with('result', $orders );
    }
	
	
	
			 public function disputed_orders(request $request)
    {
	   $orders = \App\Trades::where('status','disputed')->orderBy('id','desc')->paginate(10); 
 
	   return view('admin.trades_orders.index')->with('result', $orders );
    }
	
	
	
	    public function reported_posts(request $request)
    {
	   $posts = @\App\ReportedPosts::where('status','0')->orderBy('id','desc')->paginate(10); 
	   
	  
       return view('admin.posts.reported_posts')->with('posts', $posts );
    }
	
	
	
	
	
	
		
     public function create_notification($notification_type,$notification_message,$user_id,$notification_by_user_id, $details)
    {               
                   $for_user_notification = \App\User::select('id','notification_token')->where('id',$user_id)->first();    
                   // App::setLocale($for_user_notification['language']);
                  //  $notification_message = trans('notification.'.$notification_type);

                    $input = array();                
                    $input['notification_type'] = $notification_type;        
                    $input['notification_message'] = $notification_message;
			 
					 
                    $input['offer_id'] = $details['offer_id'];
			 
						$input['user_id'] = $user_id;
				 
                    $input['notification_by_user_id'] = $notification_by_user_id;
			 
					
                    
                    $input['is_read'] = 0;
                   
                    $input['status'] = 1;
                    $notification = \App\Notification::create($input);
                    
                  //  $by_user_notification = User::select('first_name','last_name')->where('id',$notification_by_user_id)->first();

                  //  $notification_token = $for_user_notification['notification_token'];

                    $badge_count = $for_user_notification['badge_count']+1;

                    \App\User::where('id',$for_user_notification['id'])->update(['badge_count'=>$badge_count]);
                  
                    
                   // $push_message = $by_user_notification['first_name'].' '.$by_user_notification['last_name'].$notification_message;
                     
                    
                    
                        $custom_array = array(
                            'follow_user_id' => $user_id,
                            'notification_by_user_id' => $notification_by_user_id,
                            'notification_type' => $notification_type,
                   
                        );

                     
					   
						
						//$this->notification_to_multiple_identifier($notification_token , $notification_type , $push_message , $details);


                    
           
    }
	
	
	
	
	
	
	

}