<?php

namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Mail;
use Hash;
 
 
use App\Traits\one_signal; // <-- you'll need this line...


class TransactionsController extends Controller 
{
    
  use one_signal; 
    
   
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function earned_commisions(request $request)
    {
	    // get all the users
        $data = @\App\Transactions::where('commision','yes')->orderBy('id','DESC')->paginate(10);  
		
		$total = 0;
		$transactions = @\App\Transactions::where('commision','yes')->orderBy('id','DESC')->get();
		foreach($transactions as $t)
		{
			$total = $total + abs($t->amount);
		}
        return view('admin.earned_commisions.index')->with('data', $data)->with('total', $total);
    }
	
		   

 
	
	
	

}