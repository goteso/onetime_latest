<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinbaseLogs  extends Model
{
        protected $fillable = [
        'coinbase_data','btc_amount','transaction_id','account_id','receiving_address','user_id'
    ];
 
 
 
 
     /**
     * Always capitalize the last name when we retrieve it
     */
			    public function getCreatedAtAttribute($value) {
         //$v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		return @\Carbon\Carbon::parse($value)->format('M d,Y | h:i A');
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }





 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}
